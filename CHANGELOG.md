# Changelog

## 1.0.1 (2021-03-07)

* Allow non-float input types for relevant lists in Mathematica interface.
* Efficiency improvements in function `Mass::mMSbar`.
* Add version number and last modification date in file `REvolver.h`.
* Documentation fixes.

## 1.0 (2021-02-01)

* Initial release

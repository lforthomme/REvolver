////////////////////////////////////////////////////////////////////////////////
/// \file Alpha.h
/// \brief Interface of class Alpha
/// \author Andre Hoang, Christopher Lepenik, Vicent Mateu
///
// Copyright:
//    Copyright (C) 2020 Andre Hoang, Christopher Lepenik, Vicent Mateu
//
//    This file is part of REvolver.
//
//    REvolver is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    REvolver is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with REvolver.  If not, see <https://www.gnu.org/licenses/>.
////////////////////////////////////////////////////////////////////////////////

#ifndef ALPHA_H
#define ALPHA_H

#include <complex>
//#include <iostream>
#include <vector>

#include "AnDim.h"
#include "AnDimCollection.h"
#include "Global.h"
#include "TypeDef.h"

namespace revo {

////////////////////////////////////////////////////////////////////////////////
/// \class Alpha
///
/// \brief Provides the running and matching of the strong coupling.
/// \author Christopher Lepenik and Vicent Mateu
///
/// The class uses a semi-analytic expansion for \f$\alpha_s\f$ in which
/// \f$\alpha\, \mathrm{log}\f$ is treated as \f$\mathcal{O}(1)\f$ as well as
/// other exact and approximate methods for \f$\alpha_s\f$ running. Threshold
/// matching at the specified points is computed.
////////////////////////////////////////////////////////////////////////////////

class Alpha {
  friend class Evolving;
  friend class Core;
  friend class Mass;
  friend class MQuantity;
  friend class RQuantity;

 public:
  /**
   * \name Constructors
   * \{
   */
  /// Constructor with full control over all parameters
  /**
   * Default arguments are set if not given explicitly.
   *
   * \param nTot Specifies the total number of quark flavors.
   * \param alphaPar Specifies the flavor scheme, value and scale of the input
   *                 coupling.
   * \param masses A sorted vector containing the masses of the massive flavors
   *               starting at the lightest one, either as the standard running
   *               mass or in the pole scheme.
   * \param fMatch A vector containing elements <tt>{f1, f2, ...}</tt>, where
   *               \p fn specifies that the flavor matching scale for the n-th
   *               lightest massive quark threshold is \p fn times the given
   *               mass. Default: all \p fn are set to 1.0.
   * \param run The loop order used for the running of the strong coupling.
                Default: highest available order which is 5.
   * \param lambda A parameter probing the renormalization scale dependence of
   *               the QCD beta-function. With respect to the perturbative
   *               series of the beta-function truncated at the order set by
   *               \p run, a (\p run + 1) order term is estimated from
   *               renormalization scale variation: the estimate is obtained by
   *               expanding the original perturbative series (in powers of
   *               \f$\alpha_s(\mu)\f$) in terms of
   *               \f$\alpha_s(\mathtt{lambda} \times \mu)\f$, truncating at
   *               order \p run. The result is expanded in \f$\alpha_s(\mu)\f$
   *               truncating at order (\p run + 1). Default: 1.0.
   * \param order The loop order used for the strong coupling flavor threshold
   *              matching relations. The renormalization scale dependence of
   *              these matching relations is precise to loop order \p order and
   *              independent of the value specified for \p run. Default:
   *              highest available order which is 4.
   * \param scheme %Mass scheme of the input masses - \p MScheme::MSbar or
   *               \p MScheme::pole. Default: \p MScheme::MSbar.
   * \param method Specifies the numerical method used for evolution. Default:
   *               RunMethod::Exact\n.
   *               Available options:
   *                 - \p RunMethod::Exact
   *                 - \p RunMethod::Iterative
   *                 - \p RunMethod::Expanded
   *                 - \p RunMethod::Inverse
   * \param precisionGoal The parameter setting the relative precision of all
   *                      convergent infinite sums and iterative algorithms. The
   *                      input value is clipped to the range [1e−6, 1e−15].
   *                      Default: 1e−15.
   */
  Alpha(int nTot, const RunPar& alphaPar, const doubleV& masses,
        const doubleV& fMatch = doubleV(), int run = kMaxRunAlpha,
        double lambda = 1.0, int order = kMaxOrderAlpha,
        MScheme scheme = MScheme::MSbar, RunMethod method = RunMethod::Exact,
        double precisionGoal = kMaxPrec);
  /// Constructor with masses and matching scales not yet given
  /**
   * Masses and flavor matching scales can be set after object creation by using
   * the function init(const doubleV& masses, const doubleV& fMatch).
   *
   * \sa Alpha(int nTot, const RunPar& alphaPar, const doubleV& masses, const doubleV& fMatch, int run, double lambda, int order, MScheme scheme, RunMethod method, double precisionGoal)
   */
  Alpha(int nTot, int nm, const RunPar& alphaPar, int run = kMaxRunAlpha,
        double lambda = 1.0, int order = kMaxOrderAlpha,
        MScheme scheme = MScheme::MSbar, RunMethod method = RunMethod::Exact,
        double precisionGoal = kMaxPrec);
  /// Default constructor
  /**
   * The object created with this constructor is basically useless, but the
   * state of another Alpha object can be copied to it.
   */
  Alpha() = default;
  /**
   * \}
   */

  /// Initializes the object with the given masses and flavor matching scales.
  /**
   * \p masses has to be sorted with the smallest value first. If \p fMatch is
   * not given, the elements are set to 1.0.
   */
  void init(const doubleV& masses, const doubleV& fMatch = doubleV());

  /// Sets the beta-function coefficient to custom values.
  /**
   * Note that custom beta function coefficients are meant to be used only in
   * scenarios without massive particles.
   *
   * The loop orders used for alpha running is automatically set to include all
   * given beta function coefficients.
   *
   * \sa AnDim::setBeta
   */
  void setBeta(const doubleV& beta);

  /// Returns \f$\alpha_s\f$ at the scale \p mu with \p nf active flavors.
  /**
   * If \p nf is not given, the flavor number scheme is determined from the
   * automatic matching conventions based the absolute value of \p mu.
   *
   * \tparam T Type of scale: \p double or \p std::complex<double>.
   */
  template <typename T>
  T operator()(T mu, int nf = kDefault) const;

  /// Returns an AnDim object created with \p nf flavors.
  const AnDim& andim(int nf) const;
  /// \brief Returns an ordered vector containing all given masses starting with
  ///        the lightest one.
  const doubleV& inMasses() const;
  /// Returns the number of loops used for running.
  int runOrd() const;
  /// Returns various specified flavor numbers.
  /**
   * Possible inputs:
   *   - \p NfType::Total for the total number of flavors,
   *   - \p NfType::Massive for the number of massive flavors,
   *   - \p NfType::Massless for the number of massless flavors.
   */
  int nfl(NfType type) const;
  /// Returns \f$\Lambda_{QCD}\f$ in the \p nf flavor scheme.
  /**
   * The value returned is based on the specified convention with the precision
   * for the evolution set by the Core object.
   *
   * \param nf Flavor scheme.
   * \param convention The definition of \f$\Lambda_{QCD}\f$. Possible inputs:
   *                   - \p LambdaConvention::MSbar for the conventional MSbar
   *                     definition according to the PDG2020,
   *                   - \p LambdaConvention::tScheme for the definition based on
   *                     the t-variable notations of arXiv:1704.01580.
   *                   Default: \p LambdaConvention::MSbar
   *
   */
  double lambdaQCD(int nf,
                   LambdaConvention convention = LambdaConvention::MSbar) const;
  /// Returns nf, scale and value of the strong coupling at creation.
  const RunPar& defParams() const;

 private:
  //----------------------------------------------------------------------------
  // Private enums
  //----------------------------------------------------------------------------

  /// Perturbative and consistent (always up) matching of alpha at thresholds
  enum class AlphaMatchType { Perturbative, Consistent };

  //----------------------------------------------------------------------------
  // Private data members
  //----------------------------------------------------------------------------

  int _order;                 ///< Number of loops in matching
  int _run;                   ///< Number of loops in running
  double _lambda;             ///< Parameter for RGE scale variation
  int _nTot, _nm, _n0;        ///< Number of total, massive, massless flavors
  RunPar _parDef;             ///< Reference flavor number, value and scale
  RunMethod _method;          ///< Method to solve the RGE equation
  AlphaMatchType _matchType;  ///< Matching type: perturbatively or consistent
  AnDimCollection _andim;     ///< Collection of AnDim objects
  bool _validState{false};    ///< true if object construction finished

  doubleV _masses;  ///< The given masses
  doubleV _fMatch;  ///< Factors of m(m) for flavor matchin scales
  std::unordered_map<int, double> _refMu;     ///< Reference scales
  std::unordered_map<int, double> _refAlpha;  ///< Reference values
  std::unordered_map<int, double> _lQCD;  ///< \f$\Lambda_\mathrm{QCD}^{n_f}\f$

  //----------------------------------------------------------------------------
  // Private functions
  //----------------------------------------------------------------------------

  void setLambdaQCD();  ///< Sets _lQCD;

  /// Performs threshold matching in both directions
  double match(Direction dir, int nf, double aRef, double lg) const;
  /// Performs inverse threshold matching in both directions
  double matchReverse(Direction dir, int nf, double aRef, double lg) const;
  /// Computes LambdaQCD at \p nf flavors
  /**
   * If no \p order is given the sum in the exponential is not truncated.
   */
  double computeLambdaQCD(int nf, double muLambda = 100.0,
                          int order = kDefault) const;
  /// Returns the natural flavor number of a quantity at the scale \p mu.
  int nfNatural(double mu) const;
};

////////////////////////////////////////////////////////////////////////////////
// Member function template definitions
////////////////////////////////////////////////////////////////////////////////

//------------------------------------------------------------------------------
template <typename T>
T Alpha::operator()(T mu, int nf) const {
  if (nf == kDefault) {
    nf = nfNatural(std::abs(mu));
  } else if (nf < _n0 || nf > _nTot) {
    error(ErrorType::Nf);
    return nan("");
  }
  if (_andim.count(nf) < 1) {
    error(ErrorType::InvalidState);
    return nan("");
  }

  return _andim.at(nf).alphaGeneric(_method, _refMu.at(nf), _refAlpha.at(nf),
                                    mu);
}

}  // namespace revo

#endif  // ALPHA_H

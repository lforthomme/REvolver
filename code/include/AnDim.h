////////////////////////////////////////////////////////////////////////////////
/// \file AnDim.h
/// \brief Interface of class AnDim
/// \author Andre Hoang, Christopher Lepenik, Vicent Mateu
///
// Copyright:
//    Copyright (C) 2020 Andre Hoang, Christopher Lepenik, Vicent Mateu
//
//    This file is part of REvolver.
//
//    REvolver is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    REvolver is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with REvolver.  If not, see <https://www.gnu.org/licenses/>.
////////////////////////////////////////////////////////////////////////////////

#ifndef ANDIM_H
#define ANDIM_H

#include <complex>
#include <functional>
//#include <iostream>
#include <map>
#include <unordered_map>
#include <vector>

#include "Global.h"
#include "Pochhammer.h"
#include "TypeDef.h"

namespace revo {

////////////////////////////////////////////////////////////////////////////////
/// \class AnDim
///
/// \brief Provides several coefficients and helper functions related to
///        parameter running and matching.
/// \author Christopher Lepenik and Vicent Mateu
///
/// This class provides
///   - the QCD beta funcion and mass anomalous dimensions for a given number of
///     flavors up to four loops,
///   - coefficients related to threshold matching of the coupling and masses,
///   - coefficients of the perturbative series relating several mass
///     renormalization schemes with the pole scheme, including lighter massive
///     quark effects,
///   - other coefficients needed for mass running.
////////////////////////////////////////////////////////////////////////////////

class AnDim {
  friend class Alpha;
  friend class Evolving;
  friend class Core;
  friend class Mass;
  friend class MQuantity;

 public:
  /**
   * \name Constructors
   * \{
   */
  /// Constructor specifying the used flavor number \p nf and the mass scheme.
  /**
   * \param nf Number of flavors used in various anomalous dimensions and other
   *           coefficients.
   * \param scheme %Mass scheme used for matching of the coupling constant,
   *               either MScheme::MSbar or MScheme::Pole. Default:
   *               MScheme::MSbar.
   * \param precisionGoal The parameter setting the relative precision of all
   *                      convergent infinite sums and iterative algorithms. The
   *                      input value is clipped to the range [1e−6, 1e−15].
   *                      Default: 1e−15.
   */
  explicit AnDim(int nf, MScheme scheme = MScheme::MSbar,
                 double precisionGoal = kMaxPrec);
  /// Default constructor
  /**
   * The object created with this constructor is basically useless, but the
   * state of another AnDim object can be copied to it.
   */
  AnDim() = default;
  /**
   * \}
   */

  /// Sets parameters for \f$\alpha_s\f$ running.
  /**
   * \param betaOrd The number of beta function coefficients used.
   * \param betaIsStrict Specifies if beta function related coefficients should
   *                     be computed even if the knowledge of the needed beta
   *                     coefficient orders is incomplete. This setting changes
   *                     depending on the method used for \f$\alpha_s\f$
   *                     running.
   */
  void setAlphaProp(int betaOrd, bool betaIsStrict);
  /// Sets lambda for scale variation in the running of \f$\alpha_s\f$.
  /**
   * \note This functionality is only available if \p RunMethod::Exact is used
   *       in alphaGeneric.
   */
  void setLambdaAlpha(double lambda);
  /// Sets the lambda parameter for scale variation in MSbar-mass evolution.
  void setLambdaMSbar(double lambda);
  /// Sets the lambda parameter for scale variation in R-evolution.
  void setLambdaR(double lambda);
  /// Sets parameters for R-evolution.
  /**
   * \param gammaROrd The number of R-anomalous dimension coefficients used.
   * \param sIsStrict Specifies if s coefficients should be computed even if the
   *                  knowledge of the needed beta and anomalous dimension
   *                  coefficient orders is incomplete. This setting changes
   *                  depending on the method used for R-evolution.
   */
  void setMsrProp(int gammaROrd, bool sIsStrict);
  /// Controls the error of the 4-loop coefficient in the pole-MS mass relation.
  /**
   * Should be varied between -1 and 1 to scan the standard deviation.
   */
  void setMsBarDeltaError(double msBarDeltaError);
  /// Sets beta function coefficients to an arbitrary number of custom values.
  /**
   * Convention: \f$\mu \frac{\mathrm d\alpha_s}{\mathrm d \mu}
   *                = \beta_\mathrm{QCD}(\alpha_s)
   *                = -2\alpha_s\sum_{n=1}\beta_{n-1}
   *                  \left(\frac{\alpha_s}{4\pi}\right)^n\f$
   *
   * Note that this only changes the beta function coefficients and indirectly
   * functions relying explicitly on them. Flavor matching coefficients and all
   * mass-related outputs and quantities are not changed. Custom beta function
   * coefficients are meant to be used only in scenarios without massive
   * particles for alpha evolution.
   *
   * The loop orders used for alpha running is automatically set to include
   * all given beta function coefficients.
   */
  void setBeta(const doubleV& beta);
  /// \brief Sets the R anomalous dimension coefficients to an arbitrary number
  ///        of custom values.
  /**
   * Note that this only changes the R anomalous dimension coefficients and
   * indirectly functions relying explicitly on them. Flavor matching
   * coefficients and other outputs and quantities are not changed.
   *
   * The loop orders used for R-evolution is automatically set to include
   * all given coefficients.
   */
  void setGammaR(const doubleV& gamma);
  /// \brief Sets the mass anomalous dimension coefficients to an arbitrary
  ///        number of custom values.
  /**
   * Note that this only changes the mass anomalous dimension coefficients and
   * indirectly functions relying explicitly on them. Flavor matching
   * coefficients and other outputs and quantities are not changed.
   *
   * The loop orders used for mass evolution is automatically set to include
   * all given coefficients.
   */
  void setGammaMass(const doubleV& gamma);
  /// \brief Can be used to provide a map of {int, AnDim}, used in
  ///        setNextMSBarDeltaCoef (optional).
  /*
   * If given, it is assumed that all optional parameters of the AnDim objects
   * are already set correctly and are changed manually to always coincide with
   * the parameters of this object.
   * If no map pointer is given to this object, a data member cache is used.
   */
  void provideAndimMap(std::unordered_map<int, AnDim>* andimM);

  /// Returns a vector of the used beta function coefficients.
  const doubleV& betaCoef() const;
  /// Returns precisionGoal
  double precisionGoal() const;

 private:
  //----------------------------------------------------------------------------
  // Private enums
  //----------------------------------------------------------------------------

  /// Used to distinguish several coefficients related to alpha matching
  enum class AlphaMatchCoefType { InvLog, Log, Inv, Basic };
  /// Types of  QCD static potential constants
  enum class StPotType { c1, c2, d1, d2, A };

  //----------------------------------------------------------------------------
  // Private structs
  //----------------------------------------------------------------------------

  /// Contains coefficients related to the beta function and helper constants
  /**
   * This construction, in combination with \p _coefMap is used to deal with the
   * fact that the coefficients change their values in dependence of the used
   * loop order and methods, without computing the coefficients over and over
   * again.
   */
  struct DynCoefs {
    DynCoefs();
    doubleV _bHat, _cCoef;
    std::map<int, doubleV> _gl, _glLambda;
    doubleV _betaLambda, _bCoefLambda, _bHatLambda, _cCoefLambda;
    std::unordered_map<int, doubleV> _wTildeSum;
    int _m1pow, _m1powLambda;
    double _beta02pow, _beta02powLambda;
  };

  /// Contains s coefficients and helper constants
  /**
   * This construction, in combination with \p _sMap is used to deal with the
   * fact that the s coefficients change their values in dependence of the used
   * loop order and methods, without computing the coefficients over and over
   * again.
   */
  struct SCoef {
    std::map<int, doubleV> _sCoef, _sCoefLambda;
    doubleV _gammaTil, _gammaTilLambda;
    doubleV _msrDeltaAsy, _msBarDeltaAsy;
  };

  //----------------------------------------------------------------------------
  // Private non-mutable data members
  //----------------------------------------------------------------------------

  int _nf;                         ///< Number of active flavors
  double _lambdaAlpha;             ///< For coupling RGE scale variation
  double _lambdaMSbar;             ///< For MSbar-evolution scale variation
  double _lambdaR;                 ///< For R-evolution scale variation
  MScheme _scheme;                 ///< MSbar or pole
  bool _sIsStrict, _betaIsStrict;  ///< true if strict counting is used
  int _betaOrd, _gammaROrd;  ///< orders used to compute derived coefficients
  double _msBarDeltaError;   ///< used to control error of 4-loop msBarDelta
  double _precisionGoal;     ///< relative precision of iterative algorithms
  doubleV _beta;             ///< Beta function coefficients
  doubleV _gammaMass;        ///< mass anomalous dimensions in MS-bar scheme
  doubleV _gammaMassLambda;  ///< mass anomalous dimensions with lambda terms
  doubleV _msrnDelta, _msBarDelta;  ///< mPole - MS coefficients, logs set zero
  doubleV _gammaRn;                 ///< R anomalous dimension for MSRn mass
  doubleV _gammaRnLambda;           ///< R anomalous dimension with lambda terms
  std::unordered_map<int, AnDim>* _andimM;  ///< Optional map to connect AnDim

  doubleV _bCoef;  ///< b coefficients related to the beta function

  //----------------------------------------------------------------------------
  // Private mutable data members
  // Only computed if needed, should be accessed only via getter functions
  //----------------------------------------------------------------------------

  /// Threshold matching downwards and upwards with logs set to zero (length 5)
  mutable doubleV _alphaMatch, _alphaMatchInv;
  /// \brief Threshold matching downwards and upwards with explicit coefs for
  ///        log(mu/m) (size: 5x5)
  mutable doubleG _alphaMatchLog, _alphaMatchInvLog;
  /// R anomalous dimension for MSRp mass (length 4)
  mutable doubleV _gammaRp;
  /// R anomalous dimension for MSRp mass with lambda terms (length 4)
  mutable doubleV _gammaRpLambda;
  /// mPole - mMSRp coefficients with logs set to zero (length 4)
  mutable doubleV _msrpDelta;
  /// Used as cache in deltaR
  mutable std::map<int, rac::Pochhammer> _poch;
  /// Used as cache if _andimM is not set; used in setNextMSBarDeltaCoef()
  mutable std::vector<AnDim> _andimM1;

  /// Map containing all DynCoefs already used
  mutable std::map<int, DynCoefs> _coefMap;
  /// Map containing all SCoef already used
  mutable std::map<int, SCoef> _sMap;

  //----------------------------------------------------------------------------
  // Private member functions - related to mutable data members
  //----------------------------------------------------------------------------

  /// Builds key to access \p _sMap
  static int buildSKey(int betaOrd, int gammaROrd, MScheme sType);
  /// \brief Returns \p _alphaMatch coefficients without logs and computes them
  ///        if not already done
  const doubleV& alphaMatch(AlphaMatchCoefType type) const;
  /// \brief Returns \p _alphaMatch coefficients with logs and computes them if
  /// not already done
  const doubleG& alphaMatchLog(AlphaMatchCoefType type) const;
  /// Returns \p _msrpDelta and sets it if not already done
  const doubleV& msrpDelta() const;
  /// Returns \p _gammaRp and sets it if not already set
  const doubleV& gammaRp() const;
  /// Returns \p _gammaRpLambda and sets it if not already set
  const doubleV& gammaRpLambda() const;

  //----------------------------------------------------------------------------
  // Private member functions - coupling related
  //----------------------------------------------------------------------------

  /// Computes the threshold matching with logs (output size 5x5)
  static doubleG alphaMatchingLog(const doubleV& d, int nfIn, int nfOut);
  /// \brief Changes the scale of alphaS in a perturbative series - coef starts
  ///        at 1-loop, generic beta. (Output size length coef x length coef)
  static doubleG expandAlpha(const doubleV& bet, const doubleV& coef);
  /// QCD beta function for nf active flavors (output size 5)
  static doubleV betaGen(int nf);
  /// QCD beta function, nf active flavors with lambda terms (output size 5)
  doubleV betaLambda(int betaOrd, double lambda) const;
  /// Generate anomalous dimensions with lambda terms (output size = input size)
  doubleV gammaLambda(double lambda, const doubleV& gamma) const;
  /// Used to expand alpha in a different flavor scheme
  static doubleV getInverseMatch(const doubleV& c, int n);
  /// int_a0^a1 da/beta(a) expanding out 1/beta
  template <typename T>
  static T iter(const doubleV& cCoef, T aMu, double a0, T aLLInv,
                T* last_corr = nullptr);
  /// \brief Computes the threshold matching for nf flavors with logs set to
  ///        zero (output size 5)
  doubleV alphaMatch(int nf, MScheme scheme = MScheme::Default) const;
  /// Compute next unknown bHat and c coefficients
  void setNextbHatc(int betaOrd) const;
  /// Compute next unknown bHat and c coefficients including lambda terms
  void setNextbHatcLambda(int betaOrd) const;
  /// Generic functions to compute next unknown gl and gTilde coefficients
  template <typename F>
  void setNextglGeneric(doubleV& gl, double p, F bHatIn) const;
  /// Compute next unknown gl and gTilde coefficients
  void setNextgl(int betaOrd, int p = 1) const;
  /// Compute next unknown gl and gTilde coefficients with lambdaAlpha variation
  void setNextglLambda(int betaOrd, int p = 1) const;
  /// \brief Threshold matching upwards and downwards with explicit coefs for
  ///        log(mu/m) (output size 5x5)
  doubleG matchingAlphaLog(Direction dir) const;
  /// \brief Changes the scale of alphaS in a perturbative series - coef starts
  ///        at 1-loop, coefs are coefficients of (alpha / pi)
  void expandAlpha(double muIn, double muOut, doubleV& coef,
                   int betaOrd = kDefault) const;
  /// Puts together logs and alphaS power to do the alpha matching
  doubleV thresholdMatching(Direction dir, int order, double lg) const;
  /// Matches a given series down from or up to _nf
  doubleV& matchSeries(doubleV& series, Direction dir, double log = 0.0) const;
  /// G used for lambdaQCD; if no order is given the sum is not truncated
  double gFun(double al, int order = kDefault) const;
  /// Returns bHat coefficients
  double bHat(int n, int betaOrd, bool isStrict) const;
  /// Returns bHat coefficients with lambdaAlpha variation
  double bHatLambda(int n, int betaOrd, bool isStrict) const;
  /// Returns c coefficients
  double cCoef(int n, int betaOrd, bool isStrict) const;
  /// Returns c coefficients with lambda terms
  double cCoefLambda(int n, int betaOrd, bool isStrict) const;
  /// Returns gl coefficients
  double gl(int n, int betaOrd, bool isStrict, int p = 1) const;
  /// Returns gl coefficients including lambdaAlpha variation
  double glLambda(int n, int betaOrd, bool isStrict, int p) const;
  /// Returns gTilde coefficients
  double gTilde(int n, int betaOrd, bool isStrict, int p = 1) const;
  /// Returns gTilde coefficients with lambda variation
  double gTildeLambda(int n, int betaOrd, bool isStrict, int p = 1) const;
  /// Computes alpha^(n)(mu) from alpha(muIn); mu can be complex
  template <typename T>
  T alphaGeneric(RunMethod method, double muIn, double aMuIn, T mu) const;

  //------------------------------------------------------------------------------
  // Private mamber functions - mass related
  //------------------------------------------------------------------------------

  /// \brief Returns the normalization \f$N_{p/2}\f$ of the leading renormalon
  ///        related to the R-anomalous dimension
  /**
   * The probed series is the one from which the R-anomalous dimension
   * coefficients specified at object creation were computed.
   *
   * See <https://arxiv.org/abs/1704.01580> and
   * <http://hdl.handle.net/1721.1/53228> for more details on the used
   * formula.
   *
   * \param order Specifies how many terms should be used to compute the
   *              normalization. Default: Highest available order.
   * \param p The mass dimension of the quantity related to the R-anomalous
   *          dimension
   * \param sCoef Arbitrary S coefficients, e.g. to implement lambda variation
   *
   * \sa Pp2
   */
  double Np2(int p = 1, int betaOrd = kDefault, int gammaROrd = kDefault,
             const doubleV* sCoe = nullptr) const;
  /// \brief Returns the normalization \f$P_{p/2}\f$ of the leading renormalon
  ///        related to the R-anomalous dimension
  /**
   * \sa Np2
   */
  double Pp2(int p = 1, int betaOrd = kDefault, int gammaROrd = kDefault,
             const doubleV* sCoe = nullptr) const;
  /// QCD static potential constant c1 computed from c2, de and A
  static double statConstC1(double c2, double d2, double A);
  /// QCD static potential constants
  static std::map<StPotType, double> statConst();
  /// QCD mass anomalous dimension for nf active flavors (output size 5)
  static doubleV massAndim(int nf);
  /// MS-bar pole relation (ouput size 4)
  static doubleV msBarDelta(int nl, int nh, double error4 = 0.0);
  /// PS - pole relation, rIR = muIR/muF (output size 4)
  static doubleV psDelta(int nl, double rIR);
  /// RS - pole relation (output size 4)
  doubleV rsDelta(int nRS = kMaxRunAlpha - 1, double N12var = kDefault) const;
  /// 1S - pole relation without logs (output size 4)
  static doubleV oneSDelta(int nl, double alpha);
  /// \brief Kinetic - pole relation, linear and quadriatic terms,
  ///        mu_s = 2 * mu (output site 3)
  static void kinDelta(int nl, doubleV& linear, doubleV& quadratic);
  /// Returns the coefficients relating the (natural) MSR to the pole scheme.
  /**
   * If <tt>n > gammaROrd</tt> a renormalon-based formula is used to predict the
   * coefficient of the asymptotic series.
   */
  double msrnDeltaAsy(int n, int betaOrd = kMaxRunAlpha,
                      int gammaROrd = kMaxRunMSR) const;
  /// Returns the coefficients relating the practical MSR to the pole scheme.
  /**
   * If <tt>n > gammaROrd</tt> a renormalon-based formula is used to predict the
   * coefficient of the asymptotic series.
   */
  double msrpDeltaAsy(int n, int betaOrd = kMaxRunAlpha,
                      int gammaROrd = kMaxRunMSR) const;
  /// \brief Returns the coefficients relating the \f$\overline{\mathrm{MS}}\f$
  ///        to the pole scheme.
  /**
   * If <tt>n > gammaROrd</tt> a renormalon-based formula is used to predict the
   * coefficient of the asymptotic series.
   */
  double msBarDeltaAsy(int n, int betaOrd = kMaxRunAlpha,
                       int gammaROrd = kMaxRunMSR) const;
  /// Computes R-anomalous dimensions from delta coefficients
  static doubleV gammaRComputer(const doubleV& beta, const doubleV& delta);
  /// Needed for analytic R-evolution. Incomplete Gamma functions
  template <typename T>
  T inteCorre(double a, double t0, T t1, int p = 1) const;
  /// \brief 3-loop light massive quark correction to pole-MSbar relation
  ///        - gluonic part
  static double msLightCorr3LG(double r);
  /// 3-loop light massive quark correction to pole-MSbar relation - nl part
  static double msLightCorr3LNl(double r);
  /// 3-loop light massive quark correction to pole-MSbar relation - nh part
  static double msLightCorr3LNh(double r);
  /// 2-loop light massive quark correction to pole-MSbar relation
  static double msLightCorr2L(double r);
  /// \brief 3-loop light massive quark correction to pole-MSbar relation
  ///        without double bubble
  static double msLightCorr3L(int nh, int nl, double r);
  /// \brief 3-loop light massive quark correction to pole-MSbar relation
  ///        - double bubble
  static double msLightCorr3LDoBu(double r1, double r2);
  /// Light massive quark correction to pole-MSbar relation
  static doubleV msLightCorr(int idx, int nh, int nl, const doubleV& mm);
  /// 2-loop light massive quark correction to pole-PS relation
  static double psLightCorr2L(double muf, double mL);
  /// \brief 3-loop light massive quark correction to pole-PS relation without
  ///        double bubble
  static double psLightCorr3L(int nl, double muf, double mL);
  /// 3-loop light massive quark correction to pole-PS relation - double bubble
  static double psLightCorr3LDoBu(double muf, double mL1, double mL2);
  /// Light massive quark correction to pole-PS relation
  static doubleV psLightCorr(int idx, int nl, double muF, const doubleV& mm);
  /// 2-loop light massive quark corrections to pole-1S relation (MSbar input)
  static double oneSLightCorr2L(double bohr, double mL);
  /// 3-loop light massive quark corrections to pole-1S relation (MSbar input)
  static double oneSLightCorr3L(int nl, double bohr, double mL, double muA);
  /// 3-loop light massive quark correction to pole-1S relation - double bubble
  static double oneSLightCorr3LDoBu(double bohr, double mL1, double mL2);
  /// Helper function for 1S light mass corrections
  static double oneSLighth0(double a);
  /// Helper function for 1S light mass corrections
  static double oneSLighth0bar(double a);
  /// Light massive quark correction to pole-1S relation
  static doubleV oneSLightCorr(int idx, int nl, double mB, const doubleV& mm,
                               double muA, Count1S counting, double rB);
  /// Generic function to compute next unkown s coefficient
  template <typename F1, typename F2>
  void setNextsCoefGeneric(doubleV& sCoe, doubleV& gammaTil,
                           const doubleV& gamma, F1 g, F2 b) const;
  /// Compute next unkown s coefficient
  void setNextsCoef(int betaOrd, int gammaROrd, MScheme sType, int p = 1) const;
  /// Compute next unkown s coefficient with lambda terms
  void setNextsCoefLambda(int betaOrd, int gammaROrd, MScheme sType,
                          int p = 1) const;
  /// Compute next unknown _msrDeltaAsy coefficient
  void setNextMSRDeltaCoef(MScheme type, int betaOrd = kMaxRunAlpha,
                           int gammaROrd = kMaxOrderMSR) const;
  /// Compute next unknown _msBarDeltaAsy coefficient
  void setNextMSBarDeltaCoef(int betaOrd = kMaxRunAlpha,
                             int gammaROrd = kMaxOrderMSR) const;
  /// Specific mass anomalous dimension
  /**
   * gamma^nl(alpha_nl) as a function of alpha^nf and log(mu/M)
   * (dir = Direction::Up) or gamma^nf(alpha_nf) as a function of alpha^nl and
   * log(mu/M) (dir = Direction::Down) (output size 5x5)
   */
  doubleG gammaMassDir(Direction dir, MScheme scheme) const;
  /// Coefficients for MSbar mass matching with logs mu / mm (ouput size 5x5)
  doubleG msBarMatchingLog(Direction dir, MScheme scheme) const;
  /// Coefficients for MSbar mass matching without logs (output size 5)
  doubleV msBarMatchingCoef(Direction dir, MScheme scheme) const;
  /// mPole - mMS coefficients with alpha logs (output size 4x4)
  doubleG msDeltaAlphaLog(MScheme scheme) const;
  /// PS - pole relation with logs, rIR = muIR/muF (output size 4x4)
  doubleG psDeltaLog(double rIR) const;
  /// RS - pole relation with logs (output size 4x4)
  doubleG rsDeltaLog(double N12var, int nRS) const;
  /// 1S - pole relation with logs (output size 4x4)
  doubleG oneSDeltaLog(double alpha) const;
  /// Generates logs of the 1S - pole relation (output size 4x4, input size 4)
  doubleG generate1SLogs(const doubleV& del) const;
  /// MS-bar pole relation with logs, arbitrary number of elements
  doubleG msBarDeltaAsyPiece(int length, int betaOrd = kMaxRunAlpha,
                             int gammaROrd = kMaxRunMSR,
                             int gammaMOrd = kMaxRunMSbar) const;
  /// Returns s coefficients
  double sCoef(int n, int betaOrd, int gammaROrd, MScheme sType, bool isStrict,
               int p = 1) const;
  /// Returns s coefficients with lambda terms
  double sCoefLambda(int n, int betaOrd, int gammaROrd, MScheme sType,
                     bool isStrict, int p = 1) const;
  // MSbar mass running kernel, one-parameter version used for RGI mass
  template <typename T = double>
  T wTilde(RunMethod method, int runMass, double a0, T a1 = kDefault,
           int betaOrd = kDefault) const;
  /// Analytic R-evolution for a0 = alphaS(R0), method Expanded or Partfrac
  template <typename T>
  T deltaR(MScheme type, RunMethod method, double a0, T a1, int p = 1) const;
};

////////////////////////////////////////////////////////////////////////////////
// Function template definitions
////////////////////////////////////////////////////////////////////////////////

//------------------------------------------------------------------------------
template <typename T>
T AnDim::iter(const doubleV& cCoef, T aMu, double a0, T aLLInv, T* last_corr) {
  T res = aLLInv, aMuPow = 1.0;
  double a0Pow = 1.0;
  int n = cCoef.size() - 1;

  if (n > 0) {
    res += cCoef[1] * std::log(aMu / a0);
  }

  if (n < 2) {
    if (last_corr) {
      *last_corr = res;
    }
    return res;
  }

  for (int i = 1; i < n; ++i) {
    aMuPow *= aMu;
    a0Pow *= a0;
    res += cCoef[i + 1] * (aMuPow - a0Pow) / double(i);
  }

  if (last_corr) {
    *last_corr = cCoef[n] * (aMuPow - a0Pow) / double(n - 1) / res;
  }

  return res;
}

//------------------------------------------------------------------------------
template <typename F>
void AnDim::setNextglGeneric(doubleV& gl, double p, F bHatIn) const {
  int nextCoe = gl.size();

  if (nextCoe == 0) {
    gl.push_back(1.0);
  } else {
    int m1pow = -1;
    gl.push_back(0.0);
    for (int i = 0; i < nextCoe; ++i) {
      m1pow *= -1;
      gl.back() += m1pow * bHatIn(i + 2) * gl[nextCoe - 1 - i];
    }
    gl.back() *= double(p) / nextCoe;
  }
}

//------------------------------------------------------------------------------
template <typename T>
T AnDim::alphaGeneric(RunMethod method, double muIn, double aMuIn, T mu) const {
  if (_betaOrd == 0) {
    return aMuIn;
  }
  T aLLInv, res = 0.0, corr;
  auto cCoefL = std::bind(&AnDim::cCoefLambda, this, std::placeholders::_1,
                          _betaOrd, _betaIsStrict);

  if (method == RunMethod::Iterative || method == RunMethod::Exact) {
    aLLInv = 1.0 / aMuIn + std::log(mu / muIn) * _beta[0] * 0.5 / M_PI;
    res = 1.0 / aLLInv;
    if (_betaOrd == 1 && method == RunMethod::Iterative) {
      return res;
    }
  } else {
    aLLInv = 1.0 / aMuIn;
  }

  if (method == RunMethod::Expanded || method == RunMethod::Inverse) {
    T aLL, sum;
    rac::Grid<T> b(2, _betaOrd - 1), bb(_betaOrd - 1, _betaOrd - 1);
    std::vector<T> aLList(_betaOrd);
    doubleV a0List(_betaOrd);
    double a0 = aMuIn / kFourPi;

    for (int i = 0; i < _betaOrd - 1; ++i) {
      bb[i][0] = 1.0;
    }

    if (_betaOrd > 0) {
      aLL = 1.0 / (1.0 / a0 + 2.0 * std::log(mu / muIn) * _beta[0]);
      b[0][0] = 1.0;
    } else {
      aLL = a0;
    }

    if (_betaOrd > 1) {
      b[0][1] = aLL * cCoefL(1) * std::log(aLL / a0);
    }

    if (_betaOrd > 2) {
      a0List[0] = 1.0;
      aLList[0] = 1.0;
      for (int i = 1; i < _betaOrd; ++i) {
        a0List[i] = a0List[i - 1] * a0;
        aLList[i] = aLList[i - 1] * aLL;
      }
    }

    for (int n = 2; n < _betaOrd; ++n) {
      sum = 0.0;

      for (int j = 1; j < n - 1; ++j) {
        sum += double(j) * b[1][j] * b[0][n - 1 - j];
      }

      b[1][n - 1] = b[0][n - 1] - sum / double(n - 1);

      for (int i = 1; i < _betaOrd - 1; ++i) {
        sum = 0.0;
        for (int j = 0; j < n - 1; ++j) {
          sum += b[0][n - 1 - j] * bb[i][j];
        }
        bb[i][n - 1] = bb[i - 1][n - 1] - sum;
      }

      for (int i = 2; i <= n; ++i) {
        b[0][n] += cCoefL(i) * aLList[i] * bb[i - 1][n - i] / double(i - 1);
      }

      b[0][n] += -cCoefL(n) * aLL * a0List[n - 1] / double(n - 1) -
                 cCoefL(1) * aLL * b[1][n - 1];
    }

    if (method == RunMethod::Expanded) {
      sum = std::accumulate(&b[0][0], &b[0][0] + _betaOrd, T(0.0));
      res = kFourPi * aLL / sum;
    } else {
      if (_betaOrd > 1) {
        bb[1][_betaOrd - 1] = 0.0;

        for (int i = 0; i < _betaOrd - 1; ++i) {
          bb[1][_betaOrd - 1] -= bb[1][i] * b[0][_betaOrd - 1 - i];
        }
      }

      res = std::accumulate(&bb[1][0], &bb[1][0] + _betaOrd, T(0.0));
      res *= kFourPi * aLL;
    }

  } else if (method == RunMethod::Iterative) {
    double four = 1.0;
    doubleV coe(_betaOrd);

    coe[0] = cCoefL(0);
    for (int i = 1; i < _betaOrd; ++i) {
      four *= kFourPi;
      coe[i] = cCoefL(i) / four;
    }

    for (int i = 1; i <= kMaxIter; ++i) {
      corr = 1.0 / iter(coe, res, aMuIn, aLLInv);
      if (std::abs(1.0 - res / corr) < _precisionGoal) {
        break;
      } else if (i == kMaxIter) {
        error(ErrorType::Iter);
        return std::nan("");
      }
      res = corr;
    }

  } else if (method == RunMethod::Exact) {
    double four = 1.0;
    doubleV coe{cCoefL(0)};
    int k = 1;
    auto addCoef = [&]() {
      four *= kFourPi;
      coe.push_back(cCoefL(k) / four);
      ++k;
    };
    while (k < _betaOrd) {
      addCoef();
    }
    T last_corr = 1;
    for (int i = 1; i <= kMaxIter; ++i) {
      corr = 1.0 / iter(coe, res, aMuIn, aLLInv, &last_corr);
      if (std::abs(last_corr) > _precisionGoal) {
        addCoef();
        --i;
      } else if (std::isnan(std::abs(corr)) || k > kMaxIter || i == kMaxIter) {
        res = std::nan("");
        break;
      } else if (std::abs(1.0 - res / corr) < _precisionGoal) {
        break;
      } else {
        res = corr;
      }
    }
  } else {
    res = std::nan("");
  }

  return res;
}

//------------------------------------------------------------------------------
template <typename T>
T AnDim::inteCorre(double a, double t0, T t1, int p) const {
  double den = 1.0 + a, fac = 1.0, t0pow = std::pow(-t0, den);
  T t1pow = std::pow(-t1, den), res, corr;
  int i = 0;

  if (std::abs(den) < kMaxPrec) {
    res = std::log(t0 / t1);
  } else {
    res = (t0pow - t1pow) / den;
  }

  for (int j = 0; j < kMaxIter; ++j) {
    den += 1.0;
    fac *= (++i) / double(p);
    t0pow *= -t0;
    t1pow *= -t1;

    if (std::abs(den) < kMaxPrec) {
      corr = std::log(t0 / t1) / fac;
    } else {
      corr = (t0pow - t1pow) / den / fac;
    }

    res += corr;
    if (std::abs(corr / res) <= _precisionGoal) {
      break;
    } else if (j == kMaxIter - 1) {
      error(ErrorType::Iter);
      return std::nan("");
    }
  }

  return res;
}

//------------------------------------------------------------------------------
template <typename F1, typename F2>
void AnDim::setNextsCoefGeneric(doubleV& sCoe, doubleV& gammaTil,
                                const doubleV& gamma, F1 g, F2 b) const {
  int nextCoe = sCoe.size();
  int gammaROrd = gamma.size();

  if (nextCoe == 0) {
    if (gammaTil.empty()) {
      double beta02pow = 1.0;
      for (auto ga : gamma) {
        beta02pow *= 2 * _beta[0];
        gammaTil.push_back(ga / beta02pow);
      }
    }
    sCoe.push_back(gammaTil[0]);
  } else {
    double s = 0.0;
    for (int k = 0; k <= std::min(nextCoe, gammaROrd - 1); ++k) {
      double isum = 0;
      double m1pow = -1;
      for (int i = 0; i <= nextCoe - k; ++i) {
        m1pow *= -1;
        isum += m1pow * g(nextCoe - i - k) * b(i);
      }
      s += gammaTil[k] * isum;
    }
    sCoe.push_back(s);
  }
}

//------------------------------------------------------------------------------
template <typename T>
T AnDim::wTilde(RunMethod method, int runMass, double a0, T a1,
                int betaOrd) const {
  bool a1Default = isDefault(std::complex<double>(a1).real());
  if (!a1Default && (std::abs(1.0 - a0 / a1) < kMaxPrec)) {
    return 0.0;
  }
  if (isDefault(betaOrd)) {
    betaOrd = _betaOrd;
  } else if (betaOrd < 1) {
    betaOrd = 1;
  } else if (betaOrd > kMaxRunAlpha) {
    betaOrd = kMaxRunAlpha;
  }

  T res = 0.0, a1Pi;
  double a0Pi;
  if (runMass > kMaxRunMSbar) {
    runMass = kMaxRunMSbar;
  }

  if (runMass <= 0) {
    return 0.0;
  }

  if (method == RunMethod::Expanded) {
    if (a1Default) {
      a1 = M_PI;
    }
    res = _gammaMassLambda[0] * std::log(a1 / a0);

    if (a1Default) {
      a1 = 0;
    }
    if (runMass > 1) {
      a0Pi = a0 / kFourPi;
      a1Pi = a1 / kFourPi;
    } else {
      a0Pi = 0.0;
      a1Pi = 0.0;
    }

    for (int i = 1; i < runMass; ++i) {
      double cc = 0;
      for (int j = 0; j <= i; ++j) {
        cc += _gammaMassLambda[j] * cCoefLambda(i - j, betaOrd, false);
      }
      res += cc * (a1Pi - a0Pi) / double(i);
      a0Pi *= a0 / kFourPi;
      a1Pi *= a1 / kFourPi;
    }

  } else if (method == RunMethod::Exact) {
    if (a1Default) {
      a1 = M_PI;
    }
    res = _gammaMassLambda[0] * std::log(a1 / a0);
    if (a1Default) {
      a1 = 0;
    }
    a0Pi = a0 / kFourPi;
    a1Pi = a1 / kFourPi;
    T corr;
    doubleV& isum = _coefMap[betaOrd]._wTildeSum[runMass];
    for (int n = 1; n <= kMaxIter; ++n) {
      if (int(isum.size()) < n) {
        isum.push_back(0.0);
        for (int i = 0; i <= std::min(n, runMass - 1); ++i) {
          isum[n - 1] +=
              cCoefLambda(n - i, betaOrd, true) * _gammaMassLambda[i];
        }
      }
      corr = (a1Pi - a0Pi) / double(n) * isum[n - 1];
      res += corr;
      if (std::abs(corr / res) < _precisionGoal && n >= runMass) {
        break;
      } else {
        a1Pi *= a1 / kFourPi;
        a0Pi *= a0 / kFourPi;
      }
      if (n == kMaxIter) {
        res = std::nan("");
        break;
      }
    }
  } else {
    error(ErrorType::Keyword);
    return std::nan("");
  }

  if (a1Default) {
    res *= -1.0;
  }

  return -res / _beta[0];
}

//------------------------------------------------------------------------------
template <typename T>
T AnDim::deltaR(MScheme type, RunMethod method, double a0, T a1, int p) const {
  double t0 = -2 * M_PI / _beta[0];
  T t1, corr, res = 0.0;
  int gammaROrd = _gammaROrd;
  int betaOrd = _betaOrd;
  double bHat1 = (betaOrd < 1) ? 0.0 : p * bHatLambda(1, betaOrd, _sIsStrict);

  t1 = t0 / a1;
  t0 /= a0;

  if (method == RunMethod::Expanded) {
    for (int i = 1; i <= kMaxIter; ++i) {
      corr = inteCorre(-bHat1 - i, t0, t1, p) *
             sCoefLambda(i - 1, betaOrd, gammaROrd, type, _sIsStrict, p);
      res += corr;
      if (std::abs(corr / res) <= _precisionGoal && i >= gammaROrd) {
        break;
      } else if (i == kMaxIter) {
        res = std::nan("");
        break;
      }
    }
  } else if (method == RunMethod::Partfrac) {
    T et1 = std::exp(-t1 * double(p)) * std::pow(-t1, -bHat1);
    double et0 = std::exp(-t0 * double(p)) * std::pow(-t0, -bHat1);
    doubleP it0Pow(-1.0 / t0);
    rac::Power<T> it1Pow(-1.0 / t1);
    if (_poch.count(p) < 1) {
      _poch.emplace(std::piecewise_construct, std::forward_as_tuple(p),
                    std::forward_as_tuple(1 + bHat1));
    }
    if (p == 1) {
      T corrTmp;
      corr = inteCorre(-bHat1 - 1, t0, t1, p);
      res = corr * sCoefLambda(0, betaOrd, gammaROrd, type, _sIsStrict, p);
      for (int j = 1; j < kMaxIter; ++j) {
        corr += (et1 * it1Pow[j] - et0 * it0Pow[j]) * _poch[p][j - 1];
        corrTmp = corr *
                  sCoefLambda(j, betaOrd, gammaROrd, type, _sIsStrict, p) /
                  _poch[p][j];
        res += corrTmp;
        if (std::abs(corrTmp / res) < _precisionGoal && j + 1 >= gammaROrd) {
          break;
        } else if (j == kMaxIter - 1) {
          res = std::nan("");
          break;
        }
      }
    } else {
      T term2;
      doubleP pPow(p);
      T sum1 = inteCorre(-bHat1 - 1, t0, t1, p);
      for (int j = 0; j < kMaxIter; ++j) {
        term2 = 0.0;
        for (int i = 1; i <= j; ++i) {
          term2 += (et1 * it1Pow[i] - et0 * it0Pow[i]) * _poch[p][i - 1] *
                   pPow(j - i);
        }
        corr = sum1 * pPow[j] + term2;
        corr *= sCoefLambda(j, betaOrd, gammaROrd, type, _sIsStrict, p) /
                _poch[p][j];
        res += corr;
        if (std::abs(corr / res) < _precisionGoal && j + 1 >= gammaROrd) {
          break;
        } else if (j == kMaxIter - 1) {
          res = std::nan("");
          break;
        }
      }
    }
  } else {
    error(ErrorType::Keyword);
    return std::nan("");
  }

  return res;
}

////////////////////////////////////////////////////////////////////////////////
// Inline function definitions
////////////////////////////////////////////////////////////////////////////////

//------------------------------------------------------------------------------
inline double AnDim::bHat(int n, int betaOrd, bool isStrict) const {
  if ((isStrict == false && n >= betaOrd) || n < 0) {
    return 0.0;
  }

  while (unsigned(n) >= _coefMap[betaOrd]._bHat.size()) {
    setNextbHatc(betaOrd);
  }
  return _coefMap[betaOrd]._bHat[n];
}

//------------------------------------------------------------------------------
inline double AnDim::bHatLambda(int n, int betaOrd, bool isStrict) const {
  if ((isStrict == false && n >= betaOrd) || n < 0) {
    return 0.0;
  }

  while (unsigned(n) >= _coefMap[betaOrd]._bHatLambda.size()) {
    setNextbHatcLambda(betaOrd);
  }
  return _coefMap[betaOrd]._bHatLambda[n];
}

//------------------------------------------------------------------------------
inline double AnDim::cCoef(int n, int betaOrd, bool isStrict) const {
  if ((isStrict == false && n >= betaOrd) || n < 0) {
    return 0.0;
  }

  while (unsigned(n) >= _coefMap[betaOrd]._cCoef.size()) {
    setNextbHatc(betaOrd);
  }
  return _coefMap[betaOrd]._cCoef[n];
}

//------------------------------------------------------------------------------
inline double AnDim::cCoefLambda(int n, int betaOrd, bool isStrict) const {
  if ((isStrict == false && n >= betaOrd) || n < 0) {
    return 0.0;
  }

  while (unsigned(n) >= _coefMap[betaOrd]._cCoefLambda.size()) {
    setNextbHatcLambda(betaOrd);
  }
  return _coefMap[betaOrd]._cCoefLambda[n];
}

//------------------------------------------------------------------------------
inline double AnDim::gl(int n, int betaOrd, bool isStrict, int p) const {
  if ((isStrict == false && n >= betaOrd - 1) || n < 0) {
    return 0.0;
  } else if (p == 0) {
    return n == 0 ? 1.0 : 0.0;
  }

  while (unsigned(n) >= _coefMap[betaOrd]._gl[p].size()) {
    setNextgl(betaOrd, p);
  }
  return _coefMap[betaOrd]._gl[p][n];
}

//------------------------------------------------------------------------------
inline double AnDim::glLambda(int n, int betaOrd, bool isStrict, int p) const {
  if ((isStrict == false && n >= betaOrd - 1) || n < 0) {
    return 0.0;
  } else if (p == 0) {
    return n == 0 ? 1.0 : 0.0;
  }

  while (unsigned(n) >= _coefMap[betaOrd]._glLambda[p].size()) {
    setNextglLambda(betaOrd, p);
  }
  return _coefMap[betaOrd]._glLambda[p][n];
}

//------------------------------------------------------------------------------
inline double AnDim::gTilde(int n, int betaOrd, bool isStrict, int p) const {
  return gl(n, betaOrd, isStrict, -p);
}

//------------------------------------------------------------------------------
inline double AnDim::gTildeLambda(int n, int betaOrd, bool isStrict,
                                  int p) const {
  return glLambda(n, betaOrd, isStrict, -p);
}

}  // namespace revo

#endif  // ANDIM_H

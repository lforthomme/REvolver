////////////////////////////////////////////////////////////////////////////////
/// \file Core.h
/// \brief Interface of class Core
/// \author Andre Hoang, Christopher Lepenik, Vicent Mateu
///
// Copyright:
//    Copyright (C) 2020 Andre Hoang, Christopher Lepenik, Vicent Mateu
//
//    This file is part of REvolver.
//
//    REvolver is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    REvolver is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with REvolver.  If not, see <https://www.gnu.org/licenses/>.
////////////////////////////////////////////////////////////////////////////////

#ifndef CORE_H
#define CORE_H

#include "Alpha.h"
#include "AnDim.h"
#include "Global.h"
#include "Mass.h"
#include "TypeDef.h"

namespace revo {

////////////////////////////////////////////////////////////////////////////////
/// \class Core
///
/// \brief Provides core functionality.
/// \author Christopher Lepenik and Vicent Mateu
///
/// This class combines all features of the classes AnDim, Alpha and Mass
/// (available via the functions in the section "Access to member objects") and
/// provides additional features.
///
/// Every Core object is defined by it's scenario parameters, specified at
/// construction.
///
/// The constituents of the physical scenario can be used to extract the
/// quantities at specific scales and flavor numbers, for different
/// renormalization schemes etc. Also, several coefficients like anomalous
/// dimensions can be extracted from the AnDim objects.\n
/// The interface of the Core object also allows to add masses to the current
/// scenario or using a second Core object to copy its state to it and add
/// masses there.
////////////////////////////////////////////////////////////////////////////////

class Core {
 public:
  /**
   * \name Constructors
   * \{
   */
  /// Full constructor for a scenario including massive flavors
  /**
   * Constructs a Core object for a QCD running quark mass and coupling setting.
   *
   * \p mPar A sorted vector of RunPar structs, specifying the masses of heavy
   *         quarks with standard running masses in the order of increasing
   *         values. Each RunPar contains the flavor number scheme, the running
   *         mass value and the respective scale.
   * \p fMatch A vector containing elements <tt>{f1, f2, ...}</tt>, where
   *           \p fn specifies that the flavor matching scale related for the
   *           n-th lightest massive quark threshold is \p fn times the standard
   *           running mass. Default: all \p fn set to 1.0.
   *
   * For a documentation of the other input paramers see
   * Alpha::Alpha(int nTot, int nm, const RunPar& alphaPar, int run, double lambda, int order, MScheme scheme, RunMethod method, double precisionGoal)
   * and
   * Mass::Mass(Alpha* alp, int runMSbar, double lambdaMSbar, int orderMSbar, int runMSR, double lambdaMSR, int orderMSR, double msBarDeltaError, RunMethod methodMSbar, RunMethod methodMSR)
   *
   * Default parameters are set if not given explicitly.
   *
   * \note If all flavors are massless,
   *       Core(const RunPar& alphaPar, int runAlpha, double lambdaAlpha, const doubleV& beta, double precisionGoal)
   *       has to be used.
   */
  Core(int nTot, const RunPar& alphaPar, const RunParV& mPar,
       const doubleV& fMatch = doubleV(), int runAlpha = kMaxRunAlpha,
       double lambdaAlpha = 1.0, int orderAlpha = kMaxOrderAlpha,
       int runMSbar = kMaxRunMSbar, double lambdaMSbar = 1.0,
       int orderMSbar = kMaxOrderMSbar, int runMSR = kMaxRunMSR,
       double lambdaMSR = 1.0, int orderMSR = kMaxOrderMSR,
       double msBarDeltaError = 0.0, double precisionGoal = kMaxPrec);
  /// Constructor for a scenario with massless particles only
  /**
   * With the optional input \p beta of arbitrary length one can specify an
   * arbitrary number of custom beta-function coefficients. Their default values
   * are the common MSbar QCD beta-function coefficients up to 5 loops with all
   * higher order coefficients set to zero.
   *
   * Convention: \f$\mu \frac{\mathrm d\alpha_s}{\mathrm d \mu}
   *                = \beta_\mathrm{QCD}(\alpha_s)
   *                = -2\alpha_s\sum_{n=1}\beta_{n-1}
   *                  \left(\frac{\alpha_s}{4\pi}\right)^n\f$
   *
   * The elements of \p beta correspond to the ordered list of coefficients
   * above.
   *
   * Note that adding masses to objects with custom beta-function coefficients
   * is not supported and that, depending on the choice of runAlpha not all
   * coefficients specified may be used.
   *
   * \sa Core(int nTot, const RunPar& alphaPar, const RunParV& mPar, const doubleV& fMatch, int runAlpha, double lambdaAlpha, int orderAlpha, int runMSbar, double lambdaMSbar, int orderMSbar, int runMSR, double lambdaMSR, int orderMSR, double msBarDeltaError, double precisionGoal)
   */
  explicit Core(const RunPar& alphaPar, int runAlpha = kMaxRunAlpha,
                double lambdaAlpha = 1.0, const doubleV& beta = doubleV(),
                double precisionGoal = kMaxPrec);
  /// Copy constructor
  Core(const Core& coreIn);
  /// Move constructor
  Core(Core&& coreIn);
  /// Default constructor
  /**
   * The object created with this constructor is basically useless, but the
   * state of another Core object can be copied to it.
   */
  Core() = default;
  /**
   * \}
   */

  /**
   * \name Assignment operators
   * \{
   */
  /// Copy assignment operator
  Core& operator=(const Core& coreIn);
  /// Move assignment operator
  Core& operator=(Core&& coreIn);
  /**
   * \}
   */

  /// Adds the running mass of a new heavier quark to the scenario.
  /**
   * The added particle has to be the new heaviest flavor in the scenario.
   *
   * \param nf Specifies in which flavor number scheme the running mass value
   *           \p mass is quoted.
   * \param mass Specifies the value of the running mass to be added.
   * \param scale Specifies at which scale the running mass value \p mass is
   *              quoted.
   * \param fnQ Specifies that the flavor matching scale for the additional
   *            flavor is fnQ times the standard running mass. Default: 1.0.
   * \param showOrderError Wether an error message should be shown if the given
   *                       masses are not properly ordered. Default: true.
   *
   * \sa Mass::mMS
   */
  void addMsMass(int nf, double mass, double scale, double fnQ = 1.0,
                 bool showOrderError = true);
  /// \brief Adds a new heavier running mass given as the asymptotic pole mass
  ///        to the scenario.
  /**
   * First, the value of the running mass at a specified scale and in the flavor
   * number scheme where all massive quarks are integrated out is computed from
   * the given asymptotic pole mass. This running mass is then added to the Core
   * object as the new heaviest quark mass using addMsMass.
   *
   * \param mPole Specifies the value of the asymptotic pole mass to be added.
   * \param scale Specifies the scale of the running mass to which the
   *              asymptotic pole mass is converted.
   * \param muA Specifies the scale of the strong coupling used for the
   *            conversion. Default: \p scale.
   * \param method Specifies the prescription used for the definition of the
   *               asymptotic pole mass; available options are
   *               \p PoleMethod::Min for the minimal correction term method,
   *               \p PoleMethod::Range for the range method and
   *               \p PoleMethod::DRange for the corresponding discrete version.
   *               Default: is \p PoleMethod::Min.
   * \param f Specifies a constant larger then unity multiplying the minimal
   *          correction for the method \p PoleMethod::Range or
   *          \p PoleMethod::DRange. Default: 1.25.
   * \param fnQ Specifies that the flavor matching scale for the additional
   *            flavor is fnQ times the standard running mass. Default: 1.0.
   *
   * See <http://arxiv.org/abs/1706.08526> for details.
   *
   * \sa Mass::mPole, addPoleMassFO
   */
  void addPoleMass(double mPole, double scale, double muA = kDefault,
                   PoleMethod method = PoleMethod::Default, double f = 1.25,
                   double fnQ = 1.0);
  /// \brief Adds a new heavier running mass given as an order-dependent pole
  ///        mass to the scenario.
  /**
   * First, the value of the running mass in a specified flavor number scheme at
   * a specified scale is computed from the given order-dependent pole mass.
   * This running mass is then added to the Core object as the new heaviest
   * quark mass using addMsMass.
   *
   * \param mPole Specifies the value of the order-dependent pole mass to be
   *              added.
   * \param nfConv Specifies the flavor number scheme of the running mass to
   *               which the order-dependent pole mass is converted.
   * \param scale Specifies the scale of the running mass to which the
   *              order-dependent pole mass is converted.
   * \param muA Specifies the scale of the strong coupling used in the
   *            conversion formula.
   * \param order Specifies how many perturbative orders should be used for the
   *              conversion to the running mass scheme; for <tt>order > 4</tt>
   *              a renormalon-based asymptotic formula for the coefficients of
   *              the asymptotic series is used.
   * \param fnQ Specifies that the flavor matching scale for the additional
   *            flavor is fnQ times the standard running mass. Default: 1.0.
   *
   * \sa Mass::mPoleFO, addPoleMass
   */
  void addPoleMassFO(double mPole, int nfConv, double scale, double muA,
                     int order, double fnQ = 1.0);
  /// Adds a new heavier running mass given in the PS scheme to the scenario.
  /**
   * First, the value of the running mass in a specified flavor number scheme at
   * a specified scale is computed from the given PS mass. This running mass is
   * then added to the Core object as the new heaviest quark mass using
   * addMsMass.
   *
   * The value of the PS mass is always quoted in the flavor scheme of the
   * number of flavors contained in the Core object.
   *
   * \param mPS Specifies the value of the PS mass to be added.
   * \param muF Specifies at which subtraction scale the PS mass value \p mPS is
   *            quoted.
   * \param nfConv Specifies the flavor number scheme of the running mass
   *               to which the PS mass is converted; possible values are the
   *               total number of flavors of the Core object before adding the
   *               mass (the running mass is
   *               in the MSR scheme) or the total number of flavor of the Core
   *               object after adding the mass (the running mass is in the
   *               MSbar scheme); if the running mass is in the MSbar scheme,
   *               the QCD coupling is converted to the total flavor number of
   *               the Core object before adding the mass. Default: total flavor
   *               number of the Core object before adding the mass.
   * \param scale Specifies the scale of the running mass to which the PS mass
   *              is converted. Default: \p muF.
   * \param muA Specifies the scale of the strong coupling used for the
   *            conversion to the running mass. Default: muF.
   * \param rIR Specifies the IR scale employed in the 4-loop term of the
   *            pole-PS mass relation: <tt>rIR = muIR/muF</tt>. Default: 1.
   * \param order Specifies how many perturbative orders should be used in the
   *              conversion to the running mass scheme. Default: highest
   *              available order which is 4.
   * \param fnQ Specifies that the flavor matching scale for the additional
   *            flavor is fnQ times the standard running mass. Default: 1.0.
   *
   * \sa Mass::mPS
   */
  void addPSMass(double mPS, double muF, int nfConv = kDefault,
                 double scale = kDefault, double muA = kDefault, double rIR = 1,
                 int order = kMaxOrderPs, double fnQ = 1.0);
  /// Adds a new heavier running mass given in the 1S scheme to the scenario.
  /**
   * First, the value of the running mass in a specified flavor number scheme at
   * a specified scale is computed from the given 1S mass. This running mass is
   * then added to the Core object as the new heaviest quark mass using
   * addMsMass.
   *
   * The value of the 1S mass is always quoted in the flavor scheme of the
   * number of flavors contained in the Core object.
   *
   * \param m1S Specifies the value of the 1S mass to be added.
   * \param nfConv Specifies the flavor number scheme of the running mass
   *               to which the 1S mass is converted; possible values are the
   *               total number of flavors of
   *               the Core object before adding the the mass (the running mass
   *               is in the MSR scheme) or the total number of flavor of the
   *               Core object after adding the mass (the running mass is in the
   *               MSbar scheme); if the running mass is in the MSbar scheme,
   *               the QCD coupling is converted to the total flavor number of
   *               the Core object before adding the mass. Default: total flavor
   *               number of the Core object before adding the mass.
   * \param scale Specifies the scale of the running mass to which the 1S mass
   *              is converted. Default: if
   *              the MSR scheme is used (determined by nfConv) the default
   *              scale is the natural scale of the 1S scheme, i.e. the inverse
   *              Bohr radius; in the MSbar case standard running mass is used
   *              as default.
   * \param counting Specifies the expansion scheme used for the conversion to
   *                 the running mass;
   *                 available options: \p Count1S::Nonrelativistic,
   *                 \p Count1S::Relativistic. Default:
   *                 \p Count1S::Nonrelativistic for MSR and
   *                 \p Count1S::Relativistic for MSbar; for nonrelativistic
   *                 counting the leading correction is
   *                 \f$\mathcal O(\alpha_s^2)\f$ but counted as order 1; see
   *                 Sec. 5.2 of arXiv:1704.01580. Nonrelativistic counting for
   *                 the MSbar scheme is not supported.
   * \param muA Specifies the scale of the strong coupling used for the
   *            conversion to the running mass. Default: \p scale.
   * \param order Specifies how many perturbative orders should be used in the
   *              conversion to the running mass scheme. Default: highest
   *              available order which is 4.
   * \param fnQ Specifies that the flavor matching scale for the additional
   *               flavor is fnQ times the standard running mass. Default: 1.0.
   *
   * \sa Mass::m1S, Mass::mBohr
   */
  void add1SMass(double m1S, int nfConv = kDefault, double scale = kDefault,
                 Count1S counting = Count1S::Default, double muA = kDefault,
                 int order = kMaxOrder1s, double fnQ = 1.0);
  /// Adds a new heavier running mass given in the RGI scheme to the scenario.
  /**
   * First, the value of the standard running mass is computed from the given
   * RGI mass. This mass is then added to the Core object as the new heaviest
   * quark mass using addMsMass.
   *
   * \param mRGI Specifies the value of the RGI mass to be added.
   * \param order Specifies the loop order of the beta-function and MSbar
   *              mass anomalous dimensions entering the conversion formula from
   *              the standard running mass. Default: highest available order,
   *              which is 5.
   * \param fnQ Specifies that the flavor matching scale for the additional
   *            flavor is fnQ times the standard running mass. Default: 1.0.
   *
   * \sa Mass::mRGI
   */
  void addRGIMass(double mRGI, int order = kMaxRunMSbar, double fnQ = 1.0);
  /// Adds a new heavier running mass given in the RS scheme to the scenario.
  /**
   * First, the value of the running mass in a specified flavor number scheme at
   * a specified scale is computed from the given renormalon subtracted mass.
   * This running mass is then added to the Core object as the new heaviest
   * quark mass using addMsMass.
   *
   * The value of the RS mass is always quoted in the flavor scheme of the
   * number of flavors contained in the Core object.
   *
   * \param mRS Specifies the value of RS mass to be added.
   * \param scaleRS Specifies at which subtraction scale the RS mass value
   *                \p mRS is quoted.
   * \param nfConv Specifies the flavor number scheme of the running mass
   *               to which the RS mass is converted; possible values are the
   *               total number of flavors of
   *               the Core object before adding the mass (the running mass is
   *               in the MSR scheme) or the total number of flavor of the Core
   *               object after adding the mass (the running mass is in the
   *               MSbar scheme); if the running mass is in the MSbar scheme,
   *               the QCD coupling is converted to the total flavor number of
   *               the Core object before adding the mass. Default: total flavor
   *               number of the Core object before adding the mass.
   * \param scale Specifies the scale of the running mass to which the RS mass
   *              is converted. Default: \p scaleRS.
   * \param muA Specifies the scale of the strong coupling used for the
   *            conversion to the running mass. Default: \p scaleRS.
   * \param order Specifies how many perturbative orders should be used in the
   *              conversion to the running mass scheme Default: highest
   *              available order which is 4.
   * \param nRS Specifies the number of terms (used in the perturbative
   *            construction of the Borel function and the normalization N12)
   *            for the calculation of the coefficients of the pole-RS mass
   *            perturbation series. Default: highest available order which is
   *            4.
   * \param N12 Specifies the pole mass renormalon normalization constant
   *            employed in the pole-RS mass relation. Default: value computed
   *            by the sum rule formula employed by the routine N12, summing up
   *            nRS terms in the sum rule series using all available information
   *            on the beta-function and anomalous dimension of the MSR mass.
   * \param fnQ Specifies that the flavor matching scale for the additional
   *            flavor is fnQ times the standard running mass. Default: 1.0.
   *
   * \sa Mass::mRS
   */
  void addRSMass(double mRS, double scaleRS, int nfConv = kDefault,
                 double scale = kDefault, double muA = kDefault,
                 int order = kMaxRunMSR, int nRS = kMaxRunAlpha - 1,
                 double N12 = kDefault, double fnQ = 1.0);
  /// \brief Adds a new heavier running mass given in the kinetic scheme to the
  ///        scenario.
  /**
   * First, the value of the running mass in a specified flavor number scheme at
   * a specified scale is computed from the given kinetic mass.
   * This running mass is then added to the Core object as the new heaviest
   * quark mass using addMsMass.
   *
   * The value of the kinetic mass is always quoted in the flavor scheme of the
   * number of flavors contained in the Core object.
   *
   * \param mKin Specifies the value of kinetic mass to be added.
   * \param scaleKin Specifies at which Wilsonian cut-off scale the kinetic mass
   *                 value \p mKin is quoted.
   * \param nfConv Specifies the flavor number scheme of the running mass
   *               to which the kinetic mass is converted; possible values are
   *               the total number of flavors of
   *               the Core object before adding the mass (the running mass is
   *               in the MSR scheme) or the total number of flavor of the Core
   *               object after adding the mass (the running mass is in the
   *               MSbar scheme); if the running mass is in the MSbar scheme,
   *               the QCD coupling is converted to the total flavor number of
   *               the Core object before adding the mass. Default: total flavor
   *               number of the Core object before adding the mass.
   * \param scale Specifies the scale of the running mass to which the kinetic
   *              mass is converted. Default: <tt>2 * scaleKin</tt>.
   * \param muA Specifies the scale of the strong coupling used for the
   *            conversion to the running mass. Default: <tt>2 * scaleKin</tt>.
   * \param order Specifies how many perturbative orders should be used in the
   *              conversion to the running mass scheme Default: highest
   *              available order which is 3.
   * \param fnQ Specifies that the flavor matching scale for the additional
   *            flavor is fnQ times the standard running mass. Default: 1.0.
   *
   * \sa Mass::mKin
   */
  void addKinMass(double mKin, double scaleKin, int nfConv = kDefault,
                  double scale = kDefault, double muA = kDefault,
                  int order = kMaxOrderKinetic, double fnQ = 1.0);

  /**
   * \name Access to member objects
   * \{
   */
  /// Returns a constant Mass object.
  const Mass& masses() const;
  /// Returns a constant Alpha object.
  const Alpha& alpha() const;
  /// Overloads alpha to make coupling extraction more intuitive
  template <typename T>
  T alpha(T mu, int nf = kDefault) const {
    return alpha()(mu, nf);
  }
  /// Returns a constant AnDim object defined for the given number of flavors.
  const AnDim& andim(int nf) const;
  /**
   * \}
   */

  /**
   * \name Access to parameters set by constructor
   * \{
   */
  /// Returns the total number of flavors.
  int nTot() const;
  /// Returns standard running masses.
  const doubleV& standardMasses() const;
  /// Returns f-factors used for varying the matching scales.
  const doubleV& fMatch() const;
  /// Returns order parameters.
  int getOrder(OrderPar para) const;
  /// Returns lambda parameters.
  double getLambda(LambdaPar para) const;
  /// Returns msBarDeltaError.
  double msBarDeltaError() const;
  /// Returns precisionGoal.
  double precisionGoal() const;
  /// Returns beta function coefficients.
  const doubleV& betaCoefs(int nf) const;
  /// True if the object uses a user-defined beta function
  bool betaIsUserDef() const;
  /**
   * \}
   */

 private:
  //----------------------------------------------------------------------------
  // Private data members
  //----------------------------------------------------------------------------

  int _nTot, _nm, _n0;  ///< Number of flavors: total, massive, massless
  doubleV _fMatch;      ///< Factors multiplying m(m) to get matching scales
  int _runAlpha;        ///< Number of loops used for alpha running
  double _lambdaAlpha;  ///< Scaling parameter probing alpha scale-dependence
  int _orderAlpha;      ///< Number of loops used for alpha matching
  int _runMSbar;        ///< Number of loops used for MSbar mass running
  double _lambdaMSbar;  ///< Scaling parameter probing MSbar scale-dependence
  int _orderMSbar;      ///< Number of loops used for MSbar mass matching
  int _runMSR;          ///< Number of loops used for MSR mass running
  double _lambdaMSR;    ///< Scaling parameter probing MSR scale-dependence
  int _orderMSR;        ///< Number of loops used for MSR mass matching
  double _msBarDeltaError;      ///< used to control error of 4-loop msBarDelta
  double _precisionGoal;        ///< relative precision of iterative algorithms
  RunMethod _methodMSbar;       ///< Method used for MSbar mass running
  RunMethod _methodMSR;         ///< Method used for MSR mass running
  RunMethod _methodAlpha;       ///< Method used for alpha running
  Alpha _alp;                   ///< Alpha object
  Mass _mass;                   ///< Mass object
  const doubleV* _mm{nullptr};  ///< Masses m(m), lightest first
  bool _betaIsUserDef;          ///< true if a custom beta function is used
  bool _validState{false};      ///< true if object construction finished

  //----------------------------------------------------------------------------
  // Private member functions
  //----------------------------------------------------------------------------

  /// Does some construction work
  void constrHelp(const RunParV& mPar, bool showOrderError = true);
  /// Raises mass counts in all places; clears other information
  void addMass();

  /// \brief Lowers the flavor number of a given MSbar mass and sets the new
  ///        scale to the next lighter MSbar mass
  double lowerMSbarFlavor(int nf, double mMSbarmunf, double mu) const;
};

}  // namespace revo

#endif  // CORE_H

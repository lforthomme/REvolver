////////////////////////////////////////////////////////////////////////////////
/// \file Evolving.h
/// \brief Interface of class Evolving
/// \author Andre Hoang, Christopher Lepenik, Vicent Mateu
///
// Copyright:
//    Copyright (C) 2020 Andre Hoang, Christopher Lepenik, Vicent Mateu
//
//    This file is part of REvolver.
//
//    REvolver is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    REvolver is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with REvolver.  If not, see <https://www.gnu.org/licenses/>.
////////////////////////////////////////////////////////////////////////////////

#ifndef EVOLVING_H
#define EVOLVING_H
#define _USE_MATH_DEFINES

#include <cmath>

#include "Alpha.h"
#include "Global.h"
#include "TypeDef.h"

namespace revo {

////////////////////////////////////////////////////////////////////////////////
/// \class Evolving
///
/// \brief Abstract class providing some necessary functionality for
///        R-evolution.
/// \author Andre Hoang, Christopher Lepenik, Vicent Mateu
////////////////////////////////////////////////////////////////////////////////

class Evolving {
 public:
  /**
   * \name Constructors and destructors
   * \{
   */
  /// Constructor with all available options
  /**
   * If not given, standard parameters are set.
   * \param runR The loop order used for R-evolution.
   * \param pR The power of R in R-evolution.
   * \param lambdaR A parameter probing the renormalization scale dependence
   *                of the R-evolution equation. The variation is performed by
   *                expanding the original perturbative series [in powers of
   *                \f$\alpha_s(R)\f$] in terms of
   *                \f$\alpha_s(\mathtt{lambdaR}\times R)\f$, truncating at
   *                order \p runR.
   * \param methodR Method used for the R-evolution. Default:
   *                \p RunMethod::ExactPF.\n
   *                Available options:
   *                  - \p RunMethod::Expanded
   *                  - \p RunMethod::ExpandedPF
   *                  - \p RunMethod::Exact
   *                  - \p RunMethod::ExactPF
   */
  Evolving(int runR, int pR, double lambdaR = 1.0,
           RunMethod methodR = RunMethod::ExactPF);
  /// Default constructor
  Evolving() = default;
  /// Default destructor
  virtual ~Evolving() = default;
  /**
   * \}
   */

  /// Provides access to the Alpha object embedded in the derived class.
  virtual const Alpha& alpha() const = 0;

 protected:
  //----------------------------------------------------------------------------
  // Protected data members
  //----------------------------------------------------------------------------

  int _runR;           ///< The loop order used for R-evolution
  int _powerR;         ///< Power of R in R-evolution
  double _lambdaR;     ///< Scaling param probing R-evolution scale-dependence
  RunMethod _methodR;  ///< Method used for R-evolution

  //----------------------------------------------------------------------------
  // Protected member functions
  //----------------------------------------------------------------------------

  /// Provides access to the Alpha object embedded in the derived class.
  virtual Alpha& alpha() = 0;
  /// Applies given parameters to embedded AnDim objects.
  void applyRParameters();
  /// Returns the renormalon normalization \f$N_{p/2}\f$.
  /**
   * \sa AnDim::Np2
   */
  double renormalonN(int nl, const doubleV* sCoe = nullptr) const;
  /// Returns the renormalon normalization \f$P_{p/2}\f$.
  /**
   * \sa AnDim::Pp2
   */
  double renormalonP(int nl, const doubleV* sCoe = nullptr) const;
  /// Analytic R-evolution (using lQCD)
  template <typename T>
  T diffR(MScheme type, RunMethod method, int nl, double r0, T r1) const;
  /// R-evolution kernel
  template <typename T>
  T rEvol(MScheme type, RunMethod method, int nl, double R0, T R) const;
};

////////////////////////////////////////////////////////////////////////////////
// Member function template definitions
////////////////////////////////////////////////////////////////////////////////

//------------------------------------------------------------------------------
// Protected
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
template <typename T>
T Evolving::diffR(MScheme type, RunMethod method, int nf, double r0,
                  T r1) const {
  if (std::abs(1.0 - r1 / r0) <= kMaxPrec) {
    return 0.0;
  }

  return alpha().andim(nf).deltaR(type, method, alpha()(r0 * _lambdaR, nf),
                                  alpha()(r1 * _lambdaR, nf), _powerR);
}

//------------------------------------------------------------------------------
template <typename T>
T Evolving::rEvol(MScheme type, RunMethod method, int nl, double R0,
                  T R) const {
  double lQCDp =
      std::pow(alpha().lambdaQCD(nl, LambdaConvention::tScheme), _powerR);

  switch (method) {
    case RunMethod::ExactPF:
    case RunMethod::ExpandedPF:
      return lQCDp * diffR(type, RunMethod::Partfrac, nl, R0, R);
      break;

    case RunMethod::Expanded:
    case RunMethod::Exact:
      return lQCDp * diffR(type, RunMethod::Expanded, nl, R0, R);
      break;

    default:
      error(ErrorType::Keyword);
      return 0.0;
  }
}

}  // namespace revo

#endif  // EVOLVING_H

////////////////////////////////////////////////////////////////////////////////
/// \file Grid.h
/// \brief Interface and implementation of class Grid
/// \author Andre Hoang, Christopher Lepenik, Vicent Mateu
///
// Copyright:
//    Copyright (C) 2020 Andre Hoang, Christopher Lepenik, Vicent Mateu
//
//    This file is part of REvolver.
//
//    REvolver is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    REvolver is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with REvolver.  If not, see <https://www.gnu.org/licenses/>.
////////////////////////////////////////////////////////////////////////////////

#ifndef GRID_H
#define GRID_H

#include <algorithm>

namespace rac {

////////////////////////////////////////////////////////////////////////////////
/// \class Grid
///
/// \brief A container acting as a 2-dimensional arrays
/// \author Christopher Lepenik
///
/// Grid is a container for a 2-dimensional array where the same syntax as for a
/// C style array can be used. Member functions for resetting,
/// clearing and filling are provided. Additionally, there are iterators for
/// the whole array and for individual rows.
///
/// Grids can be used as a stand alone object, created with the default
/// constructor followed by a reset, or by giving the number of rows and
/// columns when creating the Grid. Grids can also be used as a wrapper for
/// existing 2-dimensional arrays by providing a pointer to the first element
/// in the constructor or by using the member function makeWrapper.
////////////////////////////////////////////////////////////////////////////////

/// \tparam T Type of container elements
template <typename T>
class Grid {
 public:
  /**
   * \name Constructors and Destructors
   * \{
   */
  /// Default constructor - creates a Grid with zero elements.
  Grid() = default;
  /// Creates a Grid of size \p rows * \p cols with value initialized elements.
  Grid(int rows, int cols);
  /// \brief Creates a Grid of size \p rows * \p cols acting as a wrapper of
  ///        the array for which \p array points to the first element.
  Grid(int rows, int cols, T* array);
  /// Copy constructor
  /**
   * \note even if \p other is a wrapper, this constructor performs a deep copy
   */
  Grid(const Grid& other);
  /// Move constructor
  Grid(Grid&& other);
  /// Destructor
  ~Grid();
  /**
   * \}
   */

  /// Copy assignment operator
  /**
   * \note Even if \p other is a wrapper, this operator performs a deep copy.
   */
  Grid& operator=(const Grid& other);
  /// Move assignment operator
  Grid& operator=(Grid&& other);

  /// Gives pointer to first Grid element.
  T* data();
  /// Grid is deleted and consequently has zero elements.
  void clear();
  /// Grid is reset to size \p rows * \p cols with value initialized elements.
  void reset(int rows, int cols);
  /// Fills the Grid with copies of \p elems.
  void assign(const T& elems);
  /// \brief Transforms the Grid into a wrapper for an \p rows * \p cols array
  ///        for which \p array points to the first element.
  void makeWrapper(int rows, int cols, T* array);

  int numCols() const;  ///< Return the number of columns
  int numRows() const;  ///< Return the number of rows
  int size() const;     ///< Returns total number of elements
  bool empty() const;   ///< Returns \p true if the Grid contains zero elements

  /// Makes 2-dimensional array syntax possible.
  /**
   * Returns a pointer to the first element of row \p row. A second
   * <tt>operator[]</tt> consequently counts through the row elements such
   * that the syntax is equivalent to the one for 2-dimensional C-style arrays.
   */
  T* operator[](int row);
  /// Makes 2-dimensional array syntax in const functions possible.
  const T* operator[](int row) const;

  /**
   * \name Relational operators
   * \{
   */
  bool operator==(const Grid& other) const;
  bool operator!=(const Grid& other) const;
  bool operator<(const Grid& other) const;
  bool operator<=(const Grid& other) const;
  bool operator>(const Grid& other) const;
  bool operator>=(const Grid& other) const;
  /**
   * \}
   */

  /// Grid iterator (pointer to elements)
  using iterator = T*;
  /// constant Grid iterator (pointer to const elements)
  using const_iterator = const T*;

  /**
   * \name Iterators
   * Typical iterators, including ones for individual rows
   * \{
   */
  iterator begin();
  iterator end();
  const_iterator begin() const;
  const_iterator end() const;

  iterator rowBegin(int row);
  iterator rowEnd(int row);
  const_iterator rowBegin(int row) const;
  const_iterator rowEnd(int row) const;
  /**
   * \}
   */

 private:
  int _rows{0};            ///< Number of rows
  int _cols{0};            ///< Number of columns
  bool _isWrapper{false};  ///< Specifies if the Grid is an array wrapper
  T* _array{nullptr};      ///< Underlying array

  void free();  ///< Frees array data
};

////////////////////////////////////////////////////////////////////////////////
// Function definitions
////////////////////////////////////////////////////////////////////////////////

//------------------------------------------------------------------------------
// Constructors and Destructor
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
template <typename T>
Grid<T>::Grid(int rows, int cols)
    : _rows{rows},
      _cols{cols},
      _isWrapper{false},
      _array{new T[_rows * _cols]{}} {}

//------------------------------------------------------------------------------
template <typename T>
Grid<T>::Grid(int rows, int cols, T* array)
    : _rows{rows}, _cols{cols}, _isWrapper{true}, _array{array} {}

//------------------------------------------------------------------------------
template <typename T>
Grid<T>::Grid(const Grid& other)
    : _rows{other._rows},
      _cols{other._cols},
      _isWrapper{false},
      _array{new T[_rows * _cols]} {
  std::copy(other.begin(), other.end(), begin());
}

//------------------------------------------------------------------------------
template <typename T>
Grid<T>::Grid(Grid&& other)
    : _rows{other._rows},
      _cols{other._cols},
      _isWrapper{other._isWrapper},
      _array{other._array} {
  other._array = nullptr;
}

//------------------------------------------------------------------------------
template <typename T>
Grid<T>::~Grid() {
  free();
}

//------------------------------------------------------------------------------
// Assignment operators
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
template <typename T>
Grid<T>& Grid<T>::operator=(const Grid& other) {
  if (&other != this) {
    free();
    _rows = other._rows;
    _cols = other._cols;
    _isWrapper = false;
    _array = new T[_rows * _cols];
    std::copy(other.begin(), other.end(), begin());
  }

  return *this;
}

//------------------------------------------------------------------------------
template <typename T>
Grid<T>& Grid<T>::operator=(Grid&& other) {
  if (&other != this) {
    free();
    _rows = other._rows;
    _cols = other._cols;
    _isWrapper = other._isWrapper;
    _array = other._array;
    other._array = nullptr;
  }

  return *this;
}

//------------------------------------------------------------------------------
// Public member functions
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
template <typename T>
T* Grid<T>::data() {
  return _array;
}

//------------------------------------------------------------------------------
template <typename T>
void Grid<T>::clear() {
  free();
  _array = nullptr;
  _isWrapper = false;
  _rows = 0;
  _cols = 0;
}

//------------------------------------------------------------------------------
template <typename T>
void Grid<T>::reset(int rows, int cols) {
  free();
  _isWrapper = false;
  _rows = rows;
  _cols = cols;
  _array = new T[_rows * _cols]();
}

//------------------------------------------------------------------------------
template <typename T>
void Grid<T>::assign(const T& elems) {
  std::fill(begin(), end(), elems);
}

//------------------------------------------------------------------------------
template <typename T>
void Grid<T>::makeWrapper(int rows, int cols, T* array) {
  free();
  _isWrapper = true;
  _array = array;
  _rows = rows;
  _cols = cols;
}

//------------------------------------------------------------------------------
template <typename T>
int Grid<T>::numCols() const {
  return _cols;
}

//------------------------------------------------------------------------------
template <typename T>
int Grid<T>::numRows() const {
  return _rows;
}

//------------------------------------------------------------------------------
template <typename T>
int Grid<T>::size() const {
  return _rows * _cols;
}

//------------------------------------------------------------------------------
template <typename T>
bool Grid<T>::empty() const {
  return size() == 0;
}

//------------------------------------------------------------------------------
template <typename T>
T* Grid<T>::operator[](int row) {
  return _array + row * _cols;
}

//------------------------------------------------------------------------------
template <typename T>
const T* Grid<T>::operator[](int row) const {
  return _array + row * _cols;
}

//------------------------------------------------------------------------------
// Relational operators
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
template <typename T>
bool Grid<T>::operator==(const Grid& other) const {
  return !(*this < other || other < *this);
}

//------------------------------------------------------------------------------
template <typename T>
bool Grid<T>::operator!=(const Grid& other) const {
  return *this < other || other < *this;
}

//------------------------------------------------------------------------------
template <typename T>
bool Grid<T>::operator<(const Grid& other) const {
  return std::lexicographical_compare(begin(), end(), other.begin(),
                                      other.end());
}

//------------------------------------------------------------------------------
template <typename T>
bool Grid<T>::operator<=(const Grid& other) const {
  return !(other < *this);
}

//------------------------------------------------------------------------------
template <typename T>
bool Grid<T>::operator>(const Grid& other) const {
  return other < *this;
}

//------------------------------------------------------------------------------
template <typename T>
bool Grid<T>::operator>=(const Grid& other) const {
  return !(*this < other);
}

//------------------------------------------------------------------------------
// Iterators
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
template <typename T>
typename Grid<T>::iterator Grid<T>::begin() {
  return _array;
}

//------------------------------------------------------------------------------
template <typename T>
typename Grid<T>::iterator Grid<T>::end() {
  return begin() + _rows * _cols;
}

//------------------------------------------------------------------------------
template <typename T>
typename Grid<T>::const_iterator Grid<T>::begin() const {
  return _array;
}

//------------------------------------------------------------------------------
template <typename T>
typename Grid<T>::const_iterator Grid<T>::end() const {
  return begin() + _rows * _cols;
}

//------------------------------------------------------------------------------
template <typename T>
typename Grid<T>::iterator Grid<T>::rowBegin(int row) {
  return begin() + _cols * row;
}

//------------------------------------------------------------------------------
template <typename T>
typename Grid<T>::iterator Grid<T>::rowEnd(int row) {
  return rowBegin(row) + _cols;
}

//------------------------------------------------------------------------------
template <typename T>
typename Grid<T>::const_iterator Grid<T>::rowBegin(int row) const {
  return begin() + _cols * row;
}

//------------------------------------------------------------------------------
template <typename T>
typename Grid<T>::const_iterator Grid<T>::rowEnd(int row) const {
  return rowBegin(row) + _cols;
}

//------------------------------------------------------------------------------
// Private member functions
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
template <typename T>
void Grid<T>::free() {
  if (!_isWrapper) {
    delete[] _array;
  }
}

}  // namespace rac

#endif  // GRID_H

////////////////////////////////////////////////////////////////////////////////
/// \file InfVector.h
/// \brief Interface and implementation of class InfVector
/// \author Andre Hoang, Christopher Lepenik, Vicent Mateu
///
// Copyright:
//    Copyright (C) 2020 Andre Hoang, Christopher Lepenik, Vicent Mateu
//
//    This file is part of REvolver.
//
//    REvolver is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    REvolver is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with REvolver.  If not, see <https://www.gnu.org/licenses/>.
////////////////////////////////////////////////////////////////////////////////

#ifndef INFVECTOR_H
#define INFVECTOR_H

#include <functional>
#include <vector>

namespace rac {

////////////////////////////////////////////////////////////////////////////////
/// \class InfVector
///
/// \brief Provides objets representing vectors of infinite length.
/// \author Christopher Lepenik
///
/// Provided a callable, extending a vector by one element at a time, an
/// arbitrary element of the \p InfVector can be accessed by round or square
/// brackets. An incrementable iterator class is provided, allowing random
/// access via the plus operator. The underlying vector is extended whenever
/// needed.
/// The \p InfVector class can be used to define lazily evaluated quantities as
/// well.
////////////////////////////////////////////////////////////////////////////////

/// \tparam T The element type of the vector.
template <typename T>
class InfVector {
 public:
  /// Iterator class declaration.
  class iterator;

  /**
   * \name Constructors
   * \{
   */
  /// Default constructor.
  InfVector() = default;
  /// Constructor providing a callable to extend a vector.
  /**
   * The provided callable has to have the signature \p void(std::vector<T>&),
   * extending the input vector by at least one element beyond emptiness.
   */
  InfVector(const std::function<void(std::vector<T>&)>& setNext);
  /// Constructor providing a vector extending callable and an initial element.
  /**
   * \sa InfVector(const std::function<void(std::vector<T>&)>&)
   */
  InfVector(const std::function<void(std::vector<T>&)>& setNext, const T& init);
  /// Constructor providing a callable returning a vector.
  /**
   * The provided callable has to have the signature \p std::vector<T>(). The
   * callable is evaluated when an element is accessed the first time.
   */
  InfVector(const std::function<std::vector<T>()>& setNext);
  /// Copy constructor.
  InfVector(const InfVector<T>& other) = default;
  /// Move constructor.
  InfVector(InfVector<T>&& other) = default;
  /**
   * \}
   */

  /// Copy assignment operator.
  InfVector<T>& operator=(const InfVector<T>& other) = default;
  /// Move assignment operator.
  InfVector<T>& operator=(InfVector<T>&& other) = default;

  /// Provides access to an element of the vector.
  /**
   * Modifying elements is not supported.
   */
  virtual const T& operator[](int ele) const;
  /// Provides access to an element of the vector.
  /**
   * \sa operator[]
   */
  const T& operator()(int ele) const;

  /// Provides access to the underlying vector.
  /**
   * \para ele The minimal number elements of the returned vector .
   */
  std::vector<T>& getVector(int ele = 0);
  /// Provides \p const access to the underlying vector.
  /**
   * \sa std::vector<T>& getVector(int);
   */
  const std::vector<T>& getVector(int ele = 0) const;

  /// Clears the underlying vector.
  void clear();
  /// Returns \p true if the underlying vector is empty, \p false otherwise.
  bool empty() const;

  /// Returns an iterator to the first element.
  iterator begin() const;

 protected:
  /// Extends the underlying vector by one element.
  std::function<void(std::vector<T>&)> _setNext;
  /// Underlying vector.
  mutable std::vector<T> _v;
};

//------------------------------------------------------------------------------
template <typename T>
class InfVector<T>::iterator {
 public:
  /**
   * \name Constructors
   * \{
   */
  /// Default constructor.
  iterator() = default;
  /// Constructs an iterator with resect to an InfVector.
  explicit iterator(const InfVector<T>& p);
  /// Copy constructor.
  iterator(const iterator& other) = default;
  /**
   * \}
   */

  /// Copy assignment operator.
  iterator& operator=(const iterator& other) = default;

  /// Pre-increment operator.
  iterator& operator++();
  /// Post-increment operator.
  const iterator operator++(int);
  /// Addition assignment operator.
  iterator& operator+=(int n);
  /// Plus operator.
  const iterator operator+(int n) const;

  /**
   * \name Relational operators
   * \{
   */
  bool operator==(const iterator& other) const;
  bool operator!=(const iterator& other) const;
  bool operator<(const iterator& other) const;
  bool operator<=(const iterator& other) const;
  bool operator>(const iterator& other) const;
  bool operator>=(const iterator& other) const;
  /**
   * \}
   */

  /// Dereference operator.
  const T& operator*() const;
  /// Member access operator.
  const T* operator->() const;

 private:
  const InfVector<T>* _iv = nullptr;  ///< InfVector object to iterate over.
  int _pos = 0;                       ///< Current iterator position.
};

////////////////////////////////////////////////////////////////////////////////
// Function definitions: InfVector<T>
////////////////////////////////////////////////////////////////////////////////

//------------------------------------------------------------------------------
template <typename T>
InfVector<T>::InfVector(const std::function<void(std::vector<T>&)>& setNext)
    : _setNext{setNext} {}

//------------------------------------------------------------------------------
template <typename T>
InfVector<T>::InfVector(const std::function<void(std::vector<T>&)>& setNext,
                        const T& init)
    : _setNext{setNext}, _v{init} {}

//------------------------------------------------------------------------------
template <typename T>
InfVector<T>::InfVector(const std::function<std::vector<T>()>& setNext)
    : _setNext{[setNext](std::vector<T>& v) { v = setNext(); }} {}

//------------------------------------------------------------------------------
template <typename T>
const T& InfVector<T>::operator[](int ele) const {
  while (int(_v.size()) <= ele) {
    auto s = _v.size();
    _setNext(_v);
    if (s == _v.size()) {
      break;
    }
  }
  return _v[ele];
}

//------------------------------------------------------------------------------
template <typename T>
const T& InfVector<T>::operator()(int ele) const {
  return operator[](ele);
}

//------------------------------------------------------------------------------
template <typename T>
void InfVector<T>::clear() {
  _v.clear();
}

//------------------------------------------------------------------------------
template <typename T>
bool InfVector<T>::empty() const {
  return _v.empty;
}

//------------------------------------------------------------------------------
template <typename T>
std::vector<T>& InfVector<T>::getVector(int ele) {
  if (ele > 0) {
    operator[](ele - 1);
  }
  return _v;
}

//------------------------------------------------------------------------------
template <typename T>
const std::vector<T>& InfVector<T>::getVector(int ele) const {
  if (ele > 0) {
    operator[](ele - 1);
  }
  return _v;
}

//------------------------------------------------------------------------------
template <typename T>
typename InfVector<T>::iterator InfVector<T>::begin() const {
  return iterator(*this);
}

////////////////////////////////////////////////////////////////////////////////
// Function definitions: InfVector<T>::iterator
////////////////////////////////////////////////////////////////////////////////

//------------------------------------------------------------------------------
template <typename T>
InfVector<T>::iterator::iterator(const InfVector<T>& iv) : _iv(&iv), _pos(0) {}

//------------------------------------------------------------------------------
template <typename T>
typename InfVector<T>::iterator& InfVector<T>::iterator::operator++() {
  ++_pos;
  if (_iv->_v.begin() + _pos == _iv->_v.end()) {
    (*_iv)[_iv->_v.size()];
  }
  return *this;
}

//------------------------------------------------------------------------------
template <typename T>
const typename InfVector<T>::iterator InfVector<T>::iterator::operator++(int) {
  iterator retval(*this);
  ++(*this);
  return retval;
}

//------------------------------------------------------------------------------
template <typename T>
const typename InfVector<T>::iterator InfVector<T>::iterator::operator+(
    int n) const {
  iterator iter(*this);
  return (iter += n);
}

//------------------------------------------------------------------------------
template <typename T>
typename InfVector<T>::iterator& InfVector<T>::iterator::operator+=(int n) {
  for (int i = 0; i < n; ++i) {
    ++(*this);
  }
  return *this;
}

//------------------------------------------------------------------------------
template <typename T>
bool InfVector<T>::iterator::operator==(const iterator& other) const {
  return _pos == other._pos && _iv == other._iv;
}

//------------------------------------------------------------------------------
template <typename T>
bool InfVector<T>::iterator::operator!=(const iterator& other) const {
  return !(*this == other);
}

//------------------------------------------------------------------------------
template <typename T>
bool InfVector<T>::iterator::operator<(const iterator& other) const {
  return _pos < other._pos;
}

//------------------------------------------------------------------------------
template <typename T>
bool InfVector<T>::iterator::operator<=(const iterator& other) const {
  return !(other < *this);
}

//------------------------------------------------------------------------------
template <typename T>
bool InfVector<T>::iterator::operator>(const iterator& other) const {
  return other < *this;
}

//------------------------------------------------------------------------------
template <typename T>
bool InfVector<T>::iterator::operator>=(const iterator& other) const {
  return !(*this < other);
}

//------------------------------------------------------------------------------
template <typename T>
const T& InfVector<T>::iterator::operator*() const {
  return *(_iv->_v.begin() + _pos);
}

//------------------------------------------------------------------------------
template <typename T>
const T* InfVector<T>::iterator::operator->() const {
  return &(**this);
}

//////////////////////////////////////////////////////////////////////////////
// Function definitions: Non-members
//////////////////////////////////////////////////////////////////////////////

//------------------------------------------------------------------------------
template <typename T>
const typename InfVector<T>::iterator operator+(
    int n, const typename InfVector<T>::iterator& iter) {
  return iter + n;
}

}  // namespace rac

#endif  // INFVECTOR_H

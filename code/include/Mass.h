////////////////////////////////////////////////////////////////////////////////
/// \file Mass.h
/// \brief Interface of class Mass
/// \author Andre Hoang, Christopher Lepenik, Vicent Mateu
///
// Copyright:
//    Copyright (C) 2020 Andre Hoang, Christopher Lepenik, Vicent Mateu
//
//    This file is part of REvolver.
//
//    REvolver is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    REvolver is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with REvolver.  If not, see <https://www.gnu.org/licenses/>.
////////////////////////////////////////////////////////////////////////////////

#ifndef MASS_H
#define MASS_H

#include <vector>

#include "Alpha.h"
#include "AnDim.h"
#include "Evolving.h"
#include "Global.h"
#include "TypeDef.h"

namespace revo {

////////////////////////////////////////////////////////////////////////////////
/// \class Mass
///
/// \brief Provides the running, matching and conversion of quark masses.
/// \author Christopher Lepenik and Vicent Mateu
///
/// Every Mass object is linked to an Alpha object, which is provided in
/// the constructor. The masses provided by the Alpha object are adopted by the
/// respective Mass object.
///
/// Functionality includes computing values of present masses in the
/// \f$\overline{\mathrm{MS}}\f$ and MSR scheme at aribtrary scales and flavor
/// numbers, and converting these masses to other renormalization schemes like
/// the PS and pole scheme, including lighter massive quark effects. Several
/// methods and parameters can be set for the running and conversion processes.
///
/// \note The provided Alpha object stays connected to the respective Mass
///       object, no copy is made. It is important that the connected Alpha
///       object must not go out of scope if the Mass object is still to be
///       used. This also has to be considered if assignment operators or a copy
///       constructor is used.
////////////////////////////////////////////////////////////////////////////////

class Mass : private Evolving {
  friend class Core;

 public:
  /**
   * \name Constructors
   * \{
   */
  /// Constructor with all available options
  /**
   * If not given, standard parameters are set.
   * \param alp Pointer to an Alpha object. The contained masses are defining
   *            the scenario used in the Mass objects and the set parameters are
   *            used for \f$\alpha_s\f$ running. Note that only Alpha objects
   *            with \p scheme = MScheme::MSbar are supported.
   * \param runMSbar Specifies the loop order used for MSbar mass running.
   *                 Default: highest available order which is 4.
   * \param lambdaMSbar A scaling parameter probing the scale-dependence of the
   *                    MSbar mass evolution. Default: 1.
   * \param orderMSbar Specifies the loop order used for the flavor threshold
   *                   matching of the MSbar mass associated to thresholds
   *                   related to respective heavier quarks. Default: highest
   *                   available order which is 4.
   * \param runMSR Specifies the loop order used for MSR mass running. Default:
   *               highest available order which is 4.
   * \param lambdaMSR A scaling parameter probing the scale-dependence of the
   *                  MSR mass evolution. Default: 1.
   * \param orderMSR Specifies the loop order used for the flavor threshold
   *                 matching of the MSR masses associated to thresholds related
   *                 to the respective massive quark itself and to respective
   *                 lighter massive quarks. Default: highest available order
   *                 which is 4.
   * \param msBarDeltaError Parameter controlling the error of the 4-loop
   *                        coefficient in the pole-MSbar relation. Should be
   *                        varied between -1 and 1 for scanning the standard
   *                        deviation.
   * \param methodMSbar Specifies the numerical method used for the MSbar mass
   *                    evolution. Default:
   *                    \p RunMethod::Exact.\n
   *                    Available options:
   *                      - \p RunMethod::Expanded
   *                      - \p RunMethod::Exact
   * \param methodMSR Method used for the running of MSR masses. Default:
   *                  \p RunMethod::ExactPF.\n
   *                  Available options:
   *                    - \p RunMethod::Expanded
   *                    - \p RunMethod::ExpandedPF
   *                    - \p RunMethod::Exact
   *                    - \p RunMethod::ExactPF
   */
  explicit Mass(Alpha* alp, int runMSbar = kMaxRunMSbar,
                double lambdaMSbar = 1.0, int orderMSbar = kMaxOrderMSbar,
                int runMSR = kMaxRunMSR, double lambdaMSR = 1.0,
                int orderMSR = kMaxOrderMSR, double msBarDeltaError = 0.0,
                RunMethod methodMSbar = RunMethod::Exact,
                RunMethod methodMSR = RunMethod::ExactPF);
  /// Default constructor
  /**
   * The object created with this constructor is basically useless, but the
   * state of another Mass object can be copied to it.
   */
  Mass() = default;
  /**
   * \}
   */

  /// Returns the running mass.
  /**
   * Returns the running mass at the scale \p scale.
   *
   * \tparam T Type of \p scale: \p double or \p std::complex<double>.
   * \param nfIn Specifies for which quark the running mass is returned and
   *             refers to the number of dynamical flavors of the associated
   *             standard running mass.
   * \param scale Specifies the scale of the running mass returned.
   * \param nfOut Specifies that the running mass returned is in the
   *              nfOut-flavor scheme. Default: flavor number associated to
   *              absolute value of \p scale with respect to the flavor matching
   *              scales.
   */
  template <typename T>
  T mMS(int nfIn, T scale, int nfOut = kDefault) const;
  /// Returns the standard running mass.
  /**
   * \param nfIn Specifies for which quark the standard running mass is returned
   *             and refers to the number of its dynamical flavors.
   */
  double mMS(int nfIn) const;
  /// Returns a Potential Subtracted (PS) scheme mass value.
  /**
   * \param nfIn Specifies for which quark the PS mass value is returned and
   *             refers to the number of dynamical flavors of the associated
   *             standard running mass.
   * \param muF Specifies the subtraction scale \f$\mu_F\f$ of the PS scheme.
   * \param nfConv Specifies the flavor number of the running mass
   *               that is used for the conversion; for
   *               <tt>nfConv = nfIn - 1</tt> the MSR scheme is used; for
   *               <tt>nfConv = nfIn</tt> the MSbar scheme is used, where the
   *               strong coupling is rewritten to the <tt>nfIn - 1</tt> flavor
   *               scheme. Default: <tt>nfIn - 1</tt>.
   * \param scale Specifies the scale of the running mass from which the
   *              conversion is determined. Default: muF.
   * \param muA Specifies the scale of the strong coupling used for the
   *            conversion. Default: muF.
   * \param rIR Specifies the IR scale employed in the 4-loop term of the
   *            pole-PS mass relation; <tt>rIR = muIR/muF</tt>. Default: 1.
   * \param order Specifies how many perturbative orders are used in the
   *              conversion. Default: Highest available order which is 4.
   */
  double mPS(int nfIn, double muF, int nfConv = kDefault,
             double scale = kDefault, double muA = kDefault, double rIR = 1,
             int order = kMaxOrderPs) const;
  /// Returns a Renormalon Subtracted (RS) scheme mass value.
  /**
   * \param nfIn Specifies for which quark the RS mass value is returned and
   *             refers to the number of dynamical flavors of the associated
   *             standard running mass.
   * \param scaleRS Specifies the subtraction scale of the RS scheme.
   * \param nfConv Specifies the flavor number of the running mass
   *               that is used for the conversion; for
   *               <tt>nfConv = nfIn - 1</tt> the MSR scheme is used; for
   *               <tt>nfConv = nfIn</tt> the MSbar scheme is used, where the
   *               strong coupling is rewritten to the <tt>nfIn - 1</tt> flavor
   *               scheme. Default: <tt>nfIn - 1</tt>.
   * \param scale Specifies the scale of the running mass from which the
   *              conversion is determined. Default: \p scaleRS.
   * \param muA Specifies the scale of the strong coupling used for the
   *            conversion. Default: \p scaleRS.
   * \param order Specifies how many perturbative orders are used in the
   *              conversion. Default: Highest available order which is 4.
   * \param nRS Specifies the number of terms (used in the perturbative
   *            construction of the Borel function and the normalization N12)
   *            for the calculation of the coefficients of the pole-RS mass
   *            perturbation series. Default: highest available order which is
   *            4.
   * \param N12 Specifies the pole mass renormalon normalization constant
   *            employed in the pole-RS mass relation. Default: value computed
   *            by the sum rule formula employed by the routine N12, summing up
   *            \p nRS terms in the sum rule series using all available
   *            information on the beta-function and anomalous dimension of the
   *            MSR mass.
   */
  double mRS(int nfIn, double scaleRS, int nfConv = kDefault,
             double scale = kDefault, double muA = kDefault,
             int order = kMaxRunMSR, int nRS = kMaxRunAlpha - 1,
             double N12 = kDefault) const;
  /// Returns a asymptotic pole mass value.
  /**
   * Returns an asymptotic pole mass using the MSR mass in the flavor scheme
   * where all massive quarks are integrated out.
   *
   * \param nfIn Specifies for which quark the asymptotic pole mass is returned
   *             and refers to the number of dynamical flavors of the associated
   *             standard running mass.
   * \param scale Specifies the scale of the MSR mass from which the asymptotic
   *              pole mass is computed.
   * \param muA Specifies the renormalization scale of the strong coupling when
   *            the asymptotic pole mass is computed, and used for scale
   *            variation when using \p PoleMethod::DRange and
   *            \p PoleMethod::Range. Default: \p scale.
   * \param method Specifies the method used; available options are
   *               \p PoleMethod::Min for the minimal correction term method,
   *               \p PoleMethod::Range for the range method and
   *               \p PoleMethod::DRange for the corresponding discrete version.
   *               Default: an explicit method has to be chosen if \p ambiguity
   *               is given (no default); \p PoleMethod::Min otherwise.
   * \param f Specifies a constant larger then unity multiplying the minimal
   *          correction for the method \p PoleMethod::Range or
   *          \p PoleMethod::DRange. Default: 1.25.
   * \param ambiguity If given, the value of the pole mass ambiguity is saved
   *                  in the variable pointed to.
   * \param nMin If given, the order of the minimal correction term is saved in
   *             the variable pointed to.
   *
   * See <http://arxiv.org/abs/1706.08526> for details.
   *
   * \sa mPoleFO
   */
  double mPole(int nfIn, double scale, double muA = kDefault,
               PoleMethod method = PoleMethod::Default, double f = 1.25,
               double* ambiguity = nullptr, int* nMin = nullptr) const;
  /// Returns a pole mass value converted at a particular perturbative order.
  /**
   * Returns a pole mass value using conversion at a particular order in the
   * perturbative expansion.
   *
   * \param nfIn Specifies for which quark the pole mass value is returned and
   *             refers to the number of dynamical flavors of the associated
   *             standard running mass.
   * \param nfConv Specifies the flavor number of the running mass and strong
   *               coupling that is used for the conversion; for
   *               <tt>nfConv < nfIn</tt> the MSR scheme is used; else, the
   *               MSbar scheme is used.
   * \param scale Specifies the scale of the running mass from which the
   *              conversion is carried out.
   * \param muA Specifies the renormalization scale of the strong coupling for
   *            the conversion.
   * \param order Specifies how many perturbative orders are used in the
   *              conversion; for <tt>order > 4</tt> a renormalon-based
   *              asymptotic formula for the coefficients of the asymptotic
   *              series is used. See <http://arxiv.org/abs/1706.08526> for
   *              details.
   *
   * \sa mPole
   */
  double mPoleFO(int nfIn, int nfConv, double scale, double muA,
                 int order) const;
  /// Returns a 1S scheme mass value.
  /**
   * \param nfIn Specifies for which quark the 1S is returned and refers to the
   *             number of dynamical flavors of the associated standard running
   *             mass.
   * \param nfConv Specifies the flavor number of the running mass
   *               that is used for the conversion; for
   *               <tt>nfConv = nfIn - 1</tt> the MSR scheme is used; for
   *               <tt>nfConv = nfIn</tt> the MSbar scheme is used, where the
   *               strong coupling is rewritten to the nfIn - 1 flavor scheme.
   *               Default: <tt>nfIn - 1</tt>.
   * \param scale Specifies the scale of the running mass from which the
   *              conversion is determined; if
   *              the MSR scheme is used (determined by nfConv) the default
   *              scale is the inverse Bohr radius (which is returned by mBohr);
   *              if the MSbar scheme is used the default scale is the standard
   *              running mass.
   * \param counting Specifies the expansion scheme used for the conversion;
   *                 available options: \p Count1S::Nonrelativistic,
   *                 \p Count1S::Relativistic. For nonrelativistic
   *                 counting the leading correction is
   *                 \f$\mathcal O(\alpha_s^2)\f$ but counted as order 1; see
   *                 Sec. 5.2 of arXiv:1704.01580. Nonrelativistic counting for
   *                 the MSbar scheme is not supported. Default:
   *                 \p Count1S::Nonrelativistic for MSR and
   *                 \p Count1S::Relativistic for MSbar.
   * \param muA Specifies the scale of the strong coupling used for the
   *            conversion; default: <tt>scale</tt>.
   * \param order Specifies how many perturbative orders are used in the
   *              conversion from the running mass scheme. Default: highest
   *              available order which is 4.
   *
   * \sa mBohr
   */
  double m1S(int nfIn, int nfConv = kDefault, double scale = kDefault,
             Count1S counting = Count1S::Default, double muA = kDefault,
             int order = kMaxOrder1s) const;
  /// Returns the inverse Bohr radius.
  /**
   * Returns the inverse Bohr radius \f$M_B = C_F\alpha_s(M_B) m(M_B)\f$, used
   * for conversions involving the 1S mass.
   *
   * \param nfIn Specifies for which quark the 1S mass scale is returned and
   *             refers to the number of dynamical flavors of the associated
   *             standard running mass.
   * \param nb Specifies an additional factor on the right hand side of the
   *           equation above, useful for scale variation.
   *           Default: 1.0.
   *
   * \sa m1S
   */
  double mBohr(int nfIn, double nb = 1.0) const;
  /// Returns a renormalization group invariant (RGI) scheme mass value.
  /**
   * \param nfIn Specifies for which quark the RGI mass value is returned and
   *             refers to the number of dynamical flavors of the associated
   *             standard running mass.
   * \param order Specifies the loop order of the beta-function and MSbar
   *              mass anomalous dimensions entering the conversion formula from
   *              the standard running mass. Default: highest available order,
   *              which is 5.
   */
  double mRGI(int nfIn, int order = kMaxRunMSbar) const;
  /// Returns a kinetic scheme mass value.
  /**
   * \param nfIn Specifies for which quark the kinetic mass value is returned
   *             and refers to the number of dynamical flavors of the associated
   *             standard running mass.
   * \param scaleKin Specifies the Wilsonian cut-off scale of the kinetic
   *                 scheme.
   * \param nfConv Specifies the flavor number of the running mass
   *               that is used for the conversion; for
   *               <tt>nfConv = nfIn - 1</tt> the MSR scheme is used; for
   *               <tt>nfConv = nfIn</tt> the MSbar scheme is used, where the
   *               strong coupling is rewritten to the <tt>nfIn - 1</tt> flavor
   *               scheme. Default: <tt>nfIn - 1</tt>.
   * \param scale Specifies the scale of the running mass from which the
   *              conversion is determined. Default: 2 * scaleKin.
   * \param muA Specifies the scale of the strong coupling used for the
   *            conversion. Default: 2 * scaleKin.
   * \param order Specifies how many perturbative orders are used in the
   *              conversion. Default: Highest available order which is 3.
   */
  double mKin(int nfIn, double scaleKin, int nfConv = kDefault,
              double scale = kDefault, double muA = kDefault,
              int order = kMaxOrderKinetic) const;

  /// Returns the normalization \f$N_{1/2}\f$ of the pole mass renormalon.
  /**
   * See <https://arxiv.org/abs/1704.01580> for more details on the used
   * formula.
   *
   * The number of massless quarks for which the normalization is determined is
   * tied to the Core used to extract the normalization. The number of terms
   * summed up in the sum rule formula is set  by runMSR specified at Core
   * construction. The beta-function coefficients entering the sum rule formula
   * are used up to runAlpha loop order, all higher order coefficients are set
   * to zero.
   *
   * \param lambda Specifies the scaling parameter which probes the uncertainty
   *               of N12. Default: 1.0.
   *
   * \sa P12
   */
  double N12(double lambda = kDefault) const;
  /// Returns the normalization \f$P_{p/2}\f$ of the pole mass renormalon.
  /**
   * \sa N12
   */
  double P12(double lambda = kDefault) const;

 private:
  //----------------------------------------------------------------------------
  // Private enums
  //----------------------------------------------------------------------------

  /// Where the matching is done
  enum class Position { Above = 1, Below = -1 };

  //----------------------------------------------------------------------------
  // Private data members
  //----------------------------------------------------------------------------

  Alpha* _alpPtr{nullptr};      ///< Pointer to Alpha object
  int _nTot, _nm, _n0;          ///< Total, massive, massless flavor numbers
  int _runMSbar;                ///< Order of MSbar mass running
  double _lambdaMSbar;          ///< For MSbar-evolution scale variation
  int _orderMSbar;              ///< Order of MSbar mass matching
  int _orderMSR;                ///< Order of MSR mass matching
  const doubleV* _mm{nullptr};  ///< Pointer to m(m) vector, lightest first
  RunMethod _methodMSbar;       ///< Method used for R-evolution
  bool _validState{false};      ///< true if object construction finished

  doubleV _mmf;      ///< mMSbar at f*m(mm) for the respective flavor
  doubleV _mMSRm;    ///< mMSR at f*m(m) for the respective flavor
  doubleV _mMSRmM1;  ///< mMSR at the next lighter f*m(m)

  //----------------------------------------------------------------------------
  // Private member functions
  //----------------------------------------------------------------------------

  void setMmsr();                       ///< Set _mmf, _mMSRm, _mMSRmM1
  Alpha& alpha() override;              ///< Returns reference to *_alpPtr
  const Alpha& alpha() const override;  ///< Returns const reference to *_alpPtr
  const AnDim& andim(int nf) const;     ///< Returns const AnDim object

  /// mPole - mMSR with logs included
  doubleV msrDelta(MScheme type, int nf, double R, double mu) const;
  /// mPole - mMSR without powers of alphaS, to arbitrary high order
  doubleV msrDeltaAsy(int nl, double R, double mu, int length,
                      int betaOrd = kMaxRunAlpha,
                      int gammaROrd = kMaxRunMSR) const;
  /// mPole - m(muM) with log(m(muM)/muM), log(muM/muA) powers included
  doubleV msBarDeltaAsy(int nfIn, int nfConv, double muM, double muA,
                        int length, int betaOrd = kMaxRunAlpha,
                        int gammaROrd = kMaxRunMSR,
                        int gammaMOrd = kMaxRunMSbar) const;
  /// mPole - m(m) in terms of log(mm/mu) with alphaS powers included
  doubleV msBarDelta(int nf, double mu) const;
  /// mPole - mPS with log(mu/muF) and scale factors included
  doubleV psDelta(int nl, double muF, double muA, double rIR) const;
  /// mPole - mRS with log(mu/R) and scale factors included
  doubleV rsDelta(int nl, double R, double muA, double N12, int nRS) const;
  /// \brief mPole - mKin with log(mu/scaleKin) and scale factors included,
  ///        linear and quadratic terms
  void kinDelta(int nl, double scaleKin, double m, double muA, doubleV& linear,
                doubleV& quadratic) const;
  /// Coefficients for deltaM Tilde
  doubleV deltaMCoef(int nl, double muA, int betaOrd = kMaxRunAlpha) const;
  /// Fixed order MSR evolution to arbitrary high order
  doubleV msrEvolFOCoef(int order, int nl, double RIn, double ROut, double muA,
                        int betaOrd = kMaxRunAlpha,
                        int gammaROrd = kMaxRunMSR) const;
  /// Fixed order MSbar evolution to arbitrary high order
  doubleV msBarEvolFOCoef(int order, int nfMass, int nfEvo, double muIn,
                          double muOut, double muA, int betaOrd = kMaxRunAlpha,
                          int gammaROrd = kMaxRunMSR,
                          int gammaMOrd = kMaxRunMSbar) const;
  /// Coefficients for MSbar - MSR matching.
  doubleV msBarToMsrCoef(Position pos, int nl, double R,
                         double mBarR = kDefault) const;
  /// Computes mMSR from mMSbar, matching at scale R
  double msBarToMsr(int orderMass, int nl, double R,
                    double mBarR = kDefault) const;
  /// Computes mMSbar from mMSR, matching at scale R
  double msrToMsBar(int orderMass, int nl, double R,
                    double mR = kDefault) const;
  /// \brief Raises the flavor number of a given MSR mass and sets the new scale
  ///        to the next heavier MSbar mass
  double raiseMsrFlavor(int nl, double mMsrRnl, double R) const;
  /// Computes the S coeffcients needed for N12 with lambda variation
  doubleV sCoefLambda(double lambda) const;
  /// Get a vector of RunPar containing the present MSbar mass parameters
  RunParV getMassPar() const;
  /// mMSRn(R) at \p nlOut flavors of quark with \p nfIn active flavors in MSbar
  template <typename T>
  T mMSRn(int nfIn, int nlOut, T R) const;
  /// Returns a mass in the practical MSR scheme
  /**
   * \param nfIn Specifies which of the masses contained in the current scenario
   *             should be used. The parameter refers to the number of active
   *             flavors of the associated \f$\overline{\mathrm{MS}}\f$ mass
   *             \f$m(m)\f$.
   * \param R Scale at which the mass should be evaluated.
   *
   * The flavor number of the output mass is automatically set to
   * <tt>nfIn - 1</tt>
   */
  double mMSRp(int nfIn, double R) const;
  /// mMSbar(mu) at \p nfOut flavors of quark with \p nfIn active flavors
  template <typename T>
  T mMSbar(int nfIn, int nfOut, T mu) const;
  /// Evolves given mass \p mIn to scale of _mm[nm] and matches if necessary
  double evolveToMSbar(MScheme scheme, double mIn, double mu0, int nm) const;
  /// HQ symmetry breaking term, \p nl is the number of lighter quarks
  double deltaM(int nl, double muA = kDefault, int order = kDefault,
                int betaOrd = kMaxRunAlpha) const;
  /// Matching coefficient for MSbar mass, \p nf refers to higher flavor number
  double msBarMatching(Direction dir, MScheme scheme, int nf, double alpha,
                       double lg = 0.0) const;
};

////////////////////////////////////////////////////////////////////////////////
// Member function template definitions
////////////////////////////////////////////////////////////////////////////////

//------------------------------------------------------------------------------
// Public
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
template <typename T>
T Mass::mMS(int nfIn, T scale, int nfOut) const {
  if (!_validState) {
    error(ErrorType::InvalidState);
    return nan("");
  }

  if (nfOut == kDefault) {
    nfOut = _alpPtr->nfNatural(std::abs(scale));
  }
  if (nfIn > _nTot || nfIn <= _n0 || nfOut > _nTot || nfOut < _n0) {
    error(ErrorType::Nf);
    return nan("");
  }

  return (nfOut < nfIn) ? mMSRn(nfIn, nfOut, scale)
                        : mMSbar(nfIn, nfOut, scale);
}

//------------------------------------------------------------------------------
// Private
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
template <typename T>
T Mass::mMSRn(int nfIn, int nlOut, T R) const {
  int nlIn = nfIn - 1;
  const doubleV& fMatch = _alpPtr->_fMatch;

  if (nlIn < nlOut) {
    error(ErrorType::Nf);
    return nan("");
  } else if (nlIn == nlOut) {
    return _mMSRm[nlIn - _n0] + rEvol(MScheme::MSRn, _methodR, nlOut,
                                      fMatch[nlIn - _n0] * (*_mm)[nlIn - _n0],
                                      R);
  } else {
    return mMSRn(nfIn - 1, nlOut, R) - _mmf[nlIn - _n0 - 1] +
           _mMSRmM1[nlIn - _n0] + deltaM(nlIn);
  }
}

//------------------------------------------------------------------------------
template <typename T>
T Mass::mMSbar(int nfIn, int nfOut, T mu) const {
  if (nfIn > nfOut) {
    error(ErrorType::Nf);
    return nan("");
  }

  T res;

  if (nfIn == nfOut) {
    double mm = (*_mm)[nfIn - 1 - _n0];
    double alLambda0 = (*_alpPtr)(mm * _lambdaMSbar, nfIn);
    T alLambda1 = (*_alpPtr)(mu * _lambdaMSbar, nfOut);
    res = mm * std::exp(andim(nfIn).wTilde(_methodMSbar, _runMSbar, alLambda0,
                                           alLambda1));
  } else {
    double mm[2], alLambda0;
    T alLambda1;
    const doubleV& fMatch = _alpPtr->_fMatch;

    mm[0] = (*_mm)[nfIn - 1 - _n0];
    mm[1] = (*_mm)[nfIn - 1 - _n0] * fMatch[nfIn - 1 - _n0];
    alLambda0 = (*_alpPtr)(mm[0] * _lambdaMSbar, nfIn);
    alLambda1 = (*_alpPtr)(mm[1] * _lambdaMSbar, nfIn);

    res = mm[0] * std::exp(andim(nfIn).wTilde(_methodMSbar, _runMSbar,
                                              alLambda0, alLambda1));

    mm[0] = (*_mm)[nfOut - 1 - _n0] * fMatch[nfOut - 1 - _n0];
    alLambda0 = (*_alpPtr)(mm[0] * _lambdaMSbar, nfOut);
    alLambda1 = (*_alpPtr)(mu * _lambdaMSbar, nfOut);

    res *= std::exp(
        andim(nfOut).wTilde(_methodMSbar, _runMSbar, alLambda0, alLambda1));

    for (int i = 0; i < nfOut - nfIn; ++i) {
      mm[0] = (*_mm)[nfIn - 1 - _n0 + i] * fMatch[nfIn - 1 - _n0 + i];
      mm[1] = (*_mm)[nfIn - _n0 + i] * fMatch[nfIn - _n0 + i];
      alLambda0 = (*_alpPtr)(mm[0] * _lambdaMSbar, nfIn + i);
      alLambda1 = (*_alpPtr)(mm[1] * _lambdaMSbar, nfIn + i);
      double al = (*_alpPtr)(mm[1], nfIn + i);
      res *= std::exp(andim(nfIn + i).wTilde(_methodMSbar, _runMSbar, alLambda0,
                                             alLambda1)) *
             msBarMatching(Direction::Up, MScheme::MSbar, nfIn + i + 1, al,
                           std::log(fMatch[nfIn - _n0 + i]));
    }
  }

  return res;
}

}  // namespace revo

#endif  // MASS_H

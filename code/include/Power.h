////////////////////////////////////////////////////////////////////////////////
/// \file Power.h
/// \brief Interface and implementation of class Power
/// \author Andre Hoang, Christopher Lepenik, Vicent Mateu
///
// Copyright:
//    Copyright (C) 2020 Andre Hoang, Christopher Lepenik, Vicent Mateu
//
//    This file is part of REvolver.
//
//    REvolver is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    REvolver is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with REvolver.  If not, see <https://www.gnu.org/licenses/>.
////////////////////////////////////////////////////////////////////////////////

#ifndef POWER_H
#define POWER_H

#include "InfVector.h"

namespace rac {

////////////////////////////////////////////////////////////////////////////////
/// \class Power
///
/// \brief Provides powers of an object.
/// \author Christopher Lepenik
///
/// A Power object, constructed with an input value \p var, provides this number
/// raised to arbitrary positive integer power. The output can be obtained by
/// the square or round bracket operator.
/// The underlying type should be \p double or \p int.
////////////////////////////////////////////////////////////////////////////////

/// \tparam T Type of objects which is raised to a power.
template <typename T>
class Power : public InfVector<T> {
 public:
  /// Constructs a Power object with variable \p var.
  explicit Power(T var)
      : InfVector<T>([var](std::vector<T>& v) { v.push_back(var * v.back()); },
                     1){};
};

}  // namespace rac

#endif  // POWER_H

%define DOCSTRING
"pyREvolver is a python wrapper around REvolver, a code written in C++
for running and matching of the QCD coupling and quark masses. The code
also features conversion between various masses renormalization
schemes."
%enddef

%module(docstring=DOCSTRING) pyREvolver
%{
#include "../../include/REvolver.h"
%}

// Ignore some functions for wrapper
%ignore revo::Core::Core(Core&&);
%ignore revo::Core::operator=;

// Include type definitions
%include std_vector.i
%include std_complex.i
%include typemaps.i
%include "../../include/TypeDef.h"

// Include automatically generated docstrings
%feature("autodoc", "1");

// Deal with multiple-output mPole 
%apply double* OUTPUT { double* ambiguity };
%apply int* OUTPUT { int* nMin };
%rename("mPoleDetailed") revo::Mass::mPole(int, double, double, PoleMethod,
                                           double, double*, int*) const;
%ignore revo::Mass::mPole(int, double, double, PoleMethod, double,
                          double*) const;

// Parse the header files to generate wrappers
%include "../../include/AnDim.h"
%include "../../include/Alpha.h"
%include "../../include/Evolving.h"
%include "../../include/Mass.h"
%include "../../include/Core.h"
%include "../../include/RQuantity.h"
%include "../../include/MQuantity.h"

// Specialize templates
%template(doubleV) std::vector<double>;
%template(RunParV) std::vector<revo::RunPar>;
%template() revo::Alpha::operator()<double>;
%template() revo::Alpha::operator()<std::complex<double>>;
%template(alpha) revo::Core::alpha<double>;
%template(alpha) revo::Core::alpha<std::complex<double>>;
%template(mMS) revo::Mass::mMS<double>;
%template(mMS) revo::Mass::mMS<std::complex<double>>;

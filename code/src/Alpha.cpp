////////////////////////////////////////////////////////////////////////////////
/// \file Alpha.cpp
/// \brief Implementation of class Alpha
/// \author Andre Hoang, Christopher Lepenik, Vicent Mateu
///
// Copyright:
//    Copyright (C) 2020 Andre Hoang, Christopher Lepenik, Vicent Mateu
//
//    This file is part of REvolver.
//
//    REvolver is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    REvolver is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with REvolver.  If not, see <https://www.gnu.org/licenses/>.
////////////////////////////////////////////////////////////////////////////////

#define _USE_MATH_DEFINES

#include "Alpha.h"

#include <algorithm>
#include <cmath>
//#include <iostream>

using namespace std;

namespace revo {

//------------------------------------------------------------------------------
// Constructors
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
Alpha::Alpha(int nTot, const RunPar& alphaPar, const doubleV& masses,
             const doubleV& fMatch, int run, double lambda, int order,
             MScheme scheme, RunMethod method, double precisionGoal)
    : Alpha(nTot, masses.size(), alphaPar, run, lambda, order, scheme, method,
            precisionGoal) {
  init(masses, fMatch);
}

//------------------------------------------------------------------------------
Alpha::Alpha(int nTot, int nm, const RunPar& alphaPar, int run, double lambda,
             int order, MScheme scheme, RunMethod method, double precisionGoal)
    : _order{constrain(order, 0, kMaxOrderAlpha)},
      _run{constrain(run, 1, kMaxRunAlpha)},
      _lambda{lambda},
      _nTot{nTot},
      _nm{nm},
      _n0{_nTot - _nm},
      _parDef{alphaPar},
      _method{method},
      _matchType{AlphaMatchType::Consistent},
      _andim(_n0, _nTot, scheme, precisionGoal),
      _validState{false} {
  applyConstrain(_parDef.nf, _n0, _nTot);
  bool betaIsStrict;
  switch (_method) {
    case RunMethod::Expanded:
    case RunMethod::Inverse:
    case RunMethod::Iterative:
      betaIsStrict = false;
      break;

    case RunMethod::Exact:
    default:
      betaIsStrict = true;
      break;
  }

  _andim.apply(&AnDim::setAlphaProp, _run, betaIsStrict);
  _andim.apply(&AnDim::setLambdaAlpha, _lambda);

  _refMu[_parDef.nf] = _parDef.scale;
  _refAlpha[_parDef.nf] = _parDef.value;

  if (_nm == 0) {
    _validState = true;
    setLambdaQCD();
  }
}

//------------------------------------------------------------------------------
// Public member functions
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
void Alpha::init(const doubleV& masses, const doubleV& fMatch) {
  if (int(masses.size()) != _nm) {
    error(ErrorType::NumMasses);
    return;
  }
  if (_fMatch.empty() || !fMatch.empty()) {
    _fMatch = fMatch;
  }
  _fMatch.resize(_nm, 1.0);
  _masses = masses;

  for (int i = _parDef.nf + 1; i <= _nTot; ++i) {
    _refMu[i] = _masses[i - _n0 - 1] * _fMatch[i - _n0 - 1];
    _refAlpha[i] =
        match(Direction::Up, i,
              _andim[i - 1].alphaGeneric(_method, _refMu[i - 1],
                                         _refAlpha[i - 1], _refMu[i]) /
                  M_PI,
              log(_fMatch[i - _n0 - 1]));
  }

  auto matchDown = (_matchType == AlphaMatchType::Perturbative)
                       ? &Alpha::match
                       : &Alpha::matchReverse;

  for (int i = _parDef.nf - 1; i >= _n0; --i) {
    _refMu[i] = _masses[i - _n0] * _fMatch[i - _n0];
    _refAlpha[i] = (this->*matchDown)(
        Direction::Down, i + 1,
        _andim[i + 1].alphaGeneric(_method, _refMu[i + 1], _refAlpha[i + 1],
                                   _refMu[i]) /
            M_PI,
        log(_fMatch[i - _n0]));
    if (std::isnan(_refAlpha[i])) {
      _refAlpha[i] = _andim[i + 1].alphaGeneric(_method, _refMu[i + 1],
                                                _refAlpha[i + 1], _refMu[i]);
    }
  }

  setLambdaQCD();
  _validState = true;
}

//------------------------------------------------------------------------------
void Alpha::setBeta(const doubleV& beta) {
  if (_nm != 0) {
    error(ErrorType::NoCustomBeta);
    return;
  }

  _andim.apply(&AnDim::setBeta, beta);
  _run = beta.size();
  setLambdaQCD();
}

//------------------------------------------------------------------------------
const AnDim& Alpha::andim(int nf) const {
  return _andim.at(constrain(nf, _n0, _nTot));
}

//------------------------------------------------------------------------------
const doubleV& Alpha::inMasses() const { return _masses; }

//------------------------------------------------------------------------------
int Alpha::runOrd() const { return _validState ? _run : 0; }

//------------------------------------------------------------------------------
int Alpha::nfl(NfType type) const {
  if (!_validState) {
    error(ErrorType::InvalidState);
    return 0;
  }

  switch (type) {
    case NfType::Total:
      return _nTot;
      break;

    case NfType::Massive:
      return _nm;
      break;

    case NfType::Massless:
      return _n0;
      break;

    default:
      error(ErrorType::Keyword);
      return 0;
      break;
  }
}

//------------------------------------------------------------------------------
double Alpha::lambdaQCD(int nf, LambdaConvention convention) const {
  if (_lQCD.count(nf) < 1) {
    error(ErrorType::InvalidState);
    return nan("");
  } else if (nf < _n0 || nf > _nTot) {
    error(ErrorType::Nf);
    return nan("");
  }

  return convention == LambdaConvention::MSbar
             ? pow(2, andim(nf).bHatLambda(1, _run, andim(nf)._betaIsStrict)) *
                   _lQCD.at(nf)
             : _lQCD.at(nf);
}

//------------------------------------------------------------------------------
const RunPar& Alpha::defParams() const {
  if (!_validState) {
    error(ErrorType::InvalidState);
  }

  return _parDef;
  }

//------------------------------------------------------------------------------
// Private member functions
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
void Alpha::setLambdaQCD() {
  for (int nf = _n0; nf <= _nTot; ++nf) {
    _lQCD[nf] = computeLambdaQCD(nf);
  }
}

//------------------------------------------------------------------------------
double Alpha::match(Direction dir, int nf, double aRef, double lg) const {
  doubleV e = _andim.at(nf).thresholdMatching(dir, _order, lg);
  return M_PI * sumSeries(e.begin(), e.end(), aRef, 0.0);
}

//------------------------------------------------------------------------------
double Alpha::matchReverse(Direction dir, int nf, double aRef,
                           double lg) const {
  dir = -dir;
  doubleV e = _andim.at(nf).thresholdMatching(dir, _order, lg);

  double ares = findRoot(
      [&](double a) {
        return a - aRef / sumSeries(e.begin() + 1, e.end(), a, e[0]);
      },
      aRef, andim(_n0).precisionGoal());

  return M_PI * ares;
}

//------------------------------------------------------------------------------
double Alpha::computeLambdaQCD(int nf, double muLambda, int order) const {
  double al = (*this)(muLambda, nf);

  return muLambda * _andim.at(nf).gFun(al, order);
}

//------------------------------------------------------------------------------
int Alpha::nfNatural(double mu) const {
  int nf = _n0;
  for (int n = 0; n < _nm; ++n) {
    if (mu > _fMatch[n] * _masses[n]) {
      ++nf;
    } else {
      break;
    }
  }
  return nf;
}

}  // namespace revo

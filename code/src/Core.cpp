////////////////////////////////////////////////////////////////////////////////
/// \file Core.cpp
/// \brief Implementation of class Core
/// \author Andre Hoang, Christopher Lepenik, Vicent Mateu
///
// Copyright:
//    Copyright (C) 2020 Andre Hoang, Christopher Lepenik, Vicent Mateu
//
//    This file is part of REvolver.
//
//    REvolver is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    REvolver is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with REvolver.  If not, see <https://www.gnu.org/licenses/>.
////////////////////////////////////////////////////////////////////////////////

#include "Core.h"

#include <stdlib.h>

#include <algorithm>
#include <functional>
#include <iostream>
#include <vector>

using namespace std;
using namespace std::placeholders;

namespace revo {

//------------------------------------------------------------------------------
// Constructors
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
Core::Core(int nTot, const RunPar& alphaPar, const RunParV& mPar,
           const doubleV& fMatch, int runAlpha, double lambdaAlpha,
           int orderAlpha, int runMSbar, double lambdaMSbar, int orderMSbar,
           int runMSR, double lambdaMSR, int orderMSR, double msBarDeltaError,
           double precisionGoal)
    : _nTot{nTot},
      _nm{int(mPar.size())},
      _n0{_nTot - _nm},
      _fMatch{fMatch},
      _runAlpha{constrain(runAlpha, 1, kMaxRunAlpha)},
      _lambdaAlpha{lambdaAlpha},
      _orderAlpha{constrain(orderAlpha, 0, kMaxOrderAlpha)},
      _runMSbar{constrain(runMSbar, 1, kMaxRunMSbar)},
      _lambdaMSbar{lambdaMSbar},
      _orderMSbar{constrain(orderMSbar, 0, kMaxOrderMSbar)},
      _runMSR{constrain(runMSR, 1, kMaxRunMSR)},
      _lambdaMSR{lambdaMSR},
      _orderMSR{constrain(orderMSR, 0, kMaxOrderMSR)},
      _msBarDeltaError{msBarDeltaError},
      _precisionGoal{constrain(precisionGoal, kMaxPrec, kMinPrec)},
      _methodMSbar{RunMethod::Exact},
      _methodMSR{RunMethod::ExactPF},
      _methodAlpha{RunMethod::Exact},
      _alp(_nTot, _nm, alphaPar, _runAlpha, _lambdaAlpha, _orderAlpha,
           MScheme::MSbar, _methodAlpha, _precisionGoal),
      _mass(&_alp, _runMSbar, _lambdaMSbar, _orderMSbar, _runMSR, _lambdaMSR,
            _orderMSR, _msBarDeltaError, _methodMSbar, _methodMSR),
      _mm{&_alp._masses},
      _betaIsUserDef{false},
      _validState{false} {
  constrHelp(mPar);
}

//------------------------------------------------------------------------------
Core::Core(const RunPar& alphaPar, int runAlpha, double lambdaAlpha,
           const doubleV& beta, double precisionGoal)
    : _nTot{alphaPar.nf},
      _nm{0},
      _n0{alphaPar.nf},
      _runAlpha{beta.empty() ? constrain(runAlpha, 1, kMaxRunAlpha)
                             : min(int(beta.size()), runAlpha)},
      _lambdaAlpha{lambdaAlpha},
      _orderAlpha{kMaxOrderAlpha},
      _runMSbar{kMaxRunMSbar},
      _lambdaMSbar{1.0},
      _orderMSbar{kMaxOrderMSbar},
      _runMSR{kMaxRunMSR},
      _lambdaMSR{1.0},
      _orderMSR{kMaxOrderMSR},
      _msBarDeltaError{0.0},
      _precisionGoal{constrain(precisionGoal, kMaxPrec, kMinPrec)},
      _methodMSbar{RunMethod::Exact},
      _methodMSR{RunMethod::ExactPF},
      _methodAlpha{RunMethod::Exact},
      _alp(_nTot, _nm, alphaPar, _runAlpha, _lambdaAlpha, _orderAlpha,
           MScheme::MSbar, _methodAlpha, _precisionGoal),
      _mass(&_alp, _runMSbar, _lambdaMSbar, _orderMSbar, _runMSR, _lambdaMSR,
            _orderMSR, _msBarDeltaError, _methodMSbar, _methodMSR),
      _mm{&_alp._masses},
      _betaIsUserDef{!beta.empty()},
      _validState{false} {
  if (_betaIsUserDef) {
    if (int(beta.size()) > runAlpha) {
      _alp.setBeta(doubleV(beta.begin(), beta.begin() + runAlpha));
    } else {
      _alp.setBeta(beta);
    }
  }
  _validState = true;
}

//------------------------------------------------------------------------------
Core::Core(const Core& coreIn)
    : _nTot{coreIn._nTot},
      _nm{coreIn._nm},
      _n0{coreIn._n0},
      _fMatch{coreIn._fMatch},
      _runAlpha{coreIn._runAlpha},
      _lambdaAlpha{coreIn._lambdaAlpha},
      _orderAlpha{coreIn._orderAlpha},
      _runMSbar{coreIn._runMSbar},
      _lambdaMSbar{coreIn._lambdaMSbar},
      _orderMSbar{coreIn._orderMSbar},
      _runMSR{coreIn._runMSR},
      _lambdaMSR{coreIn._lambdaMSR},
      _orderMSR{coreIn._orderMSR},
      _msBarDeltaError{coreIn._msBarDeltaError},
      _precisionGoal{coreIn._precisionGoal},
      _methodMSbar{coreIn._methodMSbar},
      _methodMSR{coreIn._methodMSR},
      _methodAlpha{coreIn._methodAlpha},
      _alp{coreIn._alp},
      _mass(&_alp, _runMSbar, _lambdaMSbar, _orderMSbar, _runMSR, _lambdaMSR,
            _orderMSR, _msBarDeltaError, _methodMSbar, _methodMSR),
      _mm{&_alp.inMasses()},
      _betaIsUserDef{coreIn._betaIsUserDef},
      _validState{coreIn._validState} {}

//------------------------------------------------------------------------------
Core::Core(Core&& coreIn)
    : _nTot{coreIn._nTot},
      _nm{coreIn._nm},
      _n0{coreIn._n0},
      _fMatch{move(coreIn._fMatch)},
      _runAlpha{coreIn._runAlpha},
      _lambdaAlpha{coreIn._lambdaAlpha},
      _orderAlpha{coreIn._orderAlpha},
      _runMSbar{coreIn._runMSbar},
      _lambdaMSbar{coreIn._lambdaMSbar},
      _orderMSbar{coreIn._orderMSbar},
      _runMSR{coreIn._runMSR},
      _lambdaMSR{coreIn._lambdaMSR},
      _orderMSR{coreIn._orderMSR},
      _msBarDeltaError{coreIn._msBarDeltaError},
      _precisionGoal{coreIn._precisionGoal},
      _methodMSbar{coreIn._methodMSbar},
      _methodMSR{coreIn._methodMSR},
      _methodAlpha{coreIn._methodAlpha},
      _alp{move(coreIn._alp)},
      _mass(&_alp, _runMSbar, _lambdaMSbar, _orderMSbar, _runMSR, _lambdaMSR,
            _orderMSR, _msBarDeltaError, _methodMSbar, _methodMSR),
      _mm{&_alp.inMasses()},
      _betaIsUserDef{coreIn._betaIsUserDef},
      _validState{coreIn._validState} {}

//------------------------------------------------------------------------------
// Assignment operators
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
Core& Core::operator=(const Core& coreIn) {
  if (this != &coreIn) {
    _nTot = coreIn._nTot;
    _nm = coreIn._nm;
    _n0 = coreIn._n0;
    _fMatch = coreIn._fMatch;
    _runAlpha = coreIn._runAlpha;
    _lambdaAlpha = coreIn._lambdaAlpha;
    _orderAlpha = coreIn._orderAlpha;
    _runMSbar = coreIn._runMSbar;
    _lambdaMSbar = coreIn._lambdaMSbar;
    _orderMSbar = coreIn._orderMSbar;
    _runMSR = coreIn._runMSR;
    _lambdaMSR = coreIn._lambdaMSR;
    _orderMSR = coreIn._orderMSR;
    _msBarDeltaError = coreIn._msBarDeltaError;
    _precisionGoal = coreIn._precisionGoal;
    _methodMSbar = coreIn._methodMSbar;
    _methodMSR = coreIn._methodMSR;
    _methodAlpha = coreIn._methodAlpha;
    _alp = coreIn._alp;
    _mass =
        Mass(&_alp, _runMSbar, _lambdaMSbar, _orderMSbar, _runMSR, _lambdaMSR,
             _orderMSR, _msBarDeltaError, _methodMSbar, _methodMSR);
    _mm = &_alp.inMasses();
    _betaIsUserDef = coreIn._betaIsUserDef;
    _validState = coreIn._validState;
  }

  return *this;
}

//------------------------------------------------------------------------------
Core& Core::operator=(Core&& coreIn) {
  if (this != &coreIn) {
    _nTot = coreIn._nTot;
    _nm = coreIn._nm;
    _n0 = coreIn._n0;
    _fMatch = move(coreIn._fMatch);
    _runAlpha = coreIn._runAlpha;
    _lambdaAlpha = coreIn._lambdaAlpha;
    _orderAlpha = coreIn._orderAlpha;
    _runMSbar = coreIn._runMSbar;
    _lambdaMSbar = coreIn._lambdaMSbar;
    _orderMSbar = coreIn._orderMSbar;
    _runMSR = coreIn._runMSR;
    _lambdaMSR = coreIn._lambdaMSR;
    _orderMSR = coreIn._orderMSR;
    _msBarDeltaError = coreIn._msBarDeltaError;
    _precisionGoal = coreIn._precisionGoal;
    _methodMSbar = coreIn._methodMSbar;
    _methodMSR = coreIn._methodMSR;
    _methodAlpha = coreIn._methodAlpha;
    _alp = move(coreIn._alp);
    _mass =
        Mass(&_alp, _runMSbar, _lambdaMSbar, _orderMSbar, _runMSR, _lambdaMSR,
             _orderMSR, _msBarDeltaError, _methodMSbar, _methodMSR);
    _mm = &_alp.inMasses();
    _betaIsUserDef = coreIn._betaIsUserDef;
    _validState = coreIn._validState;
  }

  return *this;
}

//------------------------------------------------------------------------------
// Non-const public member functions
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
void Core::addMsMass(int nf, double mass, double scale, double fnQ,
                     bool showOrderError) {
  if (_betaIsUserDef) {
    error(ErrorType::CustomBeta);
    return;
  } else if (nf < _n0 || nf > _nTot + 1) {
    error(ErrorType::Nf);
    return;
  } else if (!_validState) {
    error(ErrorType::InvalidState);
    return;
  }

  RunParV mPar = _mass.getMassPar();
  mPar.push_back({nf, mass, scale});

  _fMatch.push_back(fnQ);

  addMass();
  constrHelp(mPar, showOrderError);
}

//------------------------------------------------------------------------------
void Core::addPoleMass(double mPole, double scale, double muA,
                       PoleMethod method, double f, double fnQ) {
  if (_betaIsUserDef) {
    error(ErrorType::CustomBeta);
    return;
  }

  addMsMass(_n0, scale, scale, fnQ, false);
  RunParV mPar = _mass.getMassPar();

  mPar[_nm - 1].nf = _n0;
  mPar[_nm - 1].scale = scale;
  double mPoleStep[2] = {_mass.mPole(_nTot, scale, muA, method, f)};
  double mMSStep[2] = {scale, (mPar[_nm - 1].value += mPole - mPoleStep[0])};
  constrHelp(mPar, false);

  for (int k = 0; k < kMaxIter; ++k) {
    checkIter(k);
    mPoleStep[1] = _mass.mPole(_nTot, scale, muA, method, f);
    double diff =
        (abs(mPoleStep[1] - mPoleStep[0]) > 0)
            ? (mMSStep[1] - mMSStep[0]) / (mPoleStep[1] - mPoleStep[0])
            : 1;
    double corr = (mPole - mPoleStep[1]) * diff;
    mMSStep[0] = mMSStep[1];
    mMSStep[1] = (mPar[_nm - 1].value += corr);
    constrHelp(mPar, false);
    if (std::isnan(mMSStep[1]) || abs(corr / mMSStep[0]) < _precisionGoal) {
      break;
    } else {
      mPoleStep[0] = mPoleStep[1];
    }
  }

  checkMassOrd(*_mm);
}

//------------------------------------------------------------------------------
void Core::addPoleMassFO(double mPole, int nfConv, double scale, double muA,
                         int order, double fnQ) {
  if (_betaIsUserDef) {
    error(ErrorType::CustomBeta);
    return;
  } else if (!_validState) {
    error(ErrorType::InvalidState);
    return;
  }

  if (order < 0) {
    order = 0;
  }
  if (nfConv < _n0 || nfConv > _nTot + 1) {
    error(ErrorType::Nf);
    return;
  }

  addMsMass(nfConv, scale, scale, fnQ, false);
  RunParV mPar = _mass.getMassPar();

  mPar[_nm - 1].nf = nfConv;
  mPar[_nm - 1].scale = scale;
  double mPoleStep[2] = {_mass.mPoleFO(_nTot, nfConv, scale, muA, order)};
  double mMSStep[2] = {scale, (mPar[_nm - 1].value += mPole - mPoleStep[0])};
  constrHelp(mPar, false);

  for (int k = 0; k < kMaxIter; ++k) {
    checkIter(k);
    mPoleStep[1] = _mass.mPoleFO(_nTot, nfConv, scale, muA, order);
    double diff =
        (abs(mPoleStep[1] - mPoleStep[0]) > 0)
            ? (mMSStep[1] - mMSStep[0]) / (mPoleStep[1] - mPoleStep[0])
            : 1;
    double corr = (mPole - mPoleStep[1]) * diff;
    mMSStep[0] = mMSStep[1];
    mMSStep[1] = (mPar[_nm - 1].value += corr);
    constrHelp(mPar, false);
    if (std::isnan(mMSStep[1]) || abs(corr / mMSStep[0]) < _precisionGoal) {
      break;
    } else {
      mPoleStep[0] = mPoleStep[1];
    }
  }

  checkMassOrd(*_mm);
}

//------------------------------------------------------------------------------
void Core::addPSMass(double mPS, double muF, int nfConv, double scale,
                     double muA, double rIR, int order, double fnQ) {
  if (_betaIsUserDef) {
    error(ErrorType::CustomBeta);
    return;
  } else if (!_validState) {
    error(ErrorType::InvalidState);
    return;
  }

  applyConstrain(order, 0, kMaxOrderPs);

  if (nfConv == kDefault) {
    nfConv = _nTot;
  } else if (nfConv < _nTot || nfConv > _nTot + 1) {
    error(ErrorType::Nf);
    return;
  }

  if (isDefault(muA)) {
    muA = muF;
  }
  if (isDefault(scale)) {
    scale = muF;
  }

  addMsMass(_nTot, mPS, muF, fnQ);
  RunParV mPar = _mass.getMassPar();

  mPar[_nm - 1].nf = nfConv;
  mPar[_nm - 1].scale = scale;

  findRoot(
      [&](double x) {
        mPar[_nm - 1].value = x;
        constrHelp(mPar, false);
        return mPS - _mass.mPS(_nTot, muF, nfConv, scale, muA, rIR, order);
      },
      _mass.mMS(_nTot, scale, nfConv) + mPS -
          _mass.mPS(_nTot, muF, nfConv, scale, muA, rIR, order),
      _precisionGoal);
}

//------------------------------------------------------------------------------
void Core::add1SMass(double m1S, int nfConv, double scale, Count1S counting,
                     double muA, int order, double fnQ) {
  if (_betaIsUserDef) {
    error(ErrorType::CustomBeta);
    return;
  } else if (!_validState) {
    error(ErrorType::InvalidState);
    return;
  }

  if (nfConv == kDefault) {
    nfConv = _nTot;
  } else if (!(nfConv == _nTot || nfConv == _nTot + 1)) {
    error(ErrorType::Nf);
    return;
  }

  bool isMsBar = (nfConv == _nTot + 1);

  if (counting == Count1S::Default) {
    counting = isMsBar ? Count1S::Relativistic : Count1S::Nonrelativistic;
  }

  int countNR = (counting == Count1S::Nonrelativistic) ? 1 : 0;
  applyConstrain(order, 0, kMaxOrder1s + countNR);

  bool isStdScale = isDefault(scale);
  if (isStdScale) {
    scale = m1S;
  }

  addMsMass(_nTot, m1S, scale, fnQ);
  RunParV mPar = _mass.getMassPar();

  mPar[_nm - 1].nf = nfConv;
  mPar[_nm - 1].scale = scale;

  findRoot(
      [&](double x) {
        if (isStdScale) {
          mPar[_nm - 1].scale =
              isMsBar ? (*_mm)[_nm - 1] : masses().mBohr(_nTot);
        }
        mPar[_nm - 1].value = x;
        constrHelp(mPar, false);
        return m1S - _mass.m1S(_nTot, nfConv, (isStdScale ? kDefault : scale),
                               counting, muA, order);
      },
      _mass.mMS(_nTot, mPar[_nm - 1].scale, nfConv) + m1S -
          _mass.m1S(_nTot, nfConv, (isStdScale ? kDefault : scale), counting,
                    muA, order),
      _precisionGoal);
}

//------------------------------------------------------------------------------
void Core::addRGIMass(double mRGI, int order, double fnQ) {
  if (_betaIsUserDef) {
    error(ErrorType::CustomBeta);
    return;
  } else if (!_validState) {
    error(ErrorType::InvalidState);
    return;
  }

  applyConstrain(order, 0, kMaxRunMSbar);

  addMsMass(_nTot, mRGI, mRGI, fnQ);
  RunParV mPar = _mass.getMassPar();

  mPar[_nm - 1].nf = _nTot;
  mPar[_nm - 1].scale = mRGI;

  findRoot(
      [&](double x) {
        mPar[_nm - 1].scale = (mPar[_nm - 1].value = x);
        constrHelp(mPar, false);
        return mRGI - _mass.mRGI(_nTot, order);
      },
      mPar[_nm - 1].value, _precisionGoal);
}

//------------------------------------------------------------------------------
void Core::addRSMass(double mRS, double scaleRS, int nfConv, double scale,
                     double muA, int order, int nRS, double N12, double fnQ) {
  if (_betaIsUserDef) {
    error(ErrorType::CustomBeta);
    return;
  } else if (!_validState) {
    error(ErrorType::InvalidState);
    return;
  }

  applyConstrain(order, 0, kMaxRunMSR);

  if (isDefault(muA)) {
    muA = scaleRS;
  }
  if (isDefault(scale)) {
    scale = scaleRS;
  }
  if (nfConv == kDefault) {
    nfConv = _nTot;
  } else if (!(nfConv == _nTot || nfConv == _nTot + 1)) {
    error(ErrorType::Nf);
    return;
  }

  addMsMass(_nTot, mRS, scaleRS, fnQ);
  RunParV mPar = _mass.getMassPar();
  const doubleV& mm = *_mm;

  double mmNew = mm[_nm - 1];
  mPar[_nm - 1].nf = nfConv;
  mPar[_nm - 1].scale = scale;

  for (int k = 0; k < kMaxIter; ++k) {
    checkIter(k);
    mPar[_nm - 1].value =
        mRS + _mass.mMS(_nTot, scale, nfConv) -
        _mass.mRS(_nTot, scaleRS, nfConv, scale, muA, order, nRS, N12);
    constrHelp(mPar, false);
    if (abs(1 - mm[_nm - 1] / mmNew) < _precisionGoal) {
      break;
    } else {
      mmNew = mm[_nm - 1];
    }
  }
}

//------------------------------------------------------------------------------
void Core::addKinMass(double mKin, double scaleKin, int nfConv, double scale,
                      double muA, int order, double fnQ) {
  if (_betaIsUserDef) {
    error(ErrorType::CustomBeta);
    return;
  } else if (!_validState) {
    error(ErrorType::InvalidState);
    return;
  }

  applyConstrain(order, 0, kMaxOrderKinetic);

  if (isDefault(muA)) {
    muA = 2 * scaleKin;
  }
  if (isDefault(scale)) {
    scale = 2 * scaleKin;
  }
  if (nfConv == kDefault) {
    nfConv = _nTot;
  } else if (!(nfConv == _nTot || nfConv == _nTot + 1)) {
    error(ErrorType::Nf);
    return;
  }

  addMsMass(_nTot, mKin, 2 * scaleKin, fnQ);
  RunParV mPar = _mass.getMassPar();
  const doubleV& mm = *_mm;

  double mmNew = mm[_nm - 1];
  mPar[_nm - 1].nf = nfConv;
  mPar[_nm - 1].scale = scale;

  for (int k = 0; k < kMaxIter; ++k) {
    checkIter(k);
    mPar[_nm - 1].value =
        mKin + _mass.mMS(_nTot, scale, nfConv) -
        _mass.mKin(_nTot, scaleKin, nfConv, scale, muA, order);
    constrHelp(mPar, false);
    if (abs(1 - mm[_nm - 1] / mmNew) < _precisionGoal) {
      break;
    } else {
      mmNew = mm[_nm - 1];
    }
  }
}

//------------------------------------------------------------------------------
// Access to member objects
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
const Mass& Core::masses() const { return _mass; }

//------------------------------------------------------------------------------
const Alpha& Core::alpha() const { return _alp; }

//------------------------------------------------------------------------------
const AnDim& Core::andim(int nf) const { return _alp.andim(nf); }

//------------------------------------------------------------------------------
// Access to optional parameters set by constructor
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
int Core::nTot() const { return alpha().nfl(NfType::Total); }

//------------------------------------------------------------------------------
const doubleV& Core::standardMasses() const { return alpha().inMasses(); }

//------------------------------------------------------------------------------
const doubleV& Core::fMatch() const { return _fMatch; }

//------------------------------------------------------------------------------
int Core::getOrder(OrderPar para) const {
  if (!_validState) {
    error(ErrorType::InvalidState);
    return 0;
  }

  switch (para) {
    case OrderPar::runAlpha:
      return _runAlpha;
      break;

    case OrderPar::orderAlpha:
      return _orderAlpha;
      break;

    case OrderPar::runMSbar:
      return _runMSbar;
      break;

    case OrderPar::runMSR:
      return _runMSR;
      break;

    case OrderPar::orderMSbar:
      return _orderMSbar;
      break;

    case OrderPar::orderMSR:
      return _orderMSR;
      break;

    default:
      error(ErrorType::Keyword);
      return 0;
      break;
  }
}

//------------------------------------------------------------------------------
double Core::getLambda(LambdaPar para) const {
  if (!_validState) {
    error(ErrorType::InvalidState);
    return nan("");
  }

  switch (para) {
    case LambdaPar::lambdaAlpha:
      return _lambdaAlpha;
      break;

    case LambdaPar::lambdaMSbar:
      return _lambdaMSbar;
      break;

    case LambdaPar::lambdaMSR:
      return _lambdaMSR;
      break;

    default:
      error(ErrorType::Keyword);
      return 0.0;
      break;
  }
}

//------------------------------------------------------------------------------
double Core::msBarDeltaError() const {
  return _validState ? _msBarDeltaError : nan("");
}

//------------------------------------------------------------------------------
double Core::precisionGoal() const {
  return _validState ? _precisionGoal : nan("");
}

//------------------------------------------------------------------------------
const doubleV& Core::betaCoefs(int nf) const {
  applyConstrain(nf, _n0, _nTot);

  return alpha().andim(nf).betaCoef();
}

//------------------------------------------------------------------------------
bool Core::betaIsUserDef() const { return _betaIsUserDef; }

//------------------------------------------------------------------------------
// Private member functions
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
void Core::constrHelp(const RunParV& mPar, bool showOrderError) {
  if (_nm > _nTot || _nm < 1 || _alp._parDef.nf < _n0 ||
      _alp._parDef.nf > _nTot ||
      any_of(mPar.begin(), mPar.end(), [&](const RunPar& par) {
        return (par.nf < _n0 || par.nf > _nTot);
      })) {
    error(ErrorType::Nf);
    return;
  }

  if (int(_fMatch.size()) != _nm) {
    _fMatch.resize(_nm, 1.0);
  }

  doubleV mInit, corr(_nm), vel(_nm);
  mInit.reserve(_nm);
  doubleG mIn(2, _nm);
  vector<MScheme> schemeIn;
  schemeIn.reserve(_nm);

  for (int i = 0; i < _nm; ++i) {
    mIn[0][i] = mPar[i].value;
    schemeIn.push_back(mPar[i].nf <= i + _n0 ? MScheme::MSRn : MScheme::MSbar);
  }

  mInit.insert(mInit.end(), mIn.rowBegin(0), mIn.rowEnd(0));

  _alp.init(mInit, _fMatch);
  for (int i = 0; i < kMaxIter; ++i) {
    if (showOrderError) {
      checkIter(i);
    }
    for (int j = 0; j < _nm; ++j) {
      int nfExp, nfTmp;
      double mValue, mScale;
      const RunPar& m = mPar[j];

      switch (schemeIn[j]) {
        case MScheme::MSRn:
          nfExp = _n0 + j;
          break;

        case MScheme::MSbar:
          nfExp = _n0 + j + 1;
          break;

        default:
          error(ErrorType::Internal);
          return;
          break;
      }

      nfTmp = m.nf;
      mValue = m.value;
      mScale = m.scale;
      for (int k = 0; schemeIn[j] == MScheme::MSRn && nfTmp < nfExp; ++k) {
        mValue = _mass.raiseMsrFlavor(nfTmp, mValue, mScale);
        mScale = (*_mm)[nfTmp - _n0];
        ++nfTmp;
        if (k == _nTot) {
          error(ErrorType::Internal);
          return;
        }
      }
      for (int k = 0; schemeIn[j] == MScheme::MSbar && nfTmp > nfExp; ++k) {
        mValue = lowerMSbarFlavor(nfTmp, mValue, mScale);
        mScale = (*_mm)[nfTmp - 1 - _n0];
        --nfTmp;
        if (k == _nTot) {
          error(ErrorType::Internal);
          return;
        }
      }

      mIn[1][j] = _mass.evolveToMSbar(schemeIn[j], mValue, mScale, j);
      corr[j] = abs(1 - mIn[0][j] / mIn[1][j]);
      if (i * 10 > kMaxIter && corr[j] > _precisionGoal) {
        double damp = 0.2 * double(rand() % 101) / 100.0;
        mIn[1][j] = damp * mIn[0][j] + (1 - damp) * mIn[1][j];
      }
      mInit[j] = mIn[1][j];
    }
    _alp.init(mInit);

    bool finished = all_of(corr.begin(), corr.end(),
                           bind(less<double>(), _1, _precisionGoal));
    if (finished) {
      break;
    } else {
      copy(mIn.rowBegin(1), mIn.rowEnd(1), mIn.rowBegin(0));
    }
  }

  if (showOrderError) {
    checkMassOrd(*_mm);
  }
  _mass.setMmsr();
  _validState = true;
}

//------------------------------------------------------------------------------
void Core::addMass() {
  ++_nm;
  ++_nTot;
  const RunPar& alphaPar = _alp.defParams();

  _alp = Alpha(_nTot, _nm, alphaPar, _runAlpha, _lambdaAlpha, _orderAlpha,
               MScheme::MSbar, _methodAlpha, _precisionGoal);
  _mass = Mass(&_alp, _runMSbar, _lambdaMSbar, _orderMSbar, _runMSR, _lambdaMSR,
               _orderMSR, _msBarDeltaError, _methodMSbar, _methodMSR);

  _mm = &_alp._masses;
}

//------------------------------------------------------------------------------
double Core::lowerMSbarFlavor(int nf, double mMSbarmunf, double mu) const {
  int nl = nf - 1;
  double mmL = (*_mm)[nl - _n0];
  double mmLf = (*_mm)[nl - _n0] * _fMatch[nl - _n0];

  double aNfmu = _alp(mu, nf);
  double aNfmmLf = _alp(mmLf, nf);
  double aNlmmLf = _alp(mmLf, nl);
  double aNlmmL = _alp(mmL, nl);
  double evo = exp(andim(nf).wTilde(_methodMSbar, _runMSbar, aNfmu, aNfmmLf)) *
               exp(andim(nl).wTilde(_methodMSbar, _runMSbar, aNlmmLf, aNlmmL));

  return mMSbarmunf * evo /
         _mass.msBarMatching(Direction::Up, MScheme::MSbar, nf, aNlmmLf,
                             log(_fMatch[nl - _n0]));
}

}  // namespace revo

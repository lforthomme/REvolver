////////////////////////////////////////////////////////////////////////////////
/// \file Global.cpp
/// \brief Definition of some globally available functions
/// \author Andre Hoang, Christopher Lepenik, Vicent Mateu
///
// Copyright:
//    Copyright (C) 2020 Andre Hoang, Christopher Lepenik, Vicent Mateu
//
//    This file is part of REvolver.
//
//    REvolver is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    REvolver is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with REvolver.  If not, see <https://www.gnu.org/licenses/>.
////////////////////////////////////////////////////////////////////////////////

#include "Global.h"

namespace revo {

//------------------------------------------------------------------------------
double binomial(int n, int k) {
  return
    (k > n         ) ? 0 :               // out of range
    (k==0 || k==n  ) ? 1 :               // edge
    (k==1 || k==n-1) ? n :               // first
    (k+k < n       ) ?                   // recursive:
    (binomial(n - 1, k - 1) * n) / k :   //   path to k=1   is faster
    (binomial(n - 1, k) * n) / (n - k);  //   path to k=n-1 is faster
}

//------------------------------------------------------------------------------
void seriesReExpand(const std::vector<double>& a, std::vector<double>& b,
                    bool b0Term) {
  int nb = b0Term ? b.size() - 1 : b.size();
  int na = std::min(int(a.size()), nb);
  rac::Grid<double> c(nb, nb);

  std::copy(a.begin(), a.begin() + na, c.rowBegin(0));

  for (int i = 1; i < nb; ++i) {
    for (int j = i; j < nb; ++j) {
      for (int k = 0; k <= std::min(j - i, na - 1); ++k) {
        c[i][j] += a[k] * c[i - 1][j - k - 1];
      }
    }
  }

  double* bData = (b0Term ? b.data() + 1 : b.data());
  for (int i = nb - 1; i >= 0; --i) {
    for (int k = 0; k < i; ++k) {
      bData[i] += bData[k] * c[k][i];
    }
  }
}

//------------------------------------------------------------------------------
std::vector<double> seriesInverse(const std::vector<double>& aIn) {
  int len = aIn.size();
  std::vector<double> aOut(len);
  aOut[0] = 1.0;
  for (int i = 0; i < len - 1; ++i) {
    for (int j = 0; j <= i; ++j) {
      aOut[i + 1] -= aOut[j] * aIn[i + 1 - j];
    }
  }

  return aOut;
}

//------------------------------------------------------------------------------
std::vector<double> applyLogGrid(const rac::Grid<double>& logGrid, double lg) {
  std::vector<double> out(logGrid.rowBegin(0), logGrid.rowEnd(0));
  double lgList = 1.0;
  for (int j = 1; j < logGrid.numRows(); ++j) {
    lgList *= lg;
    for (int i = j - 1; i < logGrid.numCols(); ++i) {
      out[i] += logGrid[j][i] * lgList;
    }
  }
  return out;
}

//------------------------------------------------------------------------------
bool isDefault(double x) { return std::abs(x - kDefault) < kMaxPrec; }

//------------------------------------------------------------------------------
double interpolLin(double x1, double y1, double x2, double y2, double xIn) {
  return xIn * (y1 - y2) / (x1 - x2) + (x1 * y2 - x2 * y1) / (x1 - x2);
}

//------------------------------------------------------------------------------
double Li2(double x, double precisionGoal) {
  double res = 0.0, xPow = 1.0;

  if (std::abs(x) <= kMaxPrec) {
    return 0.0;
  }

  if (std::abs(x - 1) < kMaxPrec) {
    return M_PI * M_PI / 6.0;
  }

  if (x > 1) {
    return M_PI * M_PI / 3.0 - std::pow(std::log(x), 2) / 2.0 -
           Li2(1 / x, precisionGoal);
  }

  if (x > 0.5) {
    return -Li2(1 - x, precisionGoal) - std::log(1 - x) * std::log(x) +
           M_PI * M_PI / 6.0;
  }

  if (x <= 0.0) {
    return Li2(1.0 / (1 - x), precisionGoal) +
           std::log(1 - x) * std::log((1 - x) / std::pow(x, 2)) / 2.0 -
           M_PI * M_PI / 6.0;
  }

  for (int i = 1; i < kMaxIter; ++i) {
    xPow *= x;
    double corr = xPow / std::pow(i, 2);
    res += corr;
    if (std::abs(corr / res) < precisionGoal) {
      break;
    }
  }

  return res;
}

//------------------------------------------------------------------------------
void error(ErrorType type, const char* s) {
  switch (type) {
    case ErrorType::Nf:
      std::cerr << "ERROR: Invalid input flavor number!\n";
      break;

    case ErrorType::Internal:
      std::cerr << "ERROR: There have been internal inconsistencies!\n";
      break;

    case ErrorType::MassOrd:
      std::cerr << "ERROR: Invalid ordering of input masses!\n";
      break;

    case ErrorType::Iter:
      std::cerr << "WARNING: Maximal number of iterations has been reached.\n";
      break;

    case ErrorType::CustomBeta:
      std::cerr
          << "ERROR: Functionality not available with custom beta function "
             "coefficients!\n";
      break;

    case ErrorType::Keyword:
      std::cerr << "ERROR: Invalid keyword!\n";
      break;

    case ErrorType::NumMasses:
      std::cerr << "ERROR: Invalid number of input masses!\n";
      break;

    case ErrorType::NoCustomBeta:
      std::cerr << "ERROR: Functionality not available with masses defined!\n";
      break;

    case ErrorType::InvalidInput:
      std::cerr << "ERROR: Invalid input for variable " << s << "!\n";
      break;

    case ErrorType::NoMasses:
      std::cerr << "ERROR: No massive flavors!\n";
      break;

    case ErrorType::InvalidState:
      std::cerr << "ERROR: Object is in an invalid state!\n";
      break;

    default:
      std::cerr << "ERROR: an unspecified error occurred!\n";
      break;
  }
}

//------------------------------------------------------------------------------
void checkMassOrd(const std::vector<double>& mm) {
  if (!std::is_sorted(mm.begin(), mm.end())) {
    error(ErrorType::MassOrd);
  }
}

//------------------------------------------------------------------------------
void checkIter(int k) {
  if (k == kMaxIter - 1) {
    error(ErrorType::Iter);
  }
}

}  // namespace revo

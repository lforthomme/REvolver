////////////////////////////////////////////////////////////////////////////////
/// \file Mass.cpp
/// \brief Implementation of class Mass
/// \author Andre Hoang, Christopher Lepenik, Vicent Mateu
///
// Copyright:
//    Copyright (C) 2020 Andre Hoang, Christopher Lepenik, Vicent Mateu
//
//    This file is part of REvolver.
//
//    REvolver is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    REvolver is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with REvolver.  If not, see <https://www.gnu.org/licenses/>.
////////////////////////////////////////////////////////////////////////////////

#define _USE_MATH_DEFINES

#include "Mass.h"

#include <algorithm>
#include <cmath>
#include <functional>
//#include <iostream>
#include <map>
#include <utility>

#include "Interpol.h"

using namespace std;
using namespace std::placeholders;

namespace revo {

//------------------------------------------------------------------------------
// Constructors
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
Mass::Mass(Alpha* alp, int runMSbar, double lambdaMSbar, int orderMSbar,
           int runMSR, double lambdaMSR, int orderMSR, double msBarDeltaError,
           RunMethod methodMSbar, RunMethod methodMSR)
    : Evolving(constrain(runMSR, 1, kMaxRunMSR), 1, lambdaMSR, methodMSR),
      _alpPtr{alp},
      _nTot{_alpPtr->_nTot},
      _nm{_alpPtr->_nm},
      _n0{_nTot - _nm},
      _runMSbar{constrain(runMSbar, 1, kMaxRunMSbar)},
      _lambdaMSbar{lambdaMSbar},
      _orderMSbar{constrain(orderMSbar, 0, kMaxOrderMSbar)},
      _orderMSR{constrain(orderMSR, 0, kMaxOrderMSR)},
      _mm{&_alpPtr->_masses},
      _methodMSbar{methodMSbar},
      _validState{false} {
  if (_nm > 0 && _nm <= _nTot) {
    _alpPtr->_andim.apply(&AnDim::setMsBarDeltaError, msBarDeltaError);
    _alpPtr->_andim.apply(&AnDim::setLambdaMSbar, _lambdaMSbar);
    applyRParameters();

    if (!_mm->empty()) {
      setMmsr();
    }
  }
  _validState = true;
}

//------------------------------------------------------------------------------
// Public member functions
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
double Mass::mMS(int nfIn) const {
  if (int(_alpPtr->inMasses().size()) <= nfIn - _n0 - 1) {
    error(ErrorType::InvalidState);
    return nan("");
  } else if (nfIn > _nTot || nfIn <= _n0) {
    error(ErrorType::Nf);
    return nan("");
  }

  return _alpPtr->inMasses()[nfIn - _n0 - 1];
}

//------------------------------------------------------------------------------
double Mass::mPS(int nfIn, double muF, int nfConv, double scale, double muA,
                 double rIR, int order) const {
  if (!_validState) {
    error(ErrorType::InvalidState);
    return nan("");
  } else if (nfIn <= _n0 || nfIn > _nTot) {
    error(ErrorType::Nf);
    return nan("");
  }
  if (nfConv == kDefault) {
    nfConv = nfIn - 1;
  } else if (!(nfConv == nfIn || nfConv == nfIn - 1)) {
    error(ErrorType::Nf);
    return nan("");
  }
  applyConstrain(order, 0, kMaxOrderPs);

  if (isDefault(muA)) {
    muA = muF;
  }
  if (isDefault(scale)) {
    scale = muF;
  }

  double res = mMS(nfIn, scale, nfConv);
  double mH = (*_mm)[nfIn - 1 - _n0];
  doubleV delMS = (nfConv == nfIn)
                      ? msBarDeltaAsy(nfIn, nfIn, scale, mH, 4)
                      : msrDelta(MScheme::MSRn, nfConv, scale, muA);
  doubleV delPS = psDelta(nfIn - 1, muF, muA, rIR);

  doubleV msLightCorr =
      AnDim::msLightCorr(nfIn - 1 - _n0, 1 - (nfIn - nfConv), nfIn - 1, *_mm);
  doubleV psLightCorr = AnDim::psLightCorr(nfIn - 1 - _n0, nfIn - 1, muF, *_mm);
  if (nfConv == nfIn) {
    andim(nfIn).matchSeries(delMS, Direction::Down);
    andim(nfIn).matchSeries(msLightCorr, Direction::Down);
    andim(nfIn - 1).expandAlpha(mH, muA, delMS);
  }
  andim(nfIn - 1).expandAlpha(mH, muA, msLightCorr);
  andim(nfIn - 1).expandAlpha(muF, muA, psLightCorr);

  double alp = (*_alpPtr)(muA, nfIn - 1) / M_PI;
  double alpPow = 1;
  for (int i = 0; i < order; ++i) {
    alpPow *= alp;
    res += alpPow * (delMS[i] - delPS[i]);
    if (i < 3) {
      res += alpPow * (msLightCorr[i] - psLightCorr[i]);
    }
  }

  return res;
}

//------------------------------------------------------------------------------
double Mass::mRS(int nfIn, double scaleRS, int nfConv, double scale, double muA,
                 int order, int nRS, double N12) const {
  if (!_validState) {
    error(ErrorType::InvalidState);
    return nan("");
  } else if (nfIn <= _n0 || nfIn > _nTot) {
    error(ErrorType::Nf);
    return nan("");
  }
  if (nfConv == kDefault) {
    nfConv = nfIn - 1;
  } else if (!(nfConv == nfIn || nfConv == nfIn - 1)) {
    error(ErrorType::Nf);
    return nan("");
  }
  applyConstrain(order, 0, kMaxRunMSR);
  applyConstrain(nRS, 0, kMaxRunAlpha - 1);

  if (isDefault(muA)) {
    muA = scaleRS;
  }
  if (isDefault(scale)) {
    scale = scaleRS;
  }

  double res = mMS(nfIn, scale, nfConv);
  double mH = (*_mm)[nfIn - 1 - _n0];
  doubleV delMS = (nfConv == nfIn)
                      ? msBarDeltaAsy(nfIn, nfIn, scale, muA, 4)
                      : msrDelta(MScheme::MSRn, nfConv, scale, muA);
  doubleV delRS = rsDelta(nfIn - 1, scaleRS, muA, N12, nRS);

  if (nfConv == nfIn) {
    andim(nfIn).matchSeries(delMS, Direction::Down, log(muA / mH));
  }

  double alp = (*_alpPtr)(muA, nfIn - 1) / M_PI;
  double alpPow = 1;
  for (int i = 0; i < order; ++i) {
    alpPow *= alp;
    res += alpPow * (delMS[i] - delRS[i]);
  }

  return res;
}

//------------------------------------------------------------------------------
double Mass::mPole(int nfIn, double scale, double muA, PoleMethod method,
                   double f, double* ambiguity, int* nMin) const {
  if (!_validState) {
    error(ErrorType::InvalidState);
    return nan("");
  } else if (nfIn <= _n0 || nfIn > _nTot) {
    error(ErrorType::Nf);
    return nan("");
  }
  doubleV mp(1);
  doubleV mpDiff;
  int nMinLoc;
  if (!nMin) {
    nMin = &nMinLoc;
  }
  *nMin = 1;
  if (f < 1) {
    error(ErrorType::InvalidInput, "f");
    return std::nan("");
  }

  if (isDefault(muA)) {
    muA = scale;
  }

  for (int order = 0; order < kMaxIter; ++order) {
    mp.push_back(mPoleFO(nfIn, _n0, scale, muA, order));  // mp[n] == mp(n - 1)
    mpDiff.push_back(mp[order + 1] -
                     mp[order]);  // mpDiff[n] == correction to nth term
    if (order > 1 && mpDiff[order] > mpDiff[order - 1]) {
      *nMin = order - 1;
      break;
    } else if (order == kMaxIter - 1) {
      error(ErrorType::Iter);
      return std::nan("");
    }
  }

  switch (method) {
    case PoleMethod::Default:
    case PoleMethod::Min: {
      if (ambiguity && method == PoleMethod::Default) {
        *ambiguity = 0;
      } else if (ambiguity) {
        *ambiguity = mpDiff[*nMin];
      }
      mp[0] = (mp[*nMin] * (mpDiff[*nMin + 1] - mpDiff[*nMin]) +
               mp[*nMin + 1] * (mpDiff[*nMin - 1] - mpDiff[*nMin])) /
              (mpDiff[*nMin + 1] + mpDiff[*nMin - 1] - 2 * mpDiff[*nMin]);
      break;
    }

    case PoleMethod::Range: {
      double ref = f * mpDiff[*nMin];
      int nLow = -1;
      int nHigh = 1;
      for (int order = 1; order < kMaxIter; ++order) {
        if (int(mpDiff.size()) < order + 1) {
          mp.push_back(mPoleFO(nfIn, _n0, scale, muA, order));
          mpDiff.push_back(mp[order + 1] - mp[order]);
        }
        if (mpDiff[order] <= ref && nLow == -1 && order >= 1) {
          nLow = order;
        } else if (abs(mpDiff[order]) > ref && nLow != -1 && order > 1) {
          nHigh = order - 1;
          mp.push_back(mPoleFO(nfIn, _n0, scale, muA, order + 1));
          mpDiff.push_back(mp[order + 2] - mp[order + 1]);
          break;
        } else if (order == kMaxIter - 1) {
          error(ErrorType::Iter);
          return std::nan("");
        }
      }
      rac::Interpol mpDiffInter(1, 1, doubleV(mpDiff.begin() + 1, mpDiff.end()),
                                alpha().andim(_n0)._precisionGoal, kMaxIter);
      rac::Interpol mpInter(0, 1, doubleV(mp.begin() + 1, mp.end()),
                            alpha().andim(_n0)._precisionGoal, kMaxIter);
      double nMinF1 = mpDiffInter.extr(*nMin - 0.1);
      double nMinF2 = mpDiffInter.extr(*nMin + 0.1);
      double nMinF =
          (mpDiffInter(nMinF1) < mpDiffInter(nMinF2)) ? nMinF1 : nMinF2;
      ref = f * mpDiffInter(nMinF);
      double nLowF = mpDiffInter.inverse(ref, nLow - 1) - 0.5;
      double nHighF = mpDiffInter.inverse(ref, nHigh + 1) - 0.5;
      double mLow = mpInter(nLowF);
      double mHigh = mpInter(nHighF);
      mp[0] = 0.5 * (mHigh + mLow);
      if (ambiguity) {
        *ambiguity = 0.5 * abs(mHigh - mLow);
      }
      break;
    }

    case PoleMethod::DRange: {
      double ref = f * mpDiff[*nMin];
      int nLow = -1;
      int nHigh = 1;
      for (int order = 1; order < kMaxIter; ++order) {
        if (int(mpDiff.size()) < order + 1) {
          mp.push_back(mPoleFO(nfIn, _n0, scale, muA, order));
          mpDiff.push_back(mp[order + 1] - mp[order]);
        }
        if (mpDiff[order] <= ref && nLow == -1) {
          nLow = order;
        } else if (mpDiff[order] > ref && nLow != -1 && order > 1) {
          nHigh = order - 1;
          break;
        } else if (order == kMaxIter - 1) {
          error(ErrorType::Iter);
          return std::nan("");
        }
      }

      double mpMin = mPoleFO(nfIn, _n0, scale, 2 * muA, nLow);
      double mpMax = mPoleFO(nfIn, _n0, scale, 0.5 * muA, nHigh);
      if (mpMin < mpMax) {
        mp[0] = 0.5 * (mpMax + mpMin);
        if (ambiguity) {
          *ambiguity = 0.5 * (mpMax - mpMin);
        }
      } else {
        mp[0] = 0.0;
        if (ambiguity) {
          *ambiguity = 0.0;
        }
        *nMin = 0;
      }
      break;
    }

    default:
      mp[0] = 0;
      if (ambiguity) {
        *ambiguity = 0.0;
      }
      *nMin = 0;
      break;
  }

  return mp[0];
}

//------------------------------------------------------------------------------
double Mass::mPoleFO(int nfIn, int nfConv, double scale, double muA,
                     int order) const {
  if (!_validState) {
    error(ErrorType::InvalidState);
    return nan("");
  } else if (nfIn <= _n0 || nfIn > _nTot) {
    error(ErrorType::Nf);
    return nan("");
  }
  if (order < 0) {
    order = 0;
  }

  double res = mMS(nfIn, scale, nfConv);
  if (order == 0) {
    return res;
  }
  double alpPi = (*_alpPtr)(muA, nfConv) / M_PI;
  doubleV deltaCoef;

  if (nfConv == _n0) {
    deltaCoef = msrDeltaAsy(_n0, scale, muA, order, alpha().runOrd(), _runR);
    res += sumSeries(deltaCoef.begin(), deltaCoef.end(), alpPi, 0.0);
  } else if (nfConv > _n0 && nfConv <= _nTot) {
    map<int, doubleV> alphaMatchUpTo, delM, msrMatch, msrEvoFO;
    map<int, doubleV> msBarEvoFO, msBarMatch;
    double RIn;
    doubleV msrMatchHigh;
    const doubleV& mm = *_mm;
    const doubleV& fMatch = _alpPtr->_fMatch;

    for (int nf = _n0; nf < min(nfConv, nfIn - 1); ++nf) {
      alphaMatchUpTo[nf + 1] = andim(nf + 1).thresholdMatching(
          Direction::Down, alpha()._order, log(muA / mm[nf - _n0]));
      delM[nf + 1] = deltaMCoef(nf + 1, muA);
      delM[nf + 1].resize(_orderMSR);
      delM[nf + 1].resize(order);
      msrMatch[nf + 1] =
          msBarToMsrCoef(Position::Above, nf, fMatch[nf - _n0] * mm[nf - _n0]);
      msrMatch[nf + 1].resize(_orderMSR);
      andim(nf + 1).expandAlpha(fMatch[nf - _n0] * mm[nf - _n0], muA,
                                msrMatch[nf + 1]);
      msrMatch[nf + 1].resize(order);
      if (nf == nfConv - 1) {
        RIn = scale;
      } else {
        RIn = fMatch[nf - _n0 + 1] * mm[nf - _n0 + 1];
      }
      msrEvoFO[nf + 1] =
          msrEvolFOCoef(order, nf + 1, RIn, fMatch[nf - _n0] * mm[nf - _n0],
                        muA, alpha().runOrd(), _runR);
    }
    deltaCoef = msrDeltaAsy(_n0, fMatch[0] * mm[0], muA, order,
                            alpha().runOrd(), _runR);

    for (int nf = nfIn - 1; nf < nfConv; ++nf) {
      alphaMatchUpTo[nf + 1] = andim(nf + 1).thresholdMatching(
          Direction::Down, alpha()._order, log(muA / mm[nf - _n0]));
      if (nf == nfIn - 1) {
        msrMatchHigh = msBarToMsrCoef(Position::Above, nf,
                                      fMatch[nf - _n0] * mm[nf - _n0]);
        msrMatchHigh.resize(_orderMSR);
        andim(nf + 1).expandAlpha(fMatch[nf - _n0] * mm[nf - _n0], muA,
                                  msrMatchHigh);
      }
    }
    msrMatchHigh.resize(order);

    for (int nf = nfIn; nf <= nfConv; ++nf) {
      if (nf == nfConv) {
        RIn = scale;
      } else {
        RIn = fMatch[nf - _n0] * mm[nf - _n0];
      }
      msBarEvoFO[nf] = msBarEvolFOCoef(order, nfIn, nf, RIn,
                                       fMatch[nf - _n0 - 1] * mm[nf - _n0 - 1],
                                       muA, alpha().runOrd(), _runR, _runMSbar);
      if (nf > nfIn) {
        doubleV tmp = applyLogGrid(
            andim(nf).msBarMatchingLog(Direction::Up, MScheme::MSbar),
            log(fMatch[nf - 1 - _n0]));
        msBarMatch[nf - 1] = doubleV(tmp.size() - 1);
        transform(
            tmp.begin() + 1, tmp.end(), msBarMatch[nf - 1].begin(),
            bind(multiplies<double>(), _1,
                 -mMS(nfIn, fMatch[nf - _n0 - 1] * mm[nf - _n0 - 1], nf - 1)));
        msBarMatch[nf - 1].resize(_orderMSbar);
        andim(nf - 1).expandAlpha(fMatch[nf - _n0 - 1] * mm[nf - _n0 - 1], muA,
                                  msBarMatch[nf - 1]);
        msBarMatch[nf - 1].resize(order);
      }
    }

    for (int nf = _n0 + 2; nf <= nfConv; ++nf) {
      for (int nfC = _n0 + 1; nfC < min(nf, nfIn); ++nfC) {
        seriesReExpand(alphaMatchUpTo[nf], delM[nfC]);
        seriesReExpand(alphaMatchUpTo[nf], msrMatch[nfC]);
        seriesReExpand(alphaMatchUpTo[nf], msrEvoFO[nfC]);
      }
      if (nf > nfIn) {
        seriesReExpand(alphaMatchUpTo[nf], msrMatchHigh);
        for (int nfC = nfIn; nfC < nf; ++nfC) {
          seriesReExpand(alphaMatchUpTo[nf], msBarEvoFO[nfC]);
          seriesReExpand(alphaMatchUpTo[nf], msBarMatch[nfC]);
        }
      }
    }

    for (int nf = _n0 + 1; nf <= nfConv; ++nf) {
      seriesReExpand(alphaMatchUpTo[nf], deltaCoef);
    }

    double alpPow = 1.0;
    auto addOrd = [](const map<int, doubleV>& v, int idx) {
      double out = 0.0;
      for (const auto& el : v) {
        out += el.second[idx];
      }
      return out;
    };
    for (int i = 0; i < order; ++i) {
      alpPow *= alpPi;
      res += alpPow * (addOrd(delM, i) + addOrd(msrMatch, i) +
                       addOrd(msrEvoFO, i) + addOrd(msBarEvoFO, i) +
                       addOrd(msBarMatch, i) + deltaCoef[i] + msrMatchHigh[i]);
    }
  } else {
    error(ErrorType::Nf);
    res = nan("");
  }

  return res;
}

//------------------------------------------------------------------------------
double Mass::m1S(int nfIn, int nfConv, double scale, Count1S counting,
                 double muA, int order) const {
  if (!_validState) {
    error(ErrorType::InvalidState);
    return nan("");
  } else if (nfIn <= _n0 || nfIn > _nTot) {
    error(ErrorType::Nf);
    return nan("");
  }
  if (nfConv == kDefault) {
    nfConv = nfIn - 1;
  } else if (!(nfIn == nfConv || nfIn == nfConv + 1) ||
             (nfIn == nfConv && counting == Count1S::Nonrelativistic)) {
    error(ErrorType::Nf);
    return nan("");
  }

  bool isMsBar = (nfIn == nfConv);

  if (counting == Count1S::Default) {
    counting = isMsBar ? Count1S::Relativistic : Count1S::Nonrelativistic;
  }

  int countNR = (counting == Count1S::Nonrelativistic) ? 1 : 0;
  applyConstrain(order, 0, kMaxOrder1s);

  double mH = (*_mm)[nfIn - _n0 - 1];
  if (isDefault(scale)) {
    scale = isMsBar ? mH : mBohr(nfIn);
  }

  double res = mMS(nfIn, scale, nfConv);

  if (isDefault(muA)) {
    muA = scale;
  }

  doubleV delMS;
  if (isMsBar) {
    delMS = msBarDeltaAsy(nfIn, nfIn, scale, muA, 4);
    multiplyAll(delMS, 1.0 / res);
    andim(nfIn).matchSeries(delMS, Direction::Down, log(muA / mH));
  } else {
    delMS = AnDim::msBarDelta(nfConv, 0);
    andim(nfIn - 1).expandAlpha(scale, muA, delMS);
  }

  double alpMuA = (*_alpPtr)(muA, nfIn - 1);

  doubleG del1SLog = andim(nfIn - 1).oneSDeltaLog(alpMuA);

  double rScale = (isMsBar ? res : scale);
  double mB = 4.0 / 3.0 * alpMuA * res;
  double rB = 4.0 / 3.0 * alpMuA * rScale;
  double rBmB = rB / mB;

  doubleP alpPiPow(alpMuA / M_PI);

  double termR[5] = {};
  for (int i = 1; i <= 4; ++i) {
    termR[i + countNR - 1] = alpPiPow[i] * delMS[i - 1];
  }

  double b[4][4] = {};
  double sum;
  for (int t = 0; t <= 3; ++t) {
    for (int n = 0; n <= 3; ++n) {
      if (n == 0) {
        b[n][t] += rBmB * delMS[t];
      } else {
        sum = 0;
        for (int j = 1; j <= t && n > 0; ++j) {
          sum += j * delMS[t - j] * b[n - 1][j - 1];
        }
        b[n][t] -= rBmB * sum / (t + 1.0);
      }
    }
  }

  bool isNatScale = (abs(log(muA / mB)) < kMaxPrec);
  double logMuMb = isNatScale ? 1.0 : log(muA / mB);

  double d[4][4][4] = {};
  transform(&b[0][0], &b[0][0] + 16, &d[0][0][0],
            bind(divides<double>(), _1, -logMuMb));

  for (int k = 1; k < 4; ++k) {
    for (int i = 0; i < 4; ++i) {
      for (int n = 0; n < 4; ++n) {
        for (int j = 0; j <= i + 1; ++j) {
          for (int q = 0; q <= i + 1 - j; ++q) {
            if (q >= 1 && i - j >= 0 && j >= 1 && n - q >= 0) {
              d[k][n][i] += d[0][q - 1][i - j] * d[k - 1][n - q][j - 1];
            } else if (q >= 1 && i - j >= 0 && j == 0 && n - q == -1 &&
                       !isNatScale) {
              d[k][n][i] += d[0][n][i];
            } else if (q == 0 && i - j == -1 && n - q >= 0 && j - 1 >= 0 &&
                       !isNatScale) {
              d[k][n][i] += d[k - 1][n][i];
            }
          }
        }
      }
    }
  }

  doubleP logMuMbPow(logMuMb);

  double termMb[5] = {};
  for (int n = 1; n <= 4; ++n) {
    for (int k = 0; k <= n - 1; ++k) {
      if (n + countNR - 1 < 5 && (!isNatScale || k == 0)) {
        termMb[n + countNR - 1] +=
            del1SLog[k][n - 1] * logMuMbPow[k] * alpPiPow[n];
      }
      if (k > 0) {
        for (int l = 1; l <= 4; ++l) {
          for (int j = 1; j <= l && n + l + countNR * (j + 1) - 1 < 5; ++j) {
            termMb[n + l + countNR * (j + 1) - 1] +=
                del1SLog[k][n - 1] * d[k - 1][j - 1][l - 1] * logMuMbPow[k] *
                alpPiPow[n + l];
          }
        }
      }
    }
  }

  double termRb[5] = {};
  for (int p = 1; p <= 4; ++p) {
    for (int n = 1; n <= 4; ++n) {
      for (int k = 0; k <= n - 1; ++k) {
        if (n + p + countNR * 2 - 1 < 5 && (!isNatScale || k == 0)) {
          termRb[n + p + countNR * 2 - 1] += del1SLog[k][n - 1] * delMS[p - 1] *
                                             logMuMbPow[k] * alpPiPow[n + p];
        }
        if (k > 0) {
          for (int l = 1; l <= 4; ++l) {
            for (int j = 1; j <= l && n + p + l + countNR * (j + 2) - 1 < 5;
                 ++j) {
              termRb[n + p + l + countNR * (j + 2) - 1] +=
                  del1SLog[k][n - 1] * delMS[p - 1] * d[k - 1][j - 1][l - 1] *
                  logMuMbPow[k] * alpPiPow[n + l + p];
            }
          }
        }
      }
    }
  }

  for (int i = 0; i < order + countNR; ++i) {
    res += rScale * termR[i] + mB * termMb[i] + rB * termRb[i];
  }

  doubleV msLightCorr =
      AnDim::msLightCorr(nfIn - _n0 - 1, 1 - (nfIn - nfConv), nfIn - 1, *_mm);
  doubleV oneSLightCorr = AnDim::oneSLightCorr(nfIn - _n0 - 1, nfIn - 1, mB,
                                               *_mm, muA, counting, rB);
  if (isMsBar) {
    andim(nfIn).matchSeries(msLightCorr, Direction::Down);
  }
  andim(nfIn - 1).expandAlpha(mH, muA, msLightCorr);

  double alp = alpMuA / M_PI;
  double alpPow = 1;
  for (int i = 0; i < min(order, 3); ++i) {
    alpPow *= alp;
    res += alpPow * (msLightCorr[i] - oneSLightCorr[i]);
  }

  return res;
}

//------------------------------------------------------------------------------
double Mass::mBohr(int nfIn, double nb) const {
  if (!_validState) {
    error(ErrorType::InvalidState);
    return nan("");
  } else if (nfIn <= _n0 || nfIn > _nTot) {
    error(ErrorType::Nf);
    return nan("");
  }
  return findRoot(
      [&](double x) {
        return nb * 4.0 / 3.0 * (*_alpPtr)(x, nfIn - 1) *
                   mMS(nfIn, x, nfIn - 1) -
               x;
      },
      (*_mm)[nfIn - 1 - _n0], alpha().andim(_n0).precisionGoal());
}

//------------------------------------------------------------------------------
double Mass::mRGI(int nfIn, int order) const {
  if (!_validState) {
    error(ErrorType::InvalidState);
    return nan("");
  } else if (nfIn <= _n0 || nfIn > _nTot) {
    error(ErrorType::Nf);
    return nan("");
  }
  applyConstrain(order, 0, kMaxRunMSbar);

  double mm = (*_mm)[nfIn - 1 - _n0];
  double al = (*_alpPtr)(mm, nfIn);
  return mm * exp(-andim(nfIn).wTilde(_methodMSbar, order, al, double(kDefault),
                                      order));
}

//------------------------------------------------------------------------------
double Mass::mKin(int nfIn, double scaleKin, int nfConv, double scale,
                  double muA, int order) const {
  if (!_validState) {
    error(ErrorType::InvalidState);
    return nan("");
  } else if (nfIn <= _n0 || nfIn > _nTot) {
    error(ErrorType::Nf);
    return nan("");
  }
  if (nfConv == kDefault) {
    nfConv = nfIn - 1;
  } else if (!(nfConv == nfIn || nfConv == nfIn - 1)) {
    error(ErrorType::Nf);
    return nan("");
  }

  applyConstrain(order, 0, kMaxOrderKinetic);

  if (isDefault(muA)) {
    muA = 2 * scaleKin;
  }
  if (isDefault(scale)) {
    scale = 2 * scaleKin;
  }

  double res = mMS(nfIn, scale, nfConv);

  double mH = (*_mm)[nfIn - 1 - _n0];
  doubleV delMS = (nfConv == nfIn)
                      ? msBarDeltaAsy(nfIn, nfIn, scale, muA, 4)
                      : msrDelta(MScheme::MSRn, nfConv, scale, muA);
  if (nfConv == nfIn) {
    andim(nfIn).matchSeries(delMS, Direction::Down, log(muA / mH));
  }

  doubleV delKinLin;
  doubleV delKinQuad;
  kinDelta(_n0, scaleKin, res, muA, delKinLin, delKinQuad);
  for (int nf = _n0; nf < nfIn - 1; ++nf) {
    andim(nf + 1).matchSeries(delKinLin, Direction::Up, log(muA / (*_mm)[nf - _n0]));
    andim(nf + 1).matchSeries(delKinQuad, Direction::Up, log(muA / (*_mm)[nf - _n0]));
  }

  doubleV delMSTilde(delKinLin.size() + 1);
  delMSTilde[0] = 1.0;
  for (int i = 0; i < int(delMSTilde.size() - 1); ++i) {
    for (int j = 0; j <= i; ++j) {
      delMSTilde[i + 1] -= delMS[i - j] / res * delMSTilde[j];
    }
  }

  doubleV mixTerm(delKinQuad.size());
  for (int n = 2; n < int(mixTerm.size() + 1); ++n) {
    for (int k = 1; k <= n - 1; ++k) {
      mixTerm[n - 1] += delKinQuad[k - 1] * delMSTilde[n - k];
    }
  }

  doubleV msLightCorr =
      AnDim::msLightCorr(nfIn - 1 - _n0, 1 - (nfIn - nfConv), nfIn - 1, *_mm);
  if (nfConv == nfIn) {
    andim(nfIn).matchSeries(msLightCorr, Direction::Down);
  }
  andim(nfIn - 1).expandAlpha(mH, muA, msLightCorr);

  double alp = (*_alpPtr)(muA, nfIn - 1) / M_PI;
  double alpPow = 1;
  for (int i = 0; i < order; ++i) {
    alpPow *= alp;
    res += alpPow * (delMS[i] + delKinLin[i] + delKinQuad[i] + mixTerm[i] +
                     msLightCorr[i]);
  }

  return res;
}

//------------------------------------------------------------------------------
double Mass::N12(double lambda) const {
  if (!_validState) {
    error(ErrorType::InvalidState);
    return nan("");
  } else if (_mm->empty()) {
    error(ErrorType::NoMasses);
    return std::nan("");
  }
  if (isDefault(lambda)) {
    return renormalonN(_n0);
  } else {
    doubleV s = sCoefLambda(lambda);
    return renormalonN(_n0, &s);
  }
}

//------------------------------------------------------------------------------
double Mass::P12(double lambda) const {
  if (!_validState) {
    error(ErrorType::InvalidState);
    return nan("");
  }

  if (isDefault(lambda)) {
    return renormalonP(_n0);
  } else {
    doubleV s = sCoefLambda(lambda);
    return renormalonP(_n0, &s);
  }
}

//------------------------------------------------------------------------------
// Private member functions
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
void Mass::setMmsr() {
  _mMSRm.resize(_nm);
  _mMSRmM1.resize(_nm);
  _mmf.resize(_nm);
  const doubleV& mm = *_mm;
  const doubleV& fMatch = _alpPtr->_fMatch;

  for (int j = 0; j < _nm; ++j) {
    _mmf[j] = mMSbar(_n0 + j + 1, _n0 + j + 1, fMatch[j] * mm[j]);
    _mMSRm[j] = msBarToMsr(_orderMSR, _n0 + j, fMatch[j] * mm[j]);
    if (j > 0) {
      _mMSRmM1[j] =
          _mMSRm[j] + rEvol(MScheme::MSRn, _methodR, _n0 + j, fMatch[j] * mm[j],
                            fMatch[j - 1] * mm[j - 1]);
    }
  }
}

//------------------------------------------------------------------------------
Alpha& Mass::alpha() { return *_alpPtr; }

//------------------------------------------------------------------------------
const Alpha& Mass::alpha() const { return *_alpPtr; }

//------------------------------------------------------------------------------
const AnDim& Mass::andim(int nf) const { return _alpPtr->andim(nf); }

//------------------------------------------------------------------------------
doubleV Mass::msrDelta(MScheme type, int nf, double R, double mu) const {
  doubleV delta = applyLogGrid(andim(nf).msDeltaAlphaLog(type), log(mu / R));
  multiplyAll(delta, R);
  return delta;
}

//------------------------------------------------------------------------------
doubleV Mass::msrDeltaAsy(int nl, double R, double mu, int length, int betaOrd,
                          int gammaROrd) const {
  if (length < 1) {
    return doubleV();
  }
  doubleV delta;
  for (int i = 0; i < length; ++i) {
    delta.push_back(andim(nl).msrnDeltaAsy(i, betaOrd, gammaROrd));
  }

  andim(nl).expandAlpha(R, mu, delta, betaOrd);
  multiplyAll(delta, R);

  return delta;
}

//------------------------------------------------------------------------------
doubleV Mass::msBarDeltaAsy(int nfIn, int nfConv, double muM, double muA,
                            int length, int betaOrd, int gammaROrd,
                            int gammaMOrd) const {
  if (nfIn > _nTot || nfIn <= _n0 || length < 1) {
    error(ErrorType::Nf);
    return doubleV();
  }

  double mMu = mMSbar(nfIn, nfConv, muM);
  doubleV delta = applyLogGrid(
      andim(nfConv).msBarDeltaAsyPiece(length, betaOrd, gammaROrd, gammaMOrd),
      log(muM / mMu));
  andim(nfConv).expandAlpha(muM, muA, delta, betaOrd);
  multiplyAll(delta, mMu);

  return delta;
}

//------------------------------------------------------------------------------
doubleV Mass::msBarDelta(int nf, double mu) const {
  doubleV delta = applyLogGrid(andim(nf).msDeltaAlphaLog(MScheme::MSbar),
                               log(mu / (*_mm)[nf - 1 - _n0]));

  double alp = (*_alpPtr)(mu, nf) / M_PI;
  double alpList = 1.0;
  for (auto& i : delta) {
    alpList *= alp;
    i *= (*_mm)[nf - 1 - _n0] * alpList;
  }

  return delta;
}

//------------------------------------------------------------------------------
doubleV Mass::psDelta(int nl, double muF, double muA, double rIR) const {
  doubleV delta = applyLogGrid(andim(nl).psDeltaLog(rIR), log(muA / muF));
  multiplyAll(delta, muF);
  return delta;
}

//------------------------------------------------------------------------------
doubleV Mass::rsDelta(int nl, double R, double muA, double N12, int nRS) const {
  doubleV delta = applyLogGrid(andim(nl).rsDeltaLog(N12, nRS), log(muA / R));
  multiplyAll(delta, R);
  return delta;
}

//------------------------------------------------------------------------------
void Mass::kinDelta(int nl, double scaleKin, double m, double muA,
                    doubleV& linear, doubleV& quadratic) const {
  AnDim::kinDelta(nl, linear, quadratic);
  andim(nl).expandAlpha(2 * scaleKin, muA, linear);
  andim(nl).expandAlpha(2 * scaleKin, muA, quadratic);
  multiplyAll(linear, scaleKin);
  multiplyAll(quadratic, scaleKin * scaleKin / m);
  return;
}

//------------------------------------------------------------------------------
doubleV Mass::deltaMCoef(int nl, double muA, int betaOrd) const {
  if (nl <= _n0) {
    error(ErrorType::Nf);
    return doubleV();
  }
  const doubleV& mm = *_mm;

  int nmh = nl - _n0;
  double mh = mm[nmh];
  int nml = nmh - 1;
  double ml = mm[nml];
  double muMatch = mm[nl - _n0 - 1] * _alpPtr->_fMatch[nl - _n0 - 1];

  doubleV deltaMsBar = msBarDeltaAsy(nl, nl, muMatch, ml, 3);
  doubleV coef = msrDeltaAsy(nl, muMatch, ml, 3);
  transform(coef.begin(), coef.end(), deltaMsBar.begin(), coef.begin(),
            minus<double>());

  for (int i = 0; i < nmh; ++i) {
    coef[1] += mh * AnDim::msLightCorr2L(mm[i] / mh);
    if (i < nml) {
      coef[1] -= ml * AnDim::msLightCorr2L(mm[i] / ml);
    }
  }

  for (int i = 0; i < nmh; ++i) {
    coef[2] += mh * (AnDim::msLightCorr3L(0, nl, mm[i] / mh) +
                     andim(nl).betaCoef()[0] * log(ml / mh) *
                         AnDim::msLightCorr2L(mm[i] / mh));
    for (int j = 0; j < i; ++j) {
      coef[2] += mh * AnDim::msLightCorr3LDoBu(mm[i] / mh, mm[j] / mh);
    }
    if (i < nml) {
      coef[2] -= ml * AnDim::msLightCorr3L(1, nl - 1, mm[i] / ml);
      for (int j = 0; j < i; ++j) {
        coef[2] -= ml * AnDim::msLightCorr3LDoBu(mm[i] / ml, mm[j] / ml);
      }
    }
  }

  andim(nl).expandAlpha(ml, muA, coef, betaOrd);
  return coef;
}

//------------------------------------------------------------------------------
doubleV Mass::msrEvolFOCoef(int order, int nl, double RIn, double ROut,
                            double muA, int betaOrd, int gammaROrd) const {
  doubleV deltaCoefRIn = msrDeltaAsy(nl, RIn, muA, order, betaOrd, gammaROrd);
  doubleV deltaCoefROut = msrDeltaAsy(nl, ROut, muA, order, betaOrd, gammaROrd);
  transform(deltaCoefRIn.begin(), deltaCoefRIn.end(), deltaCoefROut.begin(),
            deltaCoefRIn.begin(), minus<double>());
  return deltaCoefRIn;
}

//------------------------------------------------------------------------------
doubleV Mass::msBarEvolFOCoef(int order, int nfMass, int nfEvo, double muIn,
                              double muOut, double muA, int betaOrd,
                              int gammaROrd, int gammaMOrd) const {
  doubleV deltaCoefMuIn = msBarDeltaAsy(nfMass, nfEvo, muIn, muA, order,
                                        betaOrd, gammaROrd, gammaMOrd);
  doubleV deltaCoefMuOut = msBarDeltaAsy(nfMass, nfEvo, muOut, muA, order,
                                         betaOrd, gammaROrd, gammaMOrd);
  transform(deltaCoefMuIn.begin(), deltaCoefMuIn.end(), deltaCoefMuOut.begin(),
            deltaCoefMuIn.begin(), minus<double>());
  return deltaCoefMuIn;
}

//------------------------------------------------------------------------------
doubleV Mass::msBarToMsrCoef(Position pos, int nl, double R,
                             double mBarR) const {
  if (isDefault(mBarR)) {
    mBarR = mMSbar(nl + 1, nl + 1, R);
  }

  doubleV deltaMSR(andim(nl)._msrnDelta);
  doubleV deltaMSbar = applyLogGrid(
      andim(nl + 1).msBarDeltaAsyPiece(deltaMSR.size()), log(R / mBarR));
  multiplyAll(deltaMSbar, mBarR);
  multiplyAll(deltaMSR, R);

  double mm = (*_mm)[nl - _n0];
  switch (pos) {
    case Position::Above:
      andim(nl + 1).matchSeries(deltaMSR, Direction::Up, log(R / mm));
      break;

    case Position::Below:
    default:
      andim(nl + 1).matchSeries(deltaMSbar, Direction::Down, log(R / mm));
      break;
  }

  transform(deltaMSR.begin(), deltaMSR.end(), deltaMSbar.begin(),
            deltaMSR.begin(), bind(minus<double>(), _2, _1));

  doubleV lightMassCorr(3);
  for (auto m = _mm->begin(); m < _mm->begin() + nl - _n0; ++m) {
    lightMassCorr[2] += mm * AnDim::msLightCorr3LNh(*m / mm);
  }
  switch (pos) {
    case Position::Below:
      andim(nl).expandAlpha(mm, R, lightMassCorr);
      break;

    case Position::Above:
    default:
      andim(nl + 1).expandAlpha(mm, R, lightMassCorr);
      break;
  }

  transform(lightMassCorr.begin(), lightMassCorr.end(), deltaMSR.begin(),
            deltaMSR.begin(), plus<double>());

  return deltaMSR;
}

//------------------------------------------------------------------------------
double Mass::msBarToMsr(int orderMass, int nl, double R, double mBarR) const {
  if (isDefault(mBarR)) {
    mBarR = mMSbar(nl + 1, nl + 1, R);
  }
  if (orderMass <= 0) {
    return mBarR;
  }
  double aNlPi = (*_alpPtr)(R, nl) / M_PI;
  doubleV coef = msBarToMsrCoef(Position::Below, nl, R, mBarR);
  return sumSeries(coef.begin(), coef.begin() + min(orderMass, kMaxOrderMSR),
                   aNlPi, mBarR);
}

//------------------------------------------------------------------------------
double Mass::msrToMsBar(int orderMass, int nl, double R, double mR) const {
  return findRoot(
      [&](double m) { return mR - msBarToMsr(orderMass, nl, R, m); }, mR,
      alpha().andim(_n0).precisionGoal());
}

//------------------------------------------------------------------------------
double Mass::raiseMsrFlavor(int nl, double mMsrRnl, double R) const {
  double muMatch = (*_mm)[nl - _n0] * _alpPtr->_fMatch[nl - _n0];
  double muFin = (*_mm)[nl - _n0];
  double mBarMatch = mMSbar(nl + 1, nl + 1, muMatch);
  double match = -(msBarToMsr(_orderMSR, nl, muMatch, mBarMatch) - mBarMatch) -
                 deltaM(nl + 1, muMatch, _orderMSR);
  double res = mMsrRnl + rEvol(MScheme::MSRn, _methodR, nl, R, muMatch) +
               rEvol(MScheme::MSRn, _methodR, nl + 1, muMatch, muFin) + match;

  if (std::isnan(res)) {
    return mMsrRnl;
  } else {
    return res;
  }
}

//------------------------------------------------------------------------------
doubleV Mass::sCoefLambda(double lambda) const {
  doubleV delta = msrDelta(MScheme::MSRn, _n0, lambda, 1.0);
  doubleV gammaR = _alpPtr->andim(_n0).gammaRComputer(andim(_n0)._beta, delta);
  gammaR.resize(_runR);
  doubleV gammaTil;
  doubleV sCoe;
  for (int i = 0; i < _runR; ++i) {
    _alpPtr->andim(_n0).setNextsCoefGeneric(
        sCoe, gammaTil, gammaR,
        bind(&AnDim::gTilde, andim(_n0), _1, alpha().runOrd(), true, 1),
        bind(&AnDim::bHat, andim(_n0), _1, alpha().runOrd(), true));
  }
  return sCoe;
}

//------------------------------------------------------------------------------
RunParV Mass::getMassPar() const {
  RunParV mPar(_nm);
  for (int i = 0; i < _nm; ++i) {
    mPar[i] = {_n0 + 1 + i, (*_mm)[i], (*_mm)[i]};
  }
  return mPar;
}

//------------------------------------------------------------------------------
double Mass::mMSRp(int nfIn, double R) const {
  int nl = nfIn - 1;
  if (nl < _n0) {
    error(ErrorType::Nf);
    return nan("");
  }

  return (*_mm)[nl - _n0] +
         rEvol(MScheme::MSRp, _methodR, nl, (*_mm)[nl - _n0], R);
}

//------------------------------------------------------------------------------
double Mass::evolveToMSbar(MScheme scheme, double mIn, double mu0,
                           int nm) const {
  double mu1 = (*_mm)[nm];
  int nl = _n0 + nm;
  double a1 = (*_alpPtr)(mu1, nl + 1);
  if (scheme == MScheme::MSbar) {
    double a0 = (*_alpPtr)(mu0, nl + 1);
    return mIn *
           exp(_alpPtr->andim(nl + 1).wTilde(_methodMSbar, _runMSbar, a0, a1));
  } else if (scheme == MScheme::MSRn) {
    double muMatch = (*_mm)[nm] * _alpPtr->_fMatch[nm];
    mIn += rEvol(MScheme::MSRn, _methodR, nl, mu0, muMatch);
    mIn = msrToMsBar(_orderMSR, nl, muMatch, mIn);
    double aMatch = (*_alpPtr)(muMatch, nl + 1);
    return mIn * exp(_alpPtr->andim(nl + 1).wTilde(_methodMSbar, _runMSbar,
                                                   aMatch, a1));
  } else {
    error(ErrorType::Keyword);
    return nan("");
  }
}

//------------------------------------------------------------------------------
double Mass::deltaM(int nl, double muA, int order, int betaOrd) const {
  if (nl <= _n0) {
    error(ErrorType::Nf);
    return 0.0;
  }
  if (order == kDefault) {
    order = _orderMSR;
  } else if (order <= 0) {
    return 0.0;
  }
  if (isDefault(muA)) {
    muA = (*_mm)[nl - _n0 - 1] * _alpPtr->_fMatch[nl - _n0 - 1];
  }
  doubleV deltaCoef = deltaMCoef(nl, muA, betaOrd);
  double anlmlPi = (*_alpPtr)(muA, nl) / M_PI;

  return sumSeries(deltaCoef.begin(),
                   deltaCoef.begin() + min(order, int(deltaCoef.size())),
                   anlmlPi, 0.0);
}

//------------------------------------------------------------------------------
double Mass::msBarMatching(Direction dir, MScheme scheme, int nf, double alpha,
                           double lg) const {
  if (_orderMSbar <= 0) {
    return 1.0;
  }
  alpha /= M_PI;

  doubleV matchCoe = applyLogGrid(andim(nf).msBarMatchingLog(dir, scheme), lg);

  return sumSeries(matchCoe.begin() + 1, matchCoe.begin() + _orderMSbar + 1,
                   alpha, matchCoe[0]);
}

}  // namespace revo

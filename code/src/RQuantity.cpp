////////////////////////////////////////////////////////////////////////////////
/// \file RQuantity.cpp
/// \brief Implementation of class RQuantity
/// \author Andre Hoang, Christopher Lepenik, Vicent Mateu
///
// Copyright:
//    Copyright (C) 2020 Andre Hoang, Christopher Lepenik, Vicent Mateu
//
//    This file is part of REvolver.
//
//    REvolver is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    REvolver is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with REvolver.  If not, see <https://www.gnu.org/licenses/>.
////////////////////////////////////////////////////////////////////////////////

#include "RQuantity.h"

using namespace std;

namespace revo {

//------------------------------------------------------------------------------
// Constructors
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
RQuantity::RQuantity(const RunPar& alphaPar, const RunPar& rPar, int pR,
                     const doubleV& gamma, int runAlpha, double lambdaAlpha,
                     double lambdaR, const doubleV& beta,
                     double precisionGoal)
    : Evolving(gamma.size(), pR, lambdaR, RunMethod::ExactPF),
      _nTot{alphaPar.nf},
      _runAlpha{beta.empty() ? constrain(runAlpha, 1, kMaxRunAlpha)
                             : constrain(runAlpha, 1, int(beta.size()))},
      _lambdaAlpha{lambdaAlpha},
      _precisionGoal{constrain(precisionGoal, kMaxPrec, kMinPrec)},
      _methodAlpha{RunMethod::Exact},
      _alp(_nTot, 0, alphaPar, _runAlpha, _lambdaAlpha, kMaxOrderAlpha,
           MScheme::MSbar, _methodAlpha, _precisionGoal),
      _rScaleRef{rPar.scale},
      _rValueRef{rPar.value} {
  if (!beta.empty()) {
    if (int(beta.size()) > runAlpha) {
      _alp.setBeta(doubleV(beta.begin(), beta.begin() + runAlpha));
    } else {
      _alp.setBeta(beta);
    }
  }
  applyRParameters();
  _alp._andim.apply(&AnDim::setGammaR, gamma);
}

//------------------------------------------------------------------------------
// Public member functions
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
const Alpha& RQuantity::alpha() const { return _alp; }

//------------------------------------------------------------------------------
// Private member functions
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
Alpha& RQuantity::alpha() { return _alp; }

}  // namespace revo
